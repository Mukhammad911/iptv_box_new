import React, { useEffect, useState } from 'react';
import {
  FlatList,
  Image,
  Text,
  View,
  StyleSheet,
  ScrollView,
  DeviceEventEmitter,
  TVEventHandler
} from 'react-native';
import { fSize, percentHeight, percentWidth } from '../templates/helper';
import Icon from 'react-native-vector-icons/Ionicons';
import { colors } from '../templates/colors';
import { request_get } from '../api/request';
import { IPTV_ADMIN_URL } from '../api/url';
import FilmCard from '../components/films/FilmCard';
import { useDispatch, useSelector } from 'react-redux';
// import HistoryList from '../components/Main/historyList';
import { getHistory, setAuthState, setSid } from "../redux/actions";
import ScreenHeader from '../components/ScreenHeader';
import { SCALE_PARAM, DEFAULT_SCALE } from '../templates/scaleParameters';
import FocusableOpacity from '../components/FocusableOpacity';
import KeyEvent from 'react-native-keyevent'
import SidNotFound from '../components/SidNotFound';
import TouchableFocus from '../components/TouchableFocus';
import { useIsFocused } from '@react-navigation/native';
import { chechToken } from "../components/helper";
import AsyncStorage from "@react-native-community/async-storage";
import sidValidation from "../services/sidChecker";

const BannerItem = React.memo(({ item, index, banners, navigation, menuRef }) => {
  return (
    <TouchableFocus
      focusedOnStart={index == 0}
      style={{
        justifyContent: 'space-between',
        alignItems: 'center',
        width: percentWidth(18),
        height: percentHeight(29),
        margin: 5,
        position: 'relative',
        paddingTop: percentHeight(1),
        marginRight: percentWidth(2),
        borderWidth: 1,
        borderColor: colors.primary,
        borderRadius: 5,
        paddingHorizontal: percentWidth(1),
        paddingVertical: percentHeight(1),
      }}
      focusedStyle={{
        // transform: [{ scale: SCALE_PARAM }],
        borderColor: colors.dark_red,
      }}
      onPress={() => {
        navigation.navigate('player', {
          id: item.channel_id,
          group_channel_id: item.group_channel_id,
          selectedChannelIndex: index,
          channelsArray: banners,
        })
      }}
      // nextFocusUpSelf
      nextFocusRightSelf={((index + 1) == banners.length)}
      nextFocusLeft={((!index) ? menuRef : null)}
    >
      <View
        style={{
          width: '100%'
        }}
      >
        <Image
          source={{
            uri: `${IPTV_ADMIN_URL}images/channel/${item.channelImage}`,
          }}
          style={{
            position: 'absolute',
            width: percentWidth(3),
            height: percentHeight(5),
            resizeMode: 'center',
            zIndex: 99,
            top: percentHeight(1.5),
            left: percentWidth(0.7),
          }}
        />
        <Image
          source={{
            uri: `http://epg.somon.tv/banners/${item.banner}`,
          }}
          style={{
            resizeMode: 'cover',
            width: '100%',
            height: percentHeight(20),
            borderRadius: percentWidth(1),
          }}
        />
      </View>
      <View
        style={{
          alignItems: 'flex-start',
          width: '100%'
        }}
      >
        <Text
          style={{
            color: colors.secondary,
            fontSize: fSize(5),
            zIndex: 99,
          }}>
          {item.name.length > 50 ? (item.name.substring(0, 47) + '...') : item.name}
        </Text>
      </View>
    </TouchableFocus>
  )
})

const BannerList = React.memo(({ banners, navigation, menuRef }) => {
  return banners.length > 0 ? (
    <View style={{ height: percentHeight(35), marginBottom: percentHeight(2) }}>
      <View>
        <Text
          style={{
            color: colors.secondary,
            fontSize: fSize(7),
            paddingLeft: percentWidth(2)
          }}
        >
          Сейчас в эфире
        </Text>
      </View>
      <FlatList
        data={banners}
        horizontal={true}
        // contentContainerStyle={{
        // paddingLeft: percentWidth(2)
        // }}
        keyExtractor={(item) => item.channel_id.toString()}
        renderItem={({ item, index }) => <BannerItem item={item} index={index} banners={banners} navigation={navigation} menuRef={menuRef} />}
      />
    </View>
  ) : null
})

// let TVEventHandler = require('TVEventHandler')
// Токенизация реализована

const MainScreen = ({ navigation }) => {
  const isFocused = useIsFocused();

  const menuRef = useSelector(state => state.refReducer.menuRef);

  const [channels, setChannels] = useState([]);
  const [favoriteFilms, setFavoriteFilms] = useState([]);
  const [newFilms, setNewFilms] = useState([]);
  const [cartnoons, setCartnoons] = useState([]);
  const [serials, setSerials] = useState([]);
  const [popularFilms, setPopularFilms] = useState([]);
  const [banners, setBanners] = useState([]);
  const [radios, setRadios] = useState([]);
  const filmsHistory = useSelector((state) => state.historyReducer.historyView);
  const [musics, setMusics] = useState([]);
  const sid = useSelector((state) => state.authReducer.sid);
  const dispatch = useDispatch();

  const [onlineChannels, setOnlineChannels] = useState(Infinity)
  const [focusedChannel, setFocusedChannel] = useState(Infinity)
  const [focusedFavoriteFilm, setFocusedFavoriteFilm] = useState(Infinity)
  const [focusedNewFilm, setFocusedNewFilm] = useState(Infinity)
  const [focusedPopularFilm, setFocusedPopularFilm] = useState(Infinity)
  const [focusedCartoon, setFocusedCartoon] = useState(Infinity)
  const [focusedSerial, setFocusedSerial] = useState(Infinity)
  const [focusedMusic, setFocusedMusic] = useState(Infinity)
  const [focusedRadio, setFocusedRadio] = useState(Infinity)
  const [lookAll1, setLookAll1] = useState(false)
  const [lookAll2, setLookAll2] = useState(false)
  const [lookAll3, setLookAll3] = useState(false)
  const [lookAll4, setLookAll4] = useState(false)
  const [lookAll5, setLookAll5] = useState(false)
  const [lookAll6, setLookAll6] = useState(false)

  //Here we will check the access token
  useEffect(() => {
    const ff = async () => {
      let result = await chechToken()
      if (result) {
        dispatch(setAuthState(true))
      } else {
        dispatch(setAuthState(false))
      }
    }
    ff()
  }, []);

  //Here we will check the sid
  useEffect(() => {
    AsyncStorage.getItem('sid')
      .then((sid) => sidValidation(sid))
      .then(async ({ success, sid }) => {
        if (success) {
          dispatch(setSid(sid));
        } else {
          await AsyncStorage.removeItem('sid');
          navigation.navigate('subscribes');
        }
      })

  }, []);

  const changeNameFilm = (name) => {

    let arr_name = name.split("");
    if (arr_name.length < 14) {
      return name;
    }
    let cutname = arr_name.splice(0, 14).join('');

    return cutname + ' ...';

  }

  const formatOutput = (key, spaceCount = 0) => {
    if (typeof key == 'object') {
      for (let i in key) {
        let space = ''
        for (let j = 0; j < spaceCount; j++) {
          space += ' '
        }
        if (typeof key[i] == 'object') {
          formatOutput(key[i], spaceCount + 2)
        }
      }
    }
  }

  useEffect(() => {
    if (sid) {
      request_get(`favorites/all/?sid=${sid}`).then(({ films }) => {
        const favoriteFilmsVoddata = films.map((film) => film.voddata);
        setFavoriteFilms(favoriteFilmsVoddata);
      });
      request_get(`vod_data_top/${sid}`).then(({ data }) => {
        setPopularFilms(data);
      });
      request_get(`vod_data_news/${sid}`).then(({ data }) => {
        setNewFilms(data);
      });
      request_get('channels/banners').then(({ data }) => {
        const newBanners = data.banners.map((banner) => {
          banner.id = banner.channel_id;
          banner.channelImage = data.channels[banner.channel_id].image;
          banner.group_channel_id = data.channels[banner.channel_id].group_channel_id;
          return banner;
        });
        setBanners(newBanners);
      });

      request_get(`api/radios/${sid}`).then(({ data }) => {
        const shuffledRadios = data
          .map((data) => data.radios)
          .flat(1)
          .sort(() => Math.random - 0.5)
          .slice(0, 10);
        setRadios(shuffledRadios);
      });

      request_get(`vod_datas/${sid}/1?type=4`).then(({ data }) => {
        setCartnoons(data);
      });
      request_get(`vod_datas/${sid}/1?type=2`).then(({ data }) => {
        setSerials(data);
      });

      request_get(`musics_group/${sid}`).then(({ data }) => {
        const musics = data
          .map((music) => music['music_datas'])
          .flat(1)
          .map((music) => ({
            id: music.id,
            musics: music.music_vodfiles,
            album: {
              music_vodfiles: music.music_vodfiles,
              music_data_genres: music.music_data_genres,
            },
            poster: music.poster,
          }));
        setMusics(musics);
      });
      dispatch(getHistory(sid));
    }
  }, [sid, navigation]);

  useEffect(() => {
    DeviceEventEmitter.addListener('goBack', (e) => {
      getChannels();
    });
    getChannels();
  }, []);

  const getChannels = async () => {
    if (sid) {
      const response = await request_get('random/channels?sid=' + sid);
      const newChannels = [
        ...response.data,
        {
          id: 'all',
          image: 'all',
          group_channel_id: 'all',
        },
      ];
      setChannels(newChannels);
    }
  };

  const ChannelsCard = ({ id, image, group_channel_id }, index, data) => {
    return id === 'all' ? (
      <TouchableFocus
        style={styles.imageContainer}
        focusedStyle={{ borderColor: colors.dark_red, }}
        key={id.toString()}
        onPress={() => navigation.navigate('tv')}
        nextFocusRightSelf
      >
        <Text
          style={{
            color: '#fff',
            fontSize: fSize(5)
          }}>
          Смотреть все
        </Text>
      </TouchableFocus>
    ) : (
      <TouchableFocus
        style={styles.imageContainer}
        // focusedStyle={{ transform: [{ scale: SCALE_PARAM }] }}
        focusedStyle={{
          borderColor: colors.dark_red,
        }}
        key={id.toString()}
        onPress={() =>
          navigation.navigate('player', {
            id: id,
            group_channel_id: group_channel_id,
            selectedChannelIndex: index,
            channelsArray: data
          })
        }
        nextFocusLeft={(!index ? menuRef : null)}
      >
        <Image
          source={{ uri: `${IPTV_ADMIN_URL}images/channel/${image}` }}
          style={{ width: percentWidth(30), height: percentHeight(12) }}
          resizeMode="contain"
        />
      </TouchableFocus>
    );
  };

  const channelSlider = ({ data, title }) => {
    return (
      <View style={{ height: percentHeight(20), marginBottom: 2 }}>
        <Text style={{ fontSize: fSize(7), color: colors.secondary, paddingLeft: percentWidth(2) }}>
          {title}
        </Text>
        <FlatList
          horizontal={true}
          data={data}
          contentContainerStyle={{
            paddingTop: percentHeight(2),
            paddingLeft: percentWidth(2)
          }}
          renderItem={({ item, index }) => ChannelsCard(item, index, data)}
          keyExtractor={(item) => item.id.toString()}
        />
      </View>
    );
  };

  const slider = ({
    title,
    data,
    expandAll,
    redirectToMoviePage = false,
    params = null,
  }) => {
    return (
      <View style={{ height: percentHeight(65) }}>
        <Text style={{ fontSize: fSize(7), color: colors.secondary, paddingLeft: percentWidth(2) }}>
          {title}
        </Text>
        <TouchableFocus
          style={{
            position: 'absolute',
            right: 0,
          }}
          focusedStyle={{ transform: [{ scale: SCALE_PARAM }] }}
          onPress={() => {
            if (!redirectToMoviePage) {
              navigation.navigate(expandAll, {
                screen: expandAll,
              });
            } else {
              navigation.navigate('movies', {
                vodType: params?.vodType,
                vodName: params?.vodName,
              });
            }
          }}
          nextFocusLeftSelf
          nextFocusRightSelf
        >
          <Text
            style={{
              fontSize: fSize(7),
              color: 'white',
              paddingRight: percentWidth(2),
            }}>
            Смотреть все
          </Text>
        </TouchableFocus>
        <FlatList
          data={data.slice(0, 20)}
          contentContainerStyle={{
            paddingTop: percentHeight(4),
          }}
          removeClippedSubviews={true}
          updateCellsBatchingPeriod={100}
          initialNumToRender={5}
          maxToRenderPerBatch={5}
          windowSize={7}
          renderItem={({ item, index }) => {
            const { id, name, poster, year, genres } = item;
            let format = 'standart';
            return (
              <TouchableFocus
                style={{
                  marginBottom: percentHeight(3),
                  flex: 1,
                  paddingLeft: percentWidth(2),
                  paddingRight: percentWidth(2),
                  backgroundColor: colors.primary,
                  borderTopEndRadius: 5,
                  borderTopStartRadius: 5,
                  borderWidth: 1,
                  borderColor: colors.primary,
                  marginTop: 5,
                  overflow: 'hidden',
                  width: percentWidth(19),
                  height: percentHeight(50),
                  alignItems: 'center',
                  justifyContent: "flex-start",
                }}
                focusedStyle={{
                  borderColor: colors.dark_red
                }}
                onPress={() =>
                  navigation.navigate('vplayer', { id: id, format: format })
                }
                nextFocusRightSelf={((index + 1) == data.length)}
                nextFocusLeft={(!index ? menuRef : null)}
              >
                <View>
                  <Image
                    source={{ uri: IPTV_ADMIN_URL + 'posters/' + poster }}
                    style={{
                      height: percentHeight(40),
                      width: percentWidth(15),
                    }}
                  />
                </View>
                <View
                  style={{
                    marginTop: 5,
                    width: percentWidth(15),
                    maxHeight: percentHeight(4),
                    overflow: 'hidden',
                  }}>
                  <Text style={{ color: '#fff', fontSize: fSize(7) }}>{changeNameFilm(name)}</Text>
                </View>
                <View
                  style={{
                    width: percentWidth(15),
                    maxHeight: percentHeight(20),
                    overflow: 'hidden',
                  }}>
                  <Text style={{ color: colors.dark_text, fontSize: fSize(5) }}>
                    {year} - {genres.length > 0 ? genres[0].name : ''}
                  </Text>
                </View>
              </TouchableFocus>
            )
          }}
          keyExtractor={(item) => item.id.toString()}
          horizontal={true}
        />
      </View>
    );
  };

  return !sid ? <SidNotFound navigate={navigation.navigate} /> : (
    <ScrollView
      style={{ flex: 1, backgroundColor: colors.primary, paddingHorizontal: percentWidth(1) }}
    >
      <ScreenHeader screenName="Главная" />
      {/* {banners.length > 0 ? (
        <View style={{ height: percentHeight(35), marginBottom: percentHeight(2) }}>
          <View>
            <Text
              style={{
                color: colors.secondary,
                fontSize: fSize(7),
                paddingLeft: percentWidth(2)
              }}
            >
              Сейчас в эфире
            </Text>
          </View>
          <FlatList
            data={banners}
            horizontal={true}
            // contentContainerStyle={{
            // paddingLeft: percentWidth(2)
            // }}
            keyExtractor={(item) => item.channel_id.toString()}
            renderItem={({ item, index }) => {
              return (
                <TouchableFocus
                  focusedOnStart={index == 0}
                  style={{
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    width: percentWidth(18),
                    height: percentHeight(29),
                    margin: 5,
                    position: 'relative',
                    paddingTop: percentHeight(1),
                    marginRight: percentWidth(2),
                    borderWidth: 1,
                    borderColor: colors.primary,
                    borderRadius: 5,
                    paddingHorizontal: percentWidth(1),
                    paddingVertical: percentHeight(1),
                  }}
                  focusedStyle={{
                    // transform: [{ scale: SCALE_PARAM }],
                    borderColor: colors.dark_red,
                  }}
                  onPress={() => {
                    navigation.navigate('player', {
                      id: item.channel_id,
                      group_channel_id: item.group_channel_id,
                      selectedChannelIndex: index,
                      channelsArray: banners,
                    })
                  }}
                  // nextFocusUpSelf
                  nextFocusRightSelf={((index + 1) == banners.length)}
                  nextFocusLeft={((!index) ? menuRef : null)}
                >
                  <View
                    style={{
                      width: '100%'
                    }}
                  >
                    <Image
                      source={{
                        uri: `${IPTV_ADMIN_URL}images/channel/${item.channelImage}`,
                      }}
                      style={{
                        position: 'absolute',
                        width: percentWidth(3),
                        height: percentHeight(5),
                        resizeMode: 'center',
                        zIndex: 99,
                        top: percentHeight(1.5),
                        left: percentWidth(0.7),
                      }}
                    />
                    <Image
                      source={{
                        uri: `http://epg.somon.tv/banners/${item.banner}`,
                      }}
                      style={{
                        resizeMode: 'cover',
                        width: '100%',
                        height: percentHeight(20),
                        borderRadius: percentWidth(1),
                      }}
                    />
                  </View>
                  <View
                    style={{
                      alignItems: 'flex-start',
                      width: '100%'
                    }}
                  >
                    <Text
                      style={{
                        color: colors.secondary,
                        fontSize: fSize(5),
                        zIndex: 99,
                      }}>
                      {item.name.length > 50 ? (item.name.substring(0, 47) + '...') : item.name}
                    </Text>
                  </View>
                </TouchableFocus>
              )
            }
            }
          />
        </View>
      ) : null} */}
      <BannerList banners={banners} navigation={navigation} menuRef={menuRef} />
      {channels
        ? channelSlider({
          title: 'Телеканалы',
          data: channels,
        })
        : null}
      {favoriteFilms.length > 0
        ? slider({
          data: favoriteFilms.slice(0, 25),
          title: 'Избранные',
          expandAll: 'favorite',
        }, focusedFavoriteFilm, setFocusedFavoriteFilm, lookAll1, setLookAll1)
        : null}
      {newFilms.length > 0
        ? slider({
          data: newFilms.slice(0, 25),
          title: 'Новинки',
          expandAll: 'newFilms',
        }, focusedNewFilm, setFocusedNewFilm, lookAll2, setLookAll2)
        : null}
      {popularFilms.length > 0
        ? slider({
          data: popularFilms.slice(0, 25),
          title: 'Популярные',
          expandAll: 'popularFilms',
        }, focusedPopularFilm, setFocusedPopularFilm, lookAll3, setLookAll3)
        : null}
      {cartnoons.length > 0
        ? slider({
          data: cartnoons.slice(0, 25),
          title: 'Мультфильмы',
          expandAll: 'cartnoons',
          redirectToMoviePage: true,
          params: { vodType: 4, vodName: 'Мультфильмы' },
        }, focusedCartoon, setFocusedCartoon, lookAll4, setLookAll4)
        : null}
      {serials.length > 0
        ? slider({
          data: serials.slice(0, 25),
          title: 'Сериалы',
          expandAll: 'serials',
          redirectToMoviePage: true,
          params: { vodType: 2, vodName: 'Сериалы' },
        }, focusedSerial, setFocusedSerial, lookAll5, setLookAll5)
        : null}
      {musics.length > 0 ? (
        <View
          style={{
            height: percentHeight(30)
          }}
        >
          <Text style={{ fontSize: fSize(7), color: colors.secondary, paddingLeft: percentWidth(2) }}>
            Новые хиты
          </Text>
          <FlatList
            data={musics.slice(0, 25)}
            keyExtractor={(item) => item.id.toString()}
            // contentContainerStyle={{
            //   paddingLeft: percentWidth(2)
            // }}
            renderItem={({ item, index }) => (
              <TouchableFocus
                onPress={() =>
                  navigation.navigate('albums', {
                    musics: item.musics,
                    poster: item.poster,
                    album: item.album,
                  })
                }
                style={{
                  height: percentHeight(18),
                  width: percentWidth(16),
                  // marginRight: percentWidth(2),
                  marginTop: 10,
                  marginBottom: 10,
                  borderWidth: 1,
                  borderColor: colors.primary,
                  paddingHorizontal: percentWidth(1),
                  paddingVertical: percentHeight(1),
                  borderRadius: 5,
                }}
                // focusedStyle={{
                //   transform: [{ scale: SCALE_PARAM }]
                // }}
                focusedStyle={{
                  borderColor: colors.dark_red,
                }}
                // nextFocusRightSelf={((index + 1) == musics.length)}
                nextFocusRightSelf={((index + 1) == musics.slice(0, 25).length)}
                nextFocusLeft={(!index ? menuRef : null)}
              >
                <Image
                  source={{
                    uri: `${IPTV_ADMIN_URL}/posters/music/${item.poster}`,
                  }}
                  style={{
                    height: '100%',
                    width: '100%',
                    borderRadius: 5,
                    resizeMode: 'cover',
                  }}
                />
              </TouchableFocus>
            )}
            // keyExtractor={(item) => item..toString()}
            horizontal={true}
          />
        </View>
      ) : null}
      {radios.length > 0 ? (
        <View
          style={{
            marginTop: 20,
            height: percentHeight(30),
            marginBottom: percentHeight(10),
          }}>
          <Text style={{ fontSize: fSize(7), color: colors.secondary, paddingLeft: percentWidth(2) }}>
            Популярные радиостанции
          </Text>
          <FlatList
            data={radios.slice(0, 25)}
            // contentContainerStyle={{
            //   paddingLeft: percentWidth(2)
            // }}
            renderItem={({ item, index }) => (
              <TouchableFocus
                onPress={() =>
                  navigation.navigate('radio', { selectedRadioId: item.id })
                }
                style={{
                  height: percentHeight(18),
                  width: percentWidth(16),
                  // marginRight: percentWidth(2),
                  marginTop: 10,
                  marginBottom: 10,
                  borderWidth: 1,
                  borderColor: colors.primary,
                  borderRadius: 5,
                  paddingHorizontal: percentWidth(1),
                  paddingVertical: percentHeight(1),
                }}
                // focusedStyle={{
                //   transform: [{ scale: SCALE_PARAM }]
                // }}
                focusedStyle={{
                  borderColor: colors.dark_red,
                }}
                nextFocusDownSelf
                // nextFocusRightSelf={((+index + 1) == radios.length)}
                nextFocusRightSelf={((+index + 1) == radios.slice(0, 25).length)}
                nextFocusLeft={(!index ? menuRef : null)}
              >
                <Image
                  source={{ uri: item.image }}
                  style={{
                    width: '100%',
                    height: '100%',
                    borderRadius: percentWidth(0.5),
                    resizeMode: 'cover',
                  }}
                />
              </TouchableFocus>
            )}
            keyExtractor={(radio) => radio.id.toString()}
            horizontal={true}
          />
        </View>
      ) : null}
    </ScrollView>
  );
};
export default MainScreen;

const styles = StyleSheet.create({
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    height: percentHeight(10),
    width: percentWidth(12),
    backgroundColor: '#272536',
    marginRight: 6,
    borderRadius: 5,
    marginRight: percentWidth(2),
    borderWidth: 1,
    borderColor: colors.primary,
  },
  boxShadow: {
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.20,
    shadowRadius: 1.41,
    elevation: 2,
  }
});
