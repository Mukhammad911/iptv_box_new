import React, { useEffect, useState } from 'react';
import { globalStyles } from '../templates/globalStyles';
import {
  View,
  TouchableOpacity,
  Text,
  TextInput,
  ScrollView,
  Image,
  FlatList
} from 'react-native';

// Токенизация проведена

import { request_get } from '../api/request';
import Loader from '../components/Loader';
import ButtonsNavigation from '../components/General/ButtonsNavigation';
import Icon from 'react-native-vector-icons/Ionicons';
import { colors } from '../templates/colors';
import { fSize, percentHeight, percentWidth } from '../templates/helper';
import { IPTV_ADMIN_URL } from '../api/url';
import AsyncStorage from '@react-native-community/async-storage';
import Favorite from '../components/General/Favorite';
import ScreenHeader from '../components/ScreenHeader';
import { VirtualizedList } from 'react-native';
import { DEFAULT_SCALE, SCALE_PARAM } from '../templates/scaleParameters';
import FocusableOpacity from '../components/FocusableOpacity';
import { useDispatch, useSelector } from "react-redux";
import SidNotFound from '../components/SidNotFound';
import TouchableFocus from '../components/TouchableFocus';
import { chechToken } from "../components/helper";
import { setAuthState, setSid } from "../redux/actions";
import sidValidation from "../services/sidChecker";

const GroupChannelsScreen = ({ navigation }) => {
  const sid = useSelector((state) => state.authReducer.sid);
  const [groupChannels, setGroupChannels] = useState([]);
  const [loader, setLoader] = useState(true);
  const [type, setType] = useState("");
  const [searchShow, setSearchShow] = useState(false);
  const [searchText, setSearchText] = useState('');
  const [searchCheck, setSearchCheck] = useState(false);
  const [channels, setChannels] = useState([]);

  const [searchButtonRef, setSearchButtonRef] = useState(null);

  const [page, setPage] = useState(1);
  const [listloader, setListLoader] = useState(false);
  const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 20;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  const dispatch = useDispatch();

  //Here we will check the access token
  useEffect(() => {
    const ff = async () => {
      let result = await chechToken()
      if (result) {
        dispatch(setAuthState(true))
      } else {
        dispatch(setAuthState(false))
      }
    }
    ff()
  }, []);

  //Here we will check the sid
  useEffect(() => {
    AsyncStorage.getItem('sid')
      .then((sid) => sidValidation(sid))
      .then(async ({ success, sid }) => {
        if (success) {
          dispatch(setSid(sid));
        } else {
          await AsyncStorage.removeItem('sid');
          navigation.navigate('subscribes');
        }
      })

  }, []);

  // -------------------------------------------------------------------------------------------------------------

  const [focusedChannel, setFocusedChannel] = useState(Infinity)

  // -------------------------------------------------------------------------------------------------------------

  const menuRef = useSelector(state => state.refReducer.menuRef);

 /* useEffect(async() => {
    const response = await request_get('group_channels');
    setType(response.data[0].id)
  },[])*/


  useEffect(() => {
    const preRender = async () => {
      navigation.setOptions({
        headerRight: () => (
          <View style={{ flexDirection: 'row' }}>
            <TouchableOpacity
              style={{ marginRight: 10 }}
              onPress={() => {
                searchShow ? Search() : null;
                setSearchShow(!searchShow);
                setSearchCheck(true);
              }}>
              <Icon
                name={'search-outline'}
                size={25}
                color={colors.header_icons_color}
              />
            </TouchableOpacity>
          </View>
        ),
        headerTitle: () =>
          searchShow ? (
            searchInput()
          ) : (
            <Text style={{ color: colors.secondary, fontSize: fSize(23) }}>
              Телеканалы
            </Text>
          ),
        headerShown: false,
      });
        await getData();
        await getChannels();
    }
    preRender()

  }, [sid]);

  const getData = async () => {
    const response = await request_get('group_channels');
    setGroupChannels(response.data);
  };

  const getChannels = async (asyncPage = undefined) => {

    setChannels([]);
    setLoader(true);
    // const sid = await AsyncStorage.getItem('sid');
    const response = await request_get(
      `getChannels/${type}?search=${searchText}&sid=${sid}&page=${(asyncPage || page)}`,
    );
    setChannels([...channels, ...response.data]);
    // setPage(page + 1);

   // setPage(Math.ceil(response.data.length / 10) + 1);
    setLoader(false);
  };

  useEffect(() => {
  }, [page])

  const paginate = async (type_move) => {

    let _page = page;

    switch (type_move){
      case 'bottom':
        _page = _page + 1;
      break;
      case  'top':
        _page = _page - 1;
      break;
    }

    // const sid = await AsyncStorage.getItem('sid');
    const response = await request_get(
      `getChannels/${type}?search=${searchText}&sid=${sid}&page=${_page}`,
    );
    if (!response.success) {
      return;
    }

    if(response.data.length == 0){
      return;
    }

    setListLoader(true);

    let channelsPaginateArray = [...channels, ...response.data];

    setChannels(channelsPaginateArray);
    setPage(_page);
    setListLoader(false);

  };

  const Search = () => {
    setPage(1);
    setSearchCheck(true);
    // getChannels(1);
    setSearchText('');
    setSearchShow(false);
  };

  const searchInput = () => {
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center' }}>
        <TouchableFocus
          onPress={() => setSearchShow(false)}
          focusedStyle={{
            transform: [{ scale: 1.3 }]
          }}
        >
          <Icon
            name={'arrow-back-outline'}
            size={25}
            color={colors.header_icons_color}
          />
        </TouchableFocus>
        <TextInput
          placeholder={'Поиск'}
          placeholderTextColor={colors.header_icons_color}
          autoFocus={true}
          onSubmitEditing={() => {
            // setSearchShow(false);
            // setSearchCheck(true);
            Search();
          }}
          onChangeText={(text) => setSearchText(text)}
          value={searchText}
          style={{ color: colors.header_icons_color }}
        />
      </View>
    );
  };

  const changeNameChannel = (name) => {
    if (!name) {
      return " "
    }
    let arr_name = name.split('');
    if (arr_name.length < 10) {
      return name;
    }
    let cutname = arr_name.splice(0, 9).join('');

    return cutname + ' ...';
  };

  const channel = (item, index) => {
    return (
      <TouchableFocus
        key={item.id}
        style={{
          flexDirection: 'column',
          height: percentHeight(30),
          marginVertical: 10,
          marginLeft: percentWidth(1),
          marginRight: percentWidth(1),
          borderWidth: 1,
          borderColor: colors.primary,
          borderRadius: 5,
        }}
        focusedStyle={{
          // transform: [{ scale: SCALE_PARAM }],
          borderColor: colors.dark_red,
          // backgroundColor: colors.secondary
        }}
        focusedOnStart={!index}
        onPress={() =>
          navigation.navigate('player', {
            id: item.id,
            group_channel_id: item.group_channel_id,
            selectedChannelIndex: index,
            channelsArray: channels,
            groupChannelsOut : groupChannels
          })
        }
        nextFocusRightSelf={!((index + 1) % 5)}
        nextFocusLeft={(!(index % 5) ? menuRef : null)}
        nextFocusDownSelf={((channels.length - index) <= 5)}
      >
        <View style={{ flexDirection: 'column', height: percentHeight(20) }}>
          <View
            style={{
              width: percentWidth(15),
              height: percentHeight(18),
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#2C2A3C',
              borderRadius: 6,
              overflow: 'hidden'
            }}>
            <Image
              style={{ height: '100%', width: '100%' }}
              source={{ uri: `${IPTV_ADMIN_URL}images/channel/${item.image}` }}
              resizeMode={'center'}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'flex-start',
              width: percentWidth(10),
              marginLeft: 10,
            }}>
            <View>
              <Text style={{ color: colors.secondary, fontSize: fSize(6) }}>
                {changeNameChannel(item.name)}
              </Text>
              <Text
                style={{
                  color: colors.dark_text,
                  fontSize: fSize(4),
                  textAlign: 'left'
                }}>
                {changeNameChannel(item.program_name)}
              </Text>
              <Text
                style={{
                  color: colors.dark_text,
                  fontSize: fSize(4),
                  textAlign: 'left'
                }}>
                {item.program_time}
              </Text>
            </View>
          </View>
        </View>
      </TouchableFocus>
    );
  };

  const changeGroup = async (item_id) => {
    console.log("item_id 1", item_id)
    setPage(1);
    setType(item_id)

    setChannels([]);
    setLoader(true);

    const response = await request_get(
      `getChannels/${item_id}?search=${searchText}&sid=${sid}&page=1`,
    );
    setChannels(response.data);
    setLoader(false);
  }

  return !sid ? <SidNotFound /> : (
    <View style={{ ...globalStyles.container, paddingLeft: 10, paddingBottom: percentHeight(5) }}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between'
        }}
      >
        {
          searchShow ? (
            searchInput()
          ) : (
            <ScreenHeader screenName="Телеканалы" />
          )
        }
        <View style={{ flexDirection: 'row' }}>
          <TouchableFocus
            style={{ marginRight: 10 }}
            focusedStyle={{
              transform: [{ scale: 1.3 }]
            }}
            setRef={setSearchButtonRef}
            onPress={() => {
              // if (searchShow) {
              //   Search()
              // }
              searchShow ? Search() : setSearchShow(true);
              // setSearchShow(!searchShow);
              // setSearchCheck(true);
            }}
            nextFocusLeftSelf
            nextFocusUpSelf
            nextFocusRightSelf
          >
            <Icon
              name={'search-outline'}
              size={25}
              color={colors.header_icons_color}
            />
          </TouchableFocus>
        </View>
      </View>
      {loader ? <Loader /> : null}
      {!searchShow ? (
        <>
          <ButtonsNavigation
            data={groupChannels}
            setLoader={setLoader}
            setType={setType}
            type={type}
            Default={true}
            setFilterCheck={setSearchCheck}
            onChange={(item_id) => changeGroup(item_id)}
            // blockFocusUp
            blockFocusRight
            nextFocusUpRef={searchButtonRef}
          />
          <ScrollView
            style={{
              paddingVertical: percentHeight(0),
              paddingBottom: percentHeight(14),
              marginBottom: listloader ? percentHeight(10) : 0,
              // backgroundColor: 'yellow',
              // height: percentHeight(80)
            }}
            onScroll={async ({ nativeEvent }) => {
              console.log('didn`t got here', (page - 1) * 10, page, channels.length);
              // alert((channels.length == ((page - 1) * 10)))
              if (isCloseToBottom(nativeEvent)) {

                await paginate('bottom');
              }
            }}>
            <FlatList
              numColumns={5}
              data={channels}
              contentContainerStyle={{
                paddingTop: percentHeight(3),
              }}
              keyExtractor={(item, index) => index.toString()}
              renderItem={({ item, index }) => channel(item, index)}
            />
          </ScrollView>
          {listloader && (
            <View
              style={{
                flex: 1,
                position: 'absolute',
                bottom: percentHeight(3),
                justifyContent: 'center',
                left: 0,
                right: 0,
              }}>
              <Loader />
            </View>
          )}
        </>
      ) : null}
    </View>
  );
};
export default GroupChannelsScreen;
