import { VLCPlayer } from '@nghinv/react-native-vlc';
import React, { useEffect, useLayoutEffect, useState } from 'react';
import { StyleSheet } from 'react-native';
import { Image } from 'react-native';
import { View, Text, TouchableOpacity } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import { useDispatch, useSelector } from "react-redux";
import { request_get } from '../api/request';
import FocusableOpacity from '../components/FocusableOpacity';
import ScreenHeader from '../components/ScreenHeader';
import SidNotFound from '../components/SidNotFound';
import TouchableFocus from '../components/TouchableFocus';
import { colors } from '../templates/colors';
import { fSize, percentHeight, percentWidth } from '../templates/helper';
import { DEFAULT_SCALE, SCALE_PARAM } from '../templates/scaleParameters'
import { chechToken } from "../components/helper";
import { setAuthState, setSid } from "../redux/actions";
import AsyncStorage from "@react-native-community/async-storage";
import sidValidation from "../services/sidChecker";

const RadioScreen = ({ navigation, route }) => {
  const selectedRadioId = route.params?.selectedRadioId;
  const sid = useSelector((state) => state.authReducer.sid);
  const [radios, setRadios] = useState([]);
  const [filteredRadios, setFilteredRadios] = useState([]);
  const [radioGroup, setRadioGroup] = useState([]);
  const [selectedRadioGroup, setSelectedRadioGroup] = useState(0);
  const [currentPlayingRadio, setCurrentPlayingRadio] = useState({
    url: null,
    name: '',
  });
  const [play, setPlay] = useState(false);

  const menuRef = useSelector(state => state.refReducer.menuRef);

  // ----------------------------------------------------------------------------------------------------------------------------------------------

  const [firstRadioGroupButtonRef, setFirstRadioGroupButtonRef] = useState(null)
  const [radioPlayButtonRef, setRadioPlayButtonRef] = useState(null)
  const [radioCloseButtonRef, setRadioCloseButtonRef] = useState(null)

  // ----------------------------------------------------------------------------------------------------------------------------------------------

  const dispatch = useDispatch();

  //Here we will check the access token
  useEffect(() => {
    const ff = async () => {
      let result = await chechToken()
      if (result) {
        dispatch(setAuthState(true))
      } else {
        dispatch(setAuthState(false))
      }
    }
    ff()
  }, []);

  //Here we will check the sid
  useEffect(() => {
    AsyncStorage.getItem('sid')
      .then((sid) => sidValidation(sid))
      .then(async ({ success, sid }) => {
        if (success) {
          dispatch(setSid(sid));
        } else {
          await AsyncStorage.removeItem('sid');
          navigation.navigate('subscribes');
        }
      })

  }, []);

  useLayoutEffect(() => {
    navigation.setOptions({
      headerStyle: {
        backgroundColor: colors.primary,
      },
      headerTintColor: colors.secondary,
      headerTitle: 'Радио',
    });
  }, []);

  useEffect(() => {
    if (selectedRadioId) {
      const selectedRadio = radios.find((radio) => radio.id == selectedRadioId);
      if (selectedRadio?.id) {
        getRadioUrl(selectedRadio.id, selectedRadio.name);
      }
    }
  }, [selectedRadioId, radios]);

  const getRadioUrl = (id, name) => {
    request_get(`api/radio_url/${id}/${sid}`).then(({ data }) => {
      setCurrentPlayingRadio({
        url: data,
        name,
      });
    });
  };
  useEffect(() => {
    let filteredRadios = radios.filter(
      (radio) => +radio.radio_group_id === +selectedRadioGroup,
    );
    if (selectedRadioGroup === 0) {
      filteredRadios = radios;
    }
    setFilteredRadios(filteredRadios);
  }, [selectedRadioGroup]);

  useEffect(() => {
    if (sid) {
      request_get(`api/radios/${sid}`).then(({ data }) => {
        data.unshift({
          id: 0,
          name: 'Все радиостанции',
        });
        setRadioGroup(data);
        const radios = data
          .map((radioGroup) => radioGroup?.radios)
          .filter(Boolean);
        setRadios(radios.flat(1));
        setFilteredRadios(radios.flat(1));
      });
    }
  }, [sid]);

  const _onError = (er) => {
    console.log(er);
  }
  return !sid ? <SidNotFound /> : (
    <View
      style={{
        // flex: 1,
        backgroundColor: colors.primary,
        paddingLeft: percentWidth(1),
        paddingRight: percentWidth(1),
        height: '100%'
      }}>
      {currentPlayingRadio.url ? (
        <View
          style={{
            backgroundColor: '#2C293D',
            height: percentHeight(15),
          }}>
          <VLCPlayer
            source={{ uri: currentPlayingRadio.url }}
            volume={100}
            resume={!play}
            onError={_onError}
          />
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: percentWidth(4),
            }}>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center'
              }}
            >
              <TouchableFocus
                focusedStyle={{
                  transform: [{ scale: 1.3 }]
                }}
                onPress={() => setPlay(!play)}
                style={{
                }}
                setRef={setRadioPlayButtonRef}
                nextFocusLeftSelf
                nextFocusUpSelf
                nextFocusRight={radioCloseButtonRef}
                nextFocusDown={firstRadioGroupButtonRef}
              >
                {!play ? (
                  <Icon
                    name="pause-outline"
                    size={fSize(10)}
                    color={colors.secondary}
                  />
                ) : (
                  <Icon
                    name="play"
                    size={fSize(10)}
                    color={colors.secondary}
                  />
                )}
              </TouchableFocus>

              <Text
                style={{
                  fontWeight: 'bold',
                  color: colors.secondary,
                  fontSize: fSize(7),
                  marginLeft: percentWidth(4)
                }}>
                {currentPlayingRadio.name}
              </Text>
            </View>
            <TouchableFocus
              focusedStyle={{
                transform: [{ scale: 1.3 }]
              }}
              onPress={() => {
                setCurrentPlayingRadio({
                  url: null,
                  name: '',
                })
                firstRadioGroupButtonRef.setNativeProps({
                  hasTVPreferredFocus: true
                })
              }
              }
              setRef={setRadioCloseButtonRef}
              nextFocusUpSelf
              nextFocusRightSelf
              nextFocusLeft={radioPlayButtonRef}
              nextFocusDown={firstRadioGroupButtonRef}
            >
              <Icon
                name="close-outline"
                size={fSize(10)}
                color={colors.dark_text}
              />
            </TouchableFocus>
          </View>
        </View>
      ) : null}
      <ScreenHeader screenName="Радио" />
      <View
        style={{
          height: percentHeight(9),
          paddingLeft: percentWidth(1)
        }}
      >
        <FlatList
          data={radioGroup}
          keyExtractor={(item) => item.id.toString()}
          horizontal={true}
          contentContainerStyle={{
            // justifyContent: 'flex-start',
            // alignItems: 'flex-start',
            paddingLeft: percentWidth(2)
          }}
          renderItem={({ item, index }) => {
            return (
              <TouchableFocus
                style={
                  item.id === selectedRadioGroup
                    ? { ...styles.vodTypes, ...styles.active }
                    : { ...styles.vodTypes }
                }
                focusedStyle={{
                  transform: [{ scale: SCALE_PARAM }]
                }}
                onPress={() => {
                  setSelectedRadioGroup(item.id);
                }}
                setRef={!index ? setFirstRadioGroupButtonRef : (() => null)}
                nextFocusUp={(currentPlayingRadio.url && (index >= 0 && index <= 5)) ? radioPlayButtonRef : null}
                nextFocusUpSelf={!currentPlayingRadio.url}
                nextFocusRightSelf={((index + 1) == radioGroup.length)}
              >
                <Text
                  style={
                    item.id == selectedRadioGroup ?
                      { ...styles.text, ...styles.textActive }
                      :
                      { ...styles.text }
                  }
                >
                  {item.name}
                </Text>
              </TouchableFocus>
            );
          }}
        />
      </View>

      <View
        style={{
          // top: percentHeight(11),
          height: currentPlayingRadio.url
            ? percentHeight(60)
            : percentHeight(70),
          // position: 'absolute',
        }}>
        <FlatList
          numColumns={5}
          // showsVerticalScrollIndicator={false}
          // showsHorizontalScrollIndicator={false}
          data={filteredRadios}
          contentContainerStyle={{
            // flexDirection: 'row',
            // flexWrap: 'wrap',
            paddingLeft: percentWidth(1),
            paddingTop: percentHeight(1)
          }}
          keyExtractor={(item) => item.id.toString()}
          renderItem={({ item, index }) => {
            return (
              <TouchableFocus
                style={{
                  margin: 5,
                  // flex: 1,
                  // marginBottom: percentHeight(3),
                  borderWidth: 1,
                  borderColor: colors.primary,
                  padding: percentWidth(1),
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 10
                }}
                focusedStyle={{
                  // transform: [{ scale: SCALE_PARAM }],
                  borderColor: colors.dark_red,
                }}
                focusedOnStart={index ? false : true}
                onPress={() => getRadioUrl(item.id, item.name)}
                nextFocusRightSelf={!((index + 1) % 5)}
                nextFocusDownSelf={((filteredRadios.length - index) < 6)}
                nextFocusLeft={(!(index % 5) ? menuRef : null)}
              >
                <Image
                  source={{ uri: item.image }}
                  style={{
                    height: percentHeight(17),
                    width: percentWidth(14),
                    borderRadius: percentWidth(1),
                    resizeMode: 'cover',
                  }}
                />
                {/* <View
                  style={{
                    marginLeft: percentWidth(5),
                    marginTop: percentHeight(4),
                  }}>
                  <Text
                    style={{
                      color: colors.secondary,
                      fontWeight: 'bold',
                    }}>
                    {item.name}
                  </Text>
                </View> */}
              </TouchableFocus>
            );
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  vodTypes: {
    marginVertical: 10,
    marginHorizontal: 5,
    borderRadius: 18,
    paddingHorizontal: 10,
    justifyContent: 'center',
    alignItems: 'center',
    height: percentHeight(5),
  },
  active: {
    backgroundColor: colors.dark_red,
  },
  text: {
    color: colors.dark_text,
    fontSize: fSize(7)
  },
  textActive: {
    color: colors.secondary,
  }
});
export default RadioScreen;
