import React, {useEffect, useLayoutEffect, useState} from 'react';
import {View, Text, FlatList} from 'react-native';
import {useSelector} from 'react-redux';
import {request_get} from '../api/request';
import FilmCard from '../components/films/FilmCard';
import {colors} from '../templates/colors';

const NewFilmScreen = ({navigation}) => {
  const sid = useSelector((state) => state.authReducer.sid);
  const [films, setFilms] = useState([]);
  useLayoutEffect(() => {
    navigation.setOptions({
      title: 'Популярное',
    });
  }, []);

  useEffect(() => {
    const getFilms = async () => {
      try {
        const {data} = await request_get(`vod_datas/${sid}/1?top=1`);
        setFilms(data);
      } catch (error) {
      }
    };
    getFilms();
  }, []);

  return (
    <View style={{flex: 1, backgroundColor: colors.primary}}>
      <FlatList
        numColumns={3}
        data={films}
        renderItem={({item}) => (
          <FilmCard item={item} navigation={navigation} />
        )}
        keyExtractor={(item) => item.id.toString()}
      />
    </View>
  );
};

export default NewFilmScreen;
