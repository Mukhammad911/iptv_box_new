import React, { useEffect, useState } from 'react';
import { colors } from '../templates/colors';
import Favorite from '../components/Profile/Favorite';
import { View, Button, TouchableOpacity, TextInput } from 'react-native';
import ButtonsNavigation from '../components/General/ButtonsNavigation';
import Icon from 'react-native-vector-icons/Ionicons';
import FavoritesMusicScreen from './FavoritesMusicScreen';
import { fSize, percentHeight, percentWidth } from '../templates/helper';
import ScreenHeader from '../components/ScreenHeader';
import AsyncStorage from '@react-native-community/async-storage';
import { request_get } from '../api/request';
import { useSelector } from 'react-redux';
import FocusableOpacity from '../components/FocusableOpacity';
import { Text } from 'react-native';
import TouchableFocus from '../components/TouchableFocus';

const FavoriteScreen = ({ navigation }) => {
  const vodTypes = [
    { id: 'all', name: 'Общие' },
    { id: 'films', name: 'Фильмы' },
    { id: 'channels', name: 'Телеканалы' },
    { id: 'musics', name: 'Музыка' },
  ];
  const [type, setType] = useState({
    title: 'Общие',
    id: 'all',
  });
  const [data, setData] = useState({
    channels: [],
    films: [],
  });
  const [musics, setMusics] = useState([])
  const [loader, setLoader] = useState(true);
  const [searchCheck, setSearchCheck] = useState(false);
  const [searchText, setSearchText] = useState('');
  const [searchShow, setSearchShow] = useState(false);
  const { sid } = useSelector((state) => state.authReducer);
  const [hasData, setHasData] = useState(true)

  const [goBackButtonRef, setGoBackButtonRef] = useState(null)
  const [searchButtonRef, setSearchButtonRef] = useState(null)
  const [navigationButtonRef, setNavigationButtonRef] = useState(null)
  const [firstFocusedButtonRef, setFirstFocusedButtonRef] = useState(null)

  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableFocus
          focusedOnStart
          style={{
            marginLeft: percentWidth(2),
            width: percentWidth(3.5),
            height: percentWidth(3.5),
            borderRadius: 100,
            justifyContent: 'center',
            alignItems: 'center'
          }}
          focusedStyle={{
            backgroundColor: colors.dark_red
          }}
          onPress={() => navigation.goBack()}
          setRef={setGoBackButtonRef}
          nextFocusLeftSelf
          nextFocusUpSelf
          nextFocusRightSelf
          nextFocusRight={searchButtonRef}
        >
          <Icon
            name="arrow-back-outline"
            size={20}
            color="white"
          />
        </TouchableFocus>
      ),
      title: 'Избранные',
      // headerRight: () => (
      //   <View
      //     style={{
      //       width: percentWidth(60),
      //       flexDirection: 'row',
      //       justifyContent: 'flex-end',
      //       alignItems: "center",
      //       // backgroundColor: 'yellow'
      //     }}
      //   >
      //     {
      //       searchShow ?
      //         searchInput()
      //         :
      //         null
      //     }
      //     <TouchableFocus
      //       style={{
      //         marginRight: percentWidth(2),
      //         width: percentWidth(3.5),
      //         height: percentWidth(3.5),
      //         borderRadius: 100,
      //         justifyContent: 'center',
      //         alignItems: 'center',
      //       }}
      //       focusedStyle={{
      //         backgroundColor: colors.dark_red,
      //       }}
      //       onPress={() => {
      //         searchShow ? Search() : null;
      //         setSearchShow(!searchShow);
      //         setSearchCheck(true);
      //       }}
      //       setRef={setSearchButtonRef}
      //       nextFocusUpSelf
      //       nextFocusRightSelf
      //     >
      //       <Icon
      //         name={'search-outline'}
      //         size={20}
      //         color={colors.secondary}
      //       />
      //     </TouchableFocus>
      //   </View>
      // ),
      headerShown: true
    });
  });
  const searchInput = () => {
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center', paddingTop: percentHeight(5), paddingLeft: percentWidth(2), paddingBottom: percentHeight(3) }}>
        <TextInput
          placeholder={'Поиск'}
          placeholderTextColor={colors.header_icons_color}
          // placeholderTextColor={'white'}
          autoFocus={true}
          onChangeText={(text) => setSearchText(text)}
          value={searchText}
          style={{ color: colors.header_icons_color, backgroundColor: 'red' }}
          onSubmitEditing={() => {
            Search()
            setSearchShow(!searchShow);
            setSearchCheck(true);
          }}
        />
      </View>
    );
  };
  const getData = async () => {
    try {
      setLoader(true);
      setHasData(true);
      setData({ channels: [], films: [] });
      // const sidd = await AsyncStorage.getItem('sid');
      const response = await request_get(
        `favorites/${type.id}?sid=${sid}&search=${searchText}`,
      );
      switch (type.id) {
        case 'all':
          if (!response['channels'].length && !response['films'].length) {
            setHasData(false);
            return;
          }
          break;
        case 'films':
          if (!response['films'].length) {
            setHasData(false);
            return;
          }
          break;
        case 'channels':
          if (!response['channels'].length) {
            setHasData(false);
            return;
          }
          break;
      }
      // if (!response.data.length) {
      //   setHasData(false);
      //   return
      // }
      setHasData(true)
      setData({ ...data, channels: response.channels, films: response.films });
      setLoader(false);
    } catch {
      setLoader(false)
      setHasData(false)
    }
  };

  const getDataMusics = async () => {
    try {
      setLoader(true);
      setHasData(true);
      const response = await request_get(`musics_favorite/${sid}`);
      if (!response.data.length) {
        setHasData(false);
        return;
      }
      const vodfiles = response.data
        .map((music) => ({
          id: music.id,
          music: music.music_vodfiles,
          poster: music.poster,
          genr: music.genr,
        }))
        .filter((item) => item.music.file_torrent.toString().toLowerCase().includes(searchText.toLowerCase()));
      setMusics(vodfiles);
      setLoader(false);
    } catch {
      setHasData(false)
      setLoader(false);
    }
  };

  const Search = () => {
    type.id == 'musics' ? getDataMusics() : getData();
    setSearchText('');
  };

  useEffect(() => {
    if (!searchShow && !searchCheck) {
      type.id == 'musics' ? getDataMusics() : getData()
    }
  }, [searchShow, searchText, type.id]);

  return (
    <View style={{ backgroundColor: colors.primary, flex: 1 }}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center'
        }}
      >
        {/* {
          !searchShow ?
            <ScreenHeader screenName="Избранные" />
            :
            searchInput()
        } */}
        {/* <FocusableOpacity
          style={{ marginRight: percentWidth(2) }}
          focusedStyle={{
            transform: [{ scale: 1.3 }]
          }}
          onPress={() => {
            searchShow ? Search() : null;
            setSearchShow(!searchShow);
            setSearchCheck(true);
          }}>
          <Icon
            name={'search-outline'}
            size={20}
            color={colors.header_icons_color}
          />
        </FocusableOpacity> */}
      </View>
      <ButtonsNavigation
        data={vodTypes}
        type={type}
        setType={setType}
        setFilterCheck={setSearchCheck}
        setLoader={setLoader}
        setRef={setNavigationButtonRef}
        blockFocusRight
        blockFocusLeft
        nextFocusUpRef={goBackButtonRef}
        nextFocusDownRef={firstFocusedButtonRef}
        blockFocusDown={((type.id == 'musics') ? !musics.length : (type.id == 'films' ? !data.films.length : (type.id == 'channels' ? !data.channels.length : (!data.channels.length && !data.films.length))))}
      />
      {!hasData ? (
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Text
            style={{
              color: 'white',
              textAlign: 'center',
              fontSize: fSize(9),
              marginTop: -percentHeight(10)
            }}
          >
            Нет избранных
          </Text>
        </View>
      ) : type.id !== 'musics' ? (
        <Favorite
          route={`favorites/${type.id}`}
          navigation={navigation}
          setSearchCheck={setSearchCheck}
          searchCheck={searchCheck}
          setLoader={setLoader}
          loader={loader}
          searchShow={searchShow}
          setSearchShow={setSearchShow}
          searchText={searchText}
          setSearchText={setSearchText}
          data={data}
          firstFocusedButtonRef={firstFocusedButtonRef}
          setFirstFocusedButtonRef={setFirstFocusedButtonRef}
          navigationButtonRef={navigationButtonRef}
        />
      ) : (
        <FavoritesMusicScreen
          navigation={navigation}
          setSearchCheck={setSearchCheck}
          searchCheck={searchCheck}
          setLoader={setLoader}
          loader={loader}
          musics={musics}
          firstFocusedButtonRef={firstFocusedButtonRef}
          setFirstFocusedButtonRef={setFirstFocusedButtonRef}
        />
      )}
    </View>
  );
};
export default FavoriteScreen;
