import React, { useEffect, useState } from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  DeviceEventEmitter,
  Modal,
  Text,
  TouchableOpacity,
  TouchableHighlight,
  StatusBar,
} from 'react-native';
import { request_get } from '../api/request';
import AsyncStorage from '@react-native-community/async-storage';
import Notify from '../components/Notify';
import * as actions from '../redux/actions';
import { useDispatch, useSelector } from 'react-redux';
import { bindActionCreators } from 'redux';
import { colors } from '../templates/colors';
import Loader from '../components/Loader';
// import TvPlayer from '../components/TvPlayer';
import TvPlayer from '../components/TvPlayer2';
import FilmDetails from '../components/films/FilmDeatils';
import FilmSeries from '../components/films/FilmSeries';
import { globalStyles } from '../templates/globalStyles';
import { fSize, percentHeight, percentWidth } from '../templates/helper';
import Icon from 'react-native-vector-icons/Ionicons';
import DropDownPicker from 'react-native-dropdown-picker';
import { chechToken, items } from "../components/helper";
import MoviesInfo from '../components/MoviesInfo';
import { setAuthState, setSid } from "../redux/actions";
import sidValidation from "../services/sidChecker";

const VPlayerScreen = ({ navigation, route }) => {
  const dispatch = useDispatch();
  const { auth_false } = bindActionCreators(actions, dispatch);
  const [data, setData] = useState([]);
  const [details, setDetails] = useState({});
  const [full, setFull] = useState(false);
  const [showPlayer, setShowPlayer] = useState(false)

  const [loader, setLoader] = useState(true);
  const [uri, setUri] = useState(null);
  const [showSeries, setShowSeries] = useState(false);
  const [modalVisible, setModalVisible] = useState(false);
  const [play, setPlay] = useState(false);
  const [defaultSeries, setDefaultSeries] = useState({ id: '', file_Server: '' });
  const formats = [{ id: 'standart' }, { id: 'HD' }, { id: 'FullHD' }, { id: 'UHD' }];
  const [format, setFormat] = useState('standart');

  const sid = useSelector((state) => state.authReducer.sid)

  //Here we will check the access token
  useEffect(() => {
    const ff = async () => {
      let result = await chechToken()
      if (result) {
        dispatch(setAuthState(true))
      } else {
        dispatch(setAuthState(false))
      }
    }
    ff()
  }, []);

  //Here we will check the sid
  useEffect(() => {
    AsyncStorage.getItem('sid')
      .then((sid) => sidValidation(sid))
      .then(async ({ success, sid }) => {
        if (success) {
          dispatch(setSid(sid));
        } else {
          await AsyncStorage.removeItem('sid');
          navigation.navigate('subscribes');
        }
      })

  }, []);

  useEffect(() => {
    DeviceEventEmitter.addListener('goBack', (e) => {
      getData();
    });
    navigation.setOptions({
      headerShown: false,
    });
    if (sid) {
      getData();
    }
  }, []);

  useEffect(() => {
    navigation.setOptions({
      headerShown: false
    })
  }, [showPlayer])

  const getData = async () => {
    const sid = await AsyncStorage.getItem('sid');
    const response = await request_get(
      `get_series_list/${route.params.id}/${route.params.format}?sid=${sid}`,
    );
    setDetails(response.details);
    setLoader(false);

    if (response.details.type == 1 || response.details.type == 4) {
      setUri(response.data);
      setShowSeries(false);
    } else {
      setData(Object.entries(response.data));
      if (response.default) {
        setDefaultSeries(response.default);
      }

      setShowSeries(true);
    }
  };

  return showPlayer ?
    (<View style={styles.container}>
      {loader ? (
        <Loader />
      ) : (
        <>
          <View style={styles.videoFull}>
            <TvPlayer
              uri={uri}
              onFull={setFull}
              navigation={navigation}
              back={navigation.goBack}
              onPlay={setPlay}
              vidid={details.id}
              vidName={details.name}
              setShowPlayer={() => setShowPlayer(false)}
            />
          </View>
        </>
      )}
    </View>)
    :
    (
      <View
        style={{
          flex: 1,
          backgroundColor: colors.primary
        }}
      >
        {loader ? (
          <Loader />
        ) : (
          <MoviesInfo details={details} format={route.params.format || 'standart'} data={data} setUri={setUri} setShowPlayer={setShowPlayer} navigation={navigation} />
        )
        }
      </View>
    )
};

export default VPlayerScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  videoFull: {
    flex: 1,
  },
  video: {
    height: percentHeight(70),
  },
  listItem: {
    height: 50,
    borderBottomWidth: 1,
    borderBottomColor: colors.gray,
    justifyContent: 'center',
    paddingVertical: 10,
    paddingLeft: 10,
  },
  active: {
    backgroundColor: colors.blue,
  },
});
