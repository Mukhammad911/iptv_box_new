import React, {useEffect, useState} from 'react';
import {globalStyles} from '../templates/globalStyles';
import {View, FlatList} from 'react-native';

import ListItem from '../components/ListItems';
import {request_get} from '../api/request';
import Loader from '../components/Loader';
import Channel from '../components/Channel';
import { useDispatch } from "react-redux";
import { chechToken } from "../components/helper";
import { setAuthState, setSid } from "../redux/actions";
import AsyncStorage from "@react-native-community/async-storage";
import sidValidation from "../services/sidChecker";

const ChannelsScreen = ({navigation, route}) => {
  const [channels, setChannels] = useState([]);
  const [loader, setLoader] = useState(true);

  useEffect(() => {
    navigation.setOptions({
      title: route.params.title,
    });

    async function getData() {
      const response = await request_get('get_channels/' + route.params.id);
      setChannels(response.data);
      setLoader(false);
    }

    getData();
  }, []);

  const dispatch = useDispatch();

  //Here we will check the access token
  useEffect(() => {
    const ff = async () => {
      let result = await chechToken()
      if (result) {
        dispatch(setAuthState(true))
      } else {
        dispatch(setAuthState(false))
      }
    }
    ff()
  }, []);

  //Here we will check the sid
  useEffect(() => {
    AsyncStorage.getItem('sid')
      .then((sid) => sidValidation(sid))
      .then(async ({ success, sid }) => {
        if (success) {
          dispatch(setSid(sid));
        } else {
          await AsyncStorage.removeItem('sid');
          navigation.navigate('subscribes');
        }
      })

  }, []);

  return (
    <View style={globalStyles.container}>
      {loader ? <Loader /> : null}
      <View
        style={{
          paddingHorizontal: 10,
          justifyContent: 'center',
        }}>
        <FlatList
          data={channels}
          renderItem={({item}) => (
            <Channel
              route={'player'}
              data={item}
              navigation={navigation}
              params={{id: item.id, title: item.name}}
              images_path={'channel'}
            />
          )}
          keyExtractor={(item) => item.id.toString()}
        />
      </View>
    </View>
  );
};
export default ChannelsScreen;
