import React, { useEffect, useState } from 'react';
import {
  Image,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  StyleSheet,
  Dimensions,
  FlatList,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { request_get } from '../api/request';
import { colors } from '../templates/colors';
import { fSize, percentHeight, percentWidth } from '../templates/helper';
import Icon from 'react-native-vector-icons/Ionicons';
import Loader from '../components/Loader';
import { IPTV_ADMIN_URL } from '../api/url';
import { getFavoritesMusic, setSearchText } from '../redux/actions';
import MusicPlayerFavorites from './MusicPlayerFavorites';

const FavoritesMusicScreen = ({
  route,
  navigation,
  loader,
  setLoader,
  setSearchCheck,
  searchCheck,
  musics
}) => {
  // const { musics, searchText } = useSelector((state) => state.favoritesReducer);

  const [searchShow, setSearchShow] = useState(false);
  const { sid } = useSelector((state) => state.authReducer);
  const [showMPlayer, setShowMPlayer] = useState(false)
  const [selectedMusic, setSelectedMusic] = useState(null)
  const dispatch = useDispatch();

  // useEffect(() => {
  //   navigation.setOptions({
  //     headerRight: () => (
  //       <View style={{ flexDirection: 'row' }}>
  //         <TouchableOpacity
  //           style={{ marginRight: 10 }}
  //           onPress={() => {
  //             searchShow ? Search() : null;
  //             setSearchShow(!searchShow);
  //             setSearchCheck(true);
  //           }}>
  //           <Icon
  //             name={'search-outline'}
  //             size={20}
  //             color={colors.header_icons_color}
  //           />
  //         </TouchableOpacity>
  //       </View>
  //     ),
  //     headerTitle: () =>
  //       searchShow ? (
  //         searchInput()
  //       ) : (
  //           <Text style={{ color: colors.secondary, fontSize: fSize(7) }}>
  //             Избранные
  //           </Text>
  //         ),
  //   });

  //   if (!searchShow) {
  //     dispatch(getFavoritesMusic(sid, searchText));
  //     setLoader(false);
  //   }
  // }, [searchShow, searchText, route]);

  // useEffect(() => {
  //   if (!searchShow && !searchCheck) {
  //     getData();
  //   }
  // }, [searchShow, searchText, route]);

  // const getData = async () => {
  //   setLoader(true);
  //   const response = await request_get(`musics_favorite/${sid}`);
  //   const vodfiles = response.data
  //     .map((music) => ({
  //       id: music.id,
  //       music: music.music_vodfiles,
  //       poster: music.poster,
  //       genr: music.genr,
  //     }))
  //     .filter((item) => item.music.file_torrent.includes(searchText));
  //   setMusics(vodfiles);

  //   if (!response.success) {
  //     return navigation.navigate('profile');
  //   }
  //   setLoader(false);
  // };

  // const Search = () => {
  //   setSearchShow(false);
  //   // dispatch(setSearchText(''));
  // };

  // const searchInput = () => {
  //   return (
  //     <View style={{ flexDirection: 'row', alignItems: 'center' }}>
  //       <TouchableOpacity
  //         onPress={() => setSearchShow(false)}></TouchableOpacity>

  //       <TextInput
  //         placeholder={'Поиск'}
  //         placeholderTextColor={colors.header_icons_color}
  //         autoFocus={true}
  //         onChangeText={(text) => dispatch(setSearchText(text))}
  //         value={searchText}
  //         style={{ color: colors.header_icons_color }}
  //       />
  //     </View>
  //   );
  // };

  const changeNameMusic = (name) => {
    let arr_name = name.split('');
    if (arr_name.length < 13) {
      return name;
    }
    let cutname = arr_name.splice(0, 13).join('');

    return cutname + ' ...';
  };

  return (
    <View style={{ flex: 1, backgroundColor: colors.primary }}>
      {loader ? <Loader /> : null}
      {!searchShow ? (
        <>
          {
            showMPlayer ?
              <View
                style={{
                  backgroundColor: colors.dark_blue,
                  width: '100%',
                  height: percentHeight(20)
                }}
              >
                <MusicPlayerFavorites navigation={navigation} route={route} selectedMusicData={selectedMusic} />
              </View>
              :
              null
          }
          <View style={{ paddingHorizontal: 15 }}>
            <ScrollView style={{ marginTop: 10 }}>
              <FlatList
                data={musics}
                contentContainerStyle={{
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                  paddingBottom: percentHeight(20),
                }}
                keyExtractor={(item) => item.id.toString()}
                renderItem={({ item }) => (
                  <View
                    style={{
                      flex: 1,
                      // backgroundColor: 'blue',
                      marginTop: 25,
                      justifyContent: 'center',
                      alignItems: 'center',
                      marginRight: percentWidth(2),
                      marginLeft: percentWidth(2),
                    }}>
                    <TouchableOpacity
                      onPress={() => {
                        // navigation.navigate('mplayer', {
                        //   musics: musics,
                        //   selected: item.music.id,
                        //   poster: item.poster,
                        //   album: {
                        //     music_data_genres: [
                        //       {
                        //         name: item.genr,
                        //       },
                        //     ],
                        //     music_vodfiles: [item],
                        //   },
                        // });
                        const obj = {
                          musics: musics,
                          selected: item.music.id,
                          poster: item.poster,
                          album: {
                            music_data_genres: [
                              {
                                name: item.genr,
                              },
                            ],
                            music_vodfiles: [item],
                          },
                        }
                        setShowMPlayer(false)
                        setSelectedMusic(obj);
                        setShowMPlayer(true)
                      }}
                      style={{
                        borderTopEndRadius: 5,
                        borderTopStartRadius: 5,
                        borderRadius: 10,
                        // backgroundColor: 'blue',
                        width: percentWidth(15),
                        height: percentHeight(30),
                        justifyContent: 'space-evenly',
                        alignItems: 'center',
                      }}>
                      <View style={{ alignItems: 'flex-start', justifyContent: 'space-between' }} >
                        <Image
                          source={{
                            uri: `${IPTV_ADMIN_URL}posters/music/${item.poster}`,
                          }}
                          style={{
                            height: percentHeight(20),
                            width: percentHeight(20),
                            resizeMode: 'cover',
                          }}
                        />
                      </View>
                      <Text
                        style={{
                          color: colors.secondary,
                          fontSize: fSize(6),
                          textAlign: 'left',
                          // marginTop: percentHeight(0),
                        }}>
                        {changeNameMusic(item.music.file_torrent)}
                      </Text>
                    </TouchableOpacity>
                  </View>
                )}
              />
              {/* <FlatList
            data={musics}
            contentContainerStyle={{
              paddingBottom: percentHeight(20),
              // paddingLeft: percentWidth(4),
              paddingRight: percentWidth(4),
              flex: 1,
              flexDirection: 'row',
              flexWrap: 'wrap',
              // justifyContent: 'center'
            }}
            keyExtractor={(item) => item.id.toString()}
            centerContent={false}
            renderItem={({item}) => (
              <View
              style={{
                flex: 1,
                // backgroundColor: 'blue',
                marginTop: 25,
                justifyContent: 'center',
                alignItems: 'center',
                marginRight: percentWidth(3),
                marginLeft: percentWidth(3),
              }}>
                <TouchableOpacity
                  style={{
                    borderTopEndRadius: 5,
                    borderTopStartRadius: 5,
                    borderRadius: 10,
                    // backgroundColor: 'blue',
                    width: percentWidth(15),
                    height: percentHeight(30),
                    justifyContent: 'space-around',
                    alignItems: 'center',
                  }}
                  onPress={() =>
                    navigation.navigate('albums', {
                      musics: item.music_vodfiles,
                      poster: item.poster,
                      album: item,
                    })
                  }>
                  <View style={{alignItems: 'flex-start', justifyContent: 'space-between'}} >
                  <Image
                    source={{
                      uri: `${IPTV_ADMIN_URL}posters/music/${item.poster}`,
                    }}
                    style={{
                      height: percentHeight(20),
                      width: percentHeight(20),
                      resizeMode: 'cover',
                    }}
                  />
                  <Text
                    style={{
                      color: colors.secondary,
                      fontSize: fSize(6),
                      textAlign: 'left',
                      marginTop: percentHeight(1),
                    }}>
                    {changeNameMusic(item.file_torrent)}
                  </Text>
                  </View>
                </TouchableOpacity>
              </View>
            )}
          /> */}
            </ScrollView>
          </View>
        </>
      ) : null}
    </View>
  );
};
export default FavoritesMusicScreen;

const styles = StyleSheet.create({
  imageContainer: {
    width: percentWidth(17),
    maxHeight: percentHeight(13),
    minHeight: percentHeight(9),
    backgroundColor: '#2C2A3C',
    justifyContent: 'center',
  },
  itemContainer: {
    flexDirection: 'row',
    marginVertical: 5,
  },
});
