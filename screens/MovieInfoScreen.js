import React, { useEffect } from 'react'
import { View, Text } from 'react-native'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/Ionicons'
import { colors } from '../templates/colors'
import { fSize } from '../templates/helper'

const MovieInfoScreen = ({navigation}) => {
    useEffect(() => {
        navigation.setOptions({
            headerTitle: () => (
                <Text style={{
                    color: colors.secondary,
                    fontSize: fSize(7)
                }}>
                    Hello World!
                </Text>
            ),
            // headerLeft: () => (
            //     <TouchableOpacity>
            //         <Icon name="arrow-back-outline" style={{fontSize: fSize(8), color: colors.secondary}} />
            //     </TouchableOpacity>
            // )
            
        })
    }, [])
    return (
        <View
            style={{
                backgroundColor: colors.primary,
                flex: 1
            }}
        >
            {}
        </View>
    )
}

export default MovieInfoScreen