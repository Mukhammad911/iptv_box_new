import React, {useEffect, useLayoutEffect, useState} from 'react';
import {Text, View} from 'react-native';
import {FlatList} from 'react-native-gesture-handler';
import {useSelector} from 'react-redux';
import {request_get} from '../api/request';
import FilmCard from '../components/films/FilmCard';
import {colors} from '../templates/colors';

const HistoryTrackingScreen = ({navigation}) => {
  const sid = useSelector((state) => state.authReducer.sid);
  const [filmsHistory, setFilmsHistory] = useState([]);

  useLayoutEffect(() => {
    navigation.setOptions({
      title: 'Просмотренные',
    });
  }, []);

  useEffect(() => {
    request_get(`vod_data_history/${sid}`).then(({data}) => {
      setFilmsHistory(data);
    });
  }, []);

  return (
    <View
      style={{
        backgroundColor: colors.primary,
        flex: 1,
      }}>
      <FlatList
        data={filmsHistory}
        renderItem={({item}) => (
          <FilmCard item={item} navigation={navigation} />
        )}
        numColumns={3}
        keyExtractor={(item) => item.id.toString()}
      />
    </View>
  );
};

export default HistoryTrackingScreen;
