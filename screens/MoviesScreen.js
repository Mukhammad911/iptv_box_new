import React, { useEffect, useRef, useState } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  FlatList,
  Modal,
  TouchableHighlight,
  TextInput,
  StyleSheet,
  Image,
  ScrollView,
} from 'react-native';
import { colors } from '../templates/colors';
import { globalStyles } from '../templates/globalStyles';
import { request_get } from '../api/request';
import Loader from '../components/Loader';
import Icon from 'react-native-vector-icons/Ionicons';
import DropDownPicker from 'react-native-dropdown-picker';
import { chechToken, items } from "../components/helper";
import Banners from '../components/Banners';
import { fSize, percentHeight, percentWidth } from '../templates/helper';
import { useIsFocused } from '@react-navigation/native';
import { DeviceEventEmitter } from 'react-native';
import FilmCard from '../components/films/FilmCard';
import { useDispatch, useSelector } from "react-redux";
import ScreenHeader from '../components/ScreenHeader';
import { SCALE_PARAM, DEFAULT_SCALE } from '../templates/scaleParameters'
import FocusableOpacity from '../components/FocusableOpacity';
import KeyEvent from 'react-native-keyevent'
import SidNotFound from '../components/SidNotFound';
import TouchableFocusArray from '../components/TouchableFocusArray';
import { IPTV_ADMIN_URL } from '../api/url';
import TouchableFocus from '../components/TouchableFocus';
import AsyncStorage from "@react-native-community/async-storage";
import sidValidation from "../services/sidChecker";
import { setAuthState, setSid } from "../redux/actions";
import ButtonsNavigationMovie from "../components/General/ButtonsNavigationMovie";

const changeNameFilm = (name) => {
  let arr_name = name.split("");
  if (arr_name.length < 14) {
    return name;
  }
  let cutname = arr_name.splice(0, 14).join('');

  return cutname + ' ...';
}

// import Modal from 'react-native-modal';

// Токенизация реализована

const FilmCardItem = React.memo(({ item, index, filmsLength, menuRef, setFirstFilmCardRef, navigation }) => (
  <TouchableFocus
    style={{
      marginBottom: percentHeight(3),
      flex: 1,
      backgroundColor: colors.primary,
      borderTopEndRadius: 5,
      borderTopStartRadius: 5,
      borderWidth: 1,
      borderColor: colors.primary,
      marginTop: 5,
      overflow: 'hidden',
      width: percentWidth(15),
      height: percentHeight(50),
      alignItems: 'center',
      justifyContent: "flex-start",
    }}
    focusedStyle={{
      // transform: [{ scale: SCALE_PARAM }]
      borderColor: colors.dark_red
    }}
    index={index}
    key={item.name}
    nextFocusRightSelf={!((index + 1) % 5)}
    nextFocusDownSelf={(filmsLength - index + 1) < 5}
    nextFocusLeft={(!(index % 5) ? menuRef : null)}
    focusedOnStart={!index}
    setRef={!index ? setFirstFilmCardRef : (() => null)}
    onPress={() =>
      navigation.navigate('vplayer', { id: item.id, format: item.format })
    }
  >
    <View>
      <Image
        source={{ uri: IPTV_ADMIN_URL + 'posters/' + item.poster }}
        style={{
          height: percentHeight(40),
          width: percentWidth(15),
        }}
      />
    </View>
    <View
      style={{
        marginTop: 5,
        width: percentWidth(15),
        maxHeight: percentHeight(4),
        overflow: 'hidden',
      }}>
      <Text style={{ color: '#fff', fontSize: fSize(7) }}>{changeNameFilm(item.name)}</Text>
    </View>
    <View
      style={{
        width: percentWidth(15),
        maxHeight: percentHeight(20),
        overflow: 'hidden',
      }}>
      <Text style={{ color: colors.dark_text, fontSize: fSize(5) }}>
        {item.year} - {item.genres.length > 0 ? item.genres[0].name : ''}
      </Text>
    </View>
  </TouchableFocus>
))

const FilmCardList = React.memo(({ films = [], navigation, menuRef, setFirstFilmCardRef }) => {
  const renderFilmCardItem = ({item, index}) => (
    <FilmCardItem
      item={item}
      index={index}
      filmsLength={films.length}
      menuRef={menuRef}
      setFirstFilmCardRef={setFirstFilmCardRef}
      navigation={navigation}
    />
  )
  return (
    <FlatList
      removeClippedSubviews={true}
      updateCellsBatchingPeriod={100}
      initialNumToRender={5}
      maxToRenderPerBatch={5}
      windowSize={7}
      numColumns={5}
      data={films}
      keyExtractor={(_, index) => index}
      renderItem={renderFilmCardItem}
    />
  )
})

const MoviesScreen = ({ navigation, route }) => {

  const isFocused = useIsFocused();

  const menuRef = useSelector(state => state.refReducer.menuRef);

  const formats = [{ id: 'standart' }, { id: 'HD' }, { id: 'FullHD' }, { id: 'UHD' }];
  const [format, setFormat] = useState('standart');
  const [films, setFilms] = useState([]);
  const [loader, setLoader] = useState(true);
  const [modalVisible, setModalVisible] = useState(false);
  const [genres, setGenres] = useState([]);
  const [years, setYears] = useState([]);
  const [filters, setFilters] = useState({
    search: '',
    genre: { value: '', label: '' },
    year: '',
  });
  const [vodTypes, setVodTypes] = useState([]);
  const [banners, setBanners] = useState([]);
  const [searchShow, setSearchShow] = useState(false);
  const [type, setType] = useState({
    title: 'Фильм',
    id: 1,
  });
  const [filmsArr, setFilmsArr] = useState([]);
  const [filterCheck, setFilterCheck] = useState(false);
  const [page, setPage] = useState(1);
  const sid = useSelector((state) => state.authReducer.sid);
  const [listLoader, setListLoader] = useState(true);

  const [showFormats, setShowFormats] = useState(false);
  const [showGenres, setShowGenres] = useState(false);
  const [showYears, setShowYears] = useState(false);

  const filtersModalRef = useRef(null);

  const [firstFilmCardRef, setFirstFilmCardRef] = useState(null);

  const [filterButtonRef, setFilterButtonRef] = useState(null);
  const [searchButtonRef, setSearchButtonRef] = useState(null);

  const [filterCloseButtonRef, setFilterCloseButtonRef] = useState(null);
  const [filterApplyButtonRef, setFilterApplyButtonRef] = useState(null);
  const [filterFirstElementRef, setFilterFirstElementRef] = useState(null);

  const [formatsCloseButtonRef, setFormatsCloseButtonRef] = useState(null);
  const [formatsFirstElementRef, setFormatsFirstElementRef] = useState(null);

  const [genresCloseButtonRef, setGenresCloseButtonRef] = useState(null);
  const [genresFirstElementRef, setGenresFirstElementRef] = useState(null);

  const [yearsCloseButtonRef, setYearsCloseButtonRef] = useState(null);
  const [yearsFirstElementRef, setYearsFirstElementRef] = useState(null);

  let searchInputRef = useRef(null);
  const dispatch = useDispatch();

  //Here we will check the access token
  useEffect(() => {
    const ff = async () => {
      let result = await chechToken()
      if (result) {
        dispatch(setAuthState(true))
      } else {
        dispatch(setAuthState(false))
      }
    }
    ff()
  }, []);

  //Here we will check the sid
  useEffect(() => {
    AsyncStorage.getItem('sid')
      .then((sid) => sidValidation(sid))
      .then(async ({ success, sid }) => {
        if (success) {
          dispatch(setSid(sid));
        } else {
          await AsyncStorage.removeItem('sid');
          navigation.navigate('subscribes');
        }
      })

  }, []);


  useEffect(() => {
    if (firstFilmCardRef) {
      firstFilmCardRef.setNativeProps({
        hasTVPreferredFocus: true
      })
    }
  }, [firstFilmCardRef])

  const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 20;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  useEffect(() => {
    if (modalVisible) {
      menuRef.setNativeProps({
        disabled: true,
      })
    } else {
      menuRef.setNativeProps({
        disabled: false,
      })
    }
  }, [modalVisible])


  useEffect(() => {
    if (route.params?.vodType) {
      setType({
        id: route.params?.vodType,
        title: route.params?.vodName,
      });
    }
  }, [route.params]);

  const search = () => {
    return (
      <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: percentWidth(1) }}>
        <TouchableFocus
          style={{}}
          focusedStyle={{
            transform: [{ scale: 1.3 }]
          }}
          onPress={() => setSearchShow(false)}
          nextFocusUpSelf
          nextFocusLeft={menuRef}
          nextFocusDown={menuRef}
        >
          <Icon
            name={'arrow-back-outline'}
            size={percentHeight(5)}
            color={colors.header_icons_color}
          />
        </TouchableFocus>
        <TouchableFocus
          style={{
            // height: percentHeight(3),
          }}
          focusedStyle={{
            transform: [{ scale: 1.3 }],
            borderWidth: 1,
            borderColor: colors.dark_red,
          }}
          onPress={() => {
            searchInputRef?.current?.focus();
          }}
        >
          <TextInput
            placeholder={'Поиск'}
            ref={e => searchInputRef.current = e}
            placeholderTextColor={colors.header_icons_color}
            onSubmitEditing={() => { setFilterCheck(true); filter(); setSearchShow(!searchShow); }}
            autoFocus={true}
            onChangeText={(text) => setFilters({ ...filters, search: text })}
            value={filters.search}
            style={{ color: colors.header_icons_color }}
          />
        </TouchableFocus>
      </View>
    );
  };

  useEffect(() => {
    if (!modalVisible && (firstFilmCardRef != null)) {
      firstFilmCardRef.setNativeProps({
        hasTVPreferredFocus: true
      })
    }
  }, [modalVisible])

  // const isFocused = useIsFocused();

  useEffect(() => {
    DeviceEventEmitter.addListener('goBack', (e) => {
      if (sid) {
        getVodTypes();
        getFilms();
        getBanners();
        getData();
      }
    });
    navigation.setOptions({
      headerRight: () => (
        <View style={{ flexDirection: 'row' }}>
          <FocusableOpacity
            style={{ marginRight: 10 }}
            onPress={() => {
              setModalVisible(true);
              setFilterCheck(true);
            }}>
            {!searchShow ? (
              <Icon
                name={'options-outline'}
                size={percentHeight(4)}
                color={colors.header_icons_color}
              />
            ) : null}
          </FocusableOpacity>
          <FocusableOpacity
            style={{ marginRight: 10 }}
            focusedStyle={{
              transform: [{ scale: 1.3 }]
            }}
            onFocus={() => setFocusedFilterIcon(true)}
            onBlur={() => setFocusedFilterIcon(false)}
            onPress={() => {
              setFilterCheck(true);
              searchShow ? filter() : null;
              setSearchShow(!searchShow);
            }}>
            <Icon
              name={'search-outline'}
              size={percentHeight(4)}
              color={colors.header_icons_color}
            />
          </FocusableOpacity>
        </View>
      ),
      headerTitle: () =>
        searchShow ? (
          search()
        ) : (
          <Text style={{ color: colors.secondary, fontSize: fSize(7) }}>
            {type.title}
          </Text>
        ),
    });
    const getData = async () => {
      const response_genre = await request_get(`vod_genres/${sid}`);
      if (!response_genre.success) {
        // return navigation.navigate('profile', { auth: false });
        return;
      }
      const response_year = await request_get(`vod_years/${sid}`);
      if (!response_year.success) {
        // return navigation.navigate('profile', { auth: false });
        return;
      }


      setGenres(response_genre.data);
      setYears(response_year.data);
    };

    if (!searchShow && !filterCheck) {
      if (sid) {
        getVodTypes();
        getFilms();
        getBanners();
        getData();
      }
    }
  }, [searchShow, type, filters, sid]);

  const getVodTypes = async () => {
    const vodTypes = await request_get(`vod_types/${sid}`);
    if (!vodTypes.success) {
      // return navigation.navigate('profile', { auth: false });
      return;
    }
    setVodTypes(vodTypes.data);
  };

  const getFilms = async () => {
    try {
      setPage(1);
      setFilms([]);
      setLoader(true);
      const response = await request_get(`vod_datas/${sid}/1?type=${type.id}`);
      if (!response.success) {
        // return navigation.navigate('profile', { auth: false });
        return;
      }
      setFilms(response.data);
      setLoader(false);
      setPage(2);
    } catch (error) {
    }
  };

  const getBanners = async () => {
    setBanners([]);
    const response_banner = await request_get(`banners/${type.id}`);
    setBanners(response_banner.data);
  };

  const filter = async () => {
    setPage(1);
    const { search, year, genre } = filters;
    setBanners([]);
    setFilms([]);
    setLoader(true);
    const response = await request_get(
      `vod_datas/${sid}/1?search=${search}&year=${year}&genre=${genre.value}&type=${type.id}`,
    );
    if (!response.success) {
      // return navigation.navigate('profile', { auth: false });
      return;
    }
    if (vodTypes.length === 0) {
      getVodTypes();
    }
    setFilms(response.data);
    setLoader(false);
    setPage(2);
  };

  const paginate = async () => {
    setListLoader(true);
    const { search, year, genre } = filters;
    const response = await request_get(
      `vod_datas/${sid}/${page}?search=${search}&year=${year}&genre=${genre.value}&type=${type.id}`,
    );
    if (!response.success) {
      // return navigation.navigate('profile', { auth: false });
      return
    }
    setFilms(() => {
      return [...films, ...response.data];
    });
    setPage(page + 1);
    setListLoader(false);
  };

  return !sid ? <SidNotFound navigate={navigation.navigate} /> : (
    <View style={globalStyles.container}>
      <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }} >
        {
          searchShow ? search() : <ScreenHeader screenName="Фильмы" />
        }
        {/* {
          searchShow ? null : <ScreenHeader screenName="Фильмы" />
        } */}
        <View style={{ flexDirection: 'row', alignItems: 'center' }}>
          <TouchableFocus
            style={{ marginRight: 10 }}
            focusedStyle={{
              transform: [{ scale: 1.3 }]
            }}
            onPress={() => {
              setModalVisible(true);
              setFilterCheck(true);
            }}
            nextFocusLeftSelf={true}
            setRef={setFilterButtonRef}
          >
            {!searchShow ? (
              <Icon
                name={'options-outline'}
                size={percentHeight(4)}
                color={colors.header_icons_color}
              />
            ) : null}
          </TouchableFocus>
          <TouchableFocus
            style={{ marginRight: 10 }}
            focusedStyle={{
              transform: [{ scale: 1.3 }]
            }}
            onPress={() => {
              setFilterCheck(true);
              searchShow ? filter() : null;
              setSearchShow(!searchShow);
            }}
            setRef={setSearchButtonRef}
          >
            <Icon
              name={'search-outline'}
              size={percentHeight(4)}
              color={colors.header_icons_color}
            />
          </TouchableFocus>
        </View>
      </View>
      {loader ? <Loader /> : null}
      {!searchShow ? (
        <>
          <ButtonsNavigationMovie
            data={vodTypes}
            type={type}
            setType={setType}
            setFilterCheck={setFilterCheck}
            setLoader={setLoader}
            setDataEmpty={setFilms}
            nextFocusUpRef={filterButtonRef}
            blockFocusRight
          />
          {films.length > 0 ? (
            <ScrollView
              onScroll={async ({ nativeEvent }) => {
                if (isCloseToBottom(nativeEvent) && !(films.length % 15)) {
                  await paginate();
                }
              }}>
              <Banners neededFocus={true} banners={banners} navigate={navigation.navigate} />

              <FilmCardList films={films} navigation={navigation} menuRef={menuRef} setFirstFilmCardRef={setFirstFilmCardRef} />

              {/* Last version of flatlist
              <FlatList
                numColumns={5}
                // horizontal
                data={films}
                renderItem={({ item, index }) => (
                  <FilmCard
                    key={index}
                    focusedStyle={{
                      transform: [{ scale: SCALE_PARAM }]
                    }}
                    // focusedOnStart={index ? false : true}
                    disableRightNavigation={index % 4 ? true : false}
                    style={{
                      marginBottom: percentHeight(3)
                    }}
                    format={format}
                    item={item}
                    navigation={navigation}
                  />
                )}
                keyExtractor={(item) => item.id.toString()}
                contentContainerStyle={{
                  paddingTop: percentHeight(4)
                }}
              /> */}
              {/* <FlatList
                data={filmsArr}
                keyExtractor={(_, index) => index.toString()}
                renderItem={(item) => (
                  <FlatList
                    horizontal
                    keyExtractor={(_, index) => index.toString()}
                    data={item.item}
                    renderItem={(item1) => (
                      <View
                        style={{
                          marginHorizontal: percentWidth(2)
                        }}
                      >
                        <FilmCard
                          key={item.item}
                          focusedStyle={{
                            transform: [{ scale: SCALE_PARAM }]
                          }}
                          // focusedOnStart={index ? false : true}
                          // disableRightNavigation={index % 4 ? true : false}
                          style={{
                            marginBottom: percentHeight(3)
                          }}
                          format={format}
                          navigation={navigation}
                        />
                      </View>
                    )}
                  />
                )}
              /> */}
              {/* <View
                style={{
                  flexDirection: 'row',
                  // flexWrap: 'wrap'
                }}
              >
                {
                  films.map((item, index) => (
                    <FilmCard
                      key={index}
                      focusedStyle={{
                        transform: [{ scale: SCALE_PARAM }]
                      }}
                      // focusedOnStart={index ? false : true}
                      disableRightNavigation={index % 4 ? true : false}
                      style={{
                        marginBottom: percentHeight(3)
                      }}
                      format={format}
                      item={item}
                      navigation={navigation}
                    />
                  ))
                }
              </View> */}
              {listLoader ? (
                <View style={{ height: percentHeight(10) }}>
                  <Loader />
                </View>
              ) : null}
            </ScrollView>
          ) : !loader ? (
            <View
              style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
              <Text
                style={{ color: colors.header_icons_color, fontSize: fSize(7) }}>
                По вашему запросу ничего не найдено!
              </Text>
            </View>
          ) : null}
        </>
      ) : null}

      {
        modalVisible ?
          (<View
            focusable={true}
            style={{
              position: 'absolute',
              right: 0,
              width: percentWidth(25),
              height: '100%',
              backgroundColor: '#191723',
              // paddingHorizontal: percentWidth(2)
            }}
            transparent={true}
          >
            {/* <View
              style={{
                position: 'absolute',
                right: 0,
                width: percentWidth(25),
                height: '100%',
                backgroundColor: colors.dark_blue,
                paddingHorizontal: percentWidth(2)
              }}
            > */}
            {
              !showFormats && !showGenres && !showYears ?
                (<View style={{ height: '100%' }} >
                  <View
                    style={{
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      flexDirection: 'row',
                      paddingHorizontal: percentWidth(2)
                    }}
                  >
                    <Text
                      style={{
                        fontSize: fSize(8),
                        color: colors.dark_text,
                        paddingVertical: percentHeight(2),
                      }}
                    >
                      Фильтр
                    </Text>
                    <TouchableFocus
                      focusedStyle={{
                        transform: [{ scale: 1.3 }]
                      }}
                      onPress={() => {
                        setModalVisible(false);
                        setFilterCheck(false);
                      }}
                      setRef={setFilterCloseButtonRef}
                      nextFocusLeftSelf={true}
                      nextFocusRightSelf={true}
                      nextFocusDown={filterFirstElementRef}
                    >
                      <Icon name="close-outline" style={{ color: colors.modal_icon, fontSize: fSize(10) }} />
                    </TouchableFocus>
                  </View>
                  <TouchableFocus
                    focusedOnStart={true}
                    style={{
                      width: '100%',
                      // backgroundColor: 'red',
                      paddingVertical: percentHeight(0.7),
                      flexDirection: 'row',
                      alignItems: 'center',
                      paddingLeft: percentWidth(2)
                    }}
                    focusedStyle={{
                      // transform: [{ scale: 1.1 }]
                      backgroundColor: '#2C293D'
                    }}
                    setRef={setFilterFirstElementRef}
                    onPress={() => setShowFormats(true)}
                    nextFocusUp={filterCloseButtonRef}
                    nextFocusLeftSelf={true}
                    nextFocusRightSelf={true}
                    focusedOnStart
                  >
                    <Text
                      style={{
                        color: colors.secondary,
                        fontSize: fSize(6)
                      }}
                    >
                      Формат
                    </Text>
                    <Text
                      style={{
                        color: colors.dark_text,
                        fontSize: fSize(5),
                        marginLeft: percentWidth(2)
                      }}
                    >
                      {format}
                    </Text>
                  </TouchableFocus>
                  <TouchableFocus
                    style={{
                      width: '100%',
                      // backgroundColor: 'green',
                      paddingVertical: percentHeight(0.7),
                      flexDirection: 'row',
                      alignItems: 'center',
                      paddingLeft: percentWidth(2)
                    }}
                    focusedStyle={{
                      // transform: [{ scale: 1.1 }]
                      backgroundColor: '#2C293D'
                    }}
                    onPress={() => setShowGenres(true)}
                    nextFocusLeftSelf
                    nextFocusRightSelf
                  >
                    <Text
                      style={{
                        color: colors.secondary,
                        fontSize: fSize(6)
                      }}
                    >
                      Жанр
                    </Text>
                    <Text
                      style={{
                        color: colors.dark_text,
                        fontSize: fSize(5),
                        marginLeft: percentWidth(2)
                      }}
                    >
                      {filters.genre.label}
                    </Text>
                  </TouchableFocus>
                  <TouchableFocus
                    style={{
                      width: '100%',
                      // backgroundColor: 'blue',
                      paddingVertical: percentHeight(0.7),
                      flexDirection: 'row',
                      alignItems: 'center',
                      paddingLeft: percentWidth(2)
                    }}
                    focusedStyle={{
                      // transform: [{ scale: 1.1 }]
                      backgroundColor: '#2C293D'
                    }}
                    onPress={() => setShowYears(true)}
                    nextFocusLeftSelf
                    nextFocusRightSelf
                    nextFocusDown={filterApplyButtonRef}
                  >
                    <Text
                      style={{
                        color: colors.secondary,
                        fontSize: fSize(6)
                      }}
                    >
                      Год
                    </Text>
                    <Text
                      style={{
                        color: colors.dark_text,
                        fontSize: fSize(5),
                        marginLeft: percentWidth(2)
                      }}
                    >
                      {filters.year}
                    </Text>
                  </TouchableFocus>
                  <View
                    style={{
                      // position: 'absolute',
                      // bottom: percentHeight(5),
                      width: '100%',
                      alignItems: 'center',
                      marginTop: percentHeight(5)
                    }}
                  >
                    <View style={{ width: percentWidth(9) }}>
                      <TouchableFocus
                        style={{
                          ...globalStyles.openButton,
                          backgroundColor: colors.dark_red,
                          height: percentHeight(6),
                          justifyContent: 'center',
                        }}
                        focusedStyle={{
                          transform: [{ scale: SCALE_PARAM }]
                        }}
                        onPress={() => {
                          filter();
                          setModalVisible(false);
                        }}
                        setRef={setFilterApplyButtonRef}
                        nextFocusLeftSelf
                        nextFocusRightSelf
                        nextFocusDownSelf
                      >
                        <Text style={{ ...globalStyles.textStyle, fontSize: fSize(5) }}>Применить</Text>
                      </TouchableFocus>
                    </View>
                  </View>
                </View>)
                :
                null
            }
            {
              showFormats ?
                (<View>
                  <View
                    style={{
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      flexDirection: 'row',
                      paddingHorizontal: percentWidth(2)
                    }}
                  >
                    <Text
                      style={{
                        fontSize: fSize(8),
                        color: colors.dark_text,
                        paddingVertical: percentHeight(2),
                        // marginLeft: percentWidth(0.5)
                      }}
                    >
                      Формат
                    </Text>
                    <TouchableFocus
                      focusedStyle={{
                        transform: [{ scale: 1.3 }]
                      }}
                      style={{
                        marginLeft: percentWidth(-1)
                      }}
                      onPress={() => setShowFormats(false)}
                      setRef={setFormatsCloseButtonRef}
                      nextFocusUpSelf
                      nextFocusLeftSelf
                      nextFocusRightSelf
                      nextFocusDown={formatsFirstElementRef}
                    >
                      <Icon name="arrow-back-outline" size={fSize(10)} color={colors.dark_text} />
                    </TouchableFocus>
                  </View>
                  <ScrollView>
                    {formats.map((elem, index) => (
                      <TouchableFocus
                        style={{
                          width: '100%',
                          // backgroundColor: 'green',
                          // marginVertical: percentHeight(0.7),
                          paddingVertical: percentHeight(1),
                          // marginLeft: percentWidth(1),
                          flexDirection: 'row',
                          alignItems: 'center',
                          paddingLeft: percentWidth(2)
                        }}
                        onPress={() => { setFormat(elem.id); setShowFormats(false) }}
                        focusedOnStart={!index}
                        focusedStyle={{
                          // transform: [{ scale: 1.1 }]
                          backgroundColor: '#2C293D'
                        }}
                        setRef={!index ? setFormatsFirstElementRef : (() => null)}
                        nextFocusLeftSelf
                        nextFocusRightSelf
                        nextFocusUp={!index ? formatsCloseButtonRef : null}
                        nextFocusDownSelf={(index == (formats.length - 1))}
                      >
                        <Text
                          style={{
                            color: colors.secondary,
                            fontSize: fSize(6),
                          }}
                        >
                          {elem.id}
                        </Text>
                      </TouchableFocus>
                    ))}
                  </ScrollView>
                </View>)
                :
                null
            }
            {
              showGenres ?
                (<View>
                  <View
                    style={{
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      flexDirection: 'row',
                      paddingHorizontal: percentWidth(2)
                    }}
                  >
                    <Text
                      style={{
                        fontSize: fSize(8),
                        color: colors.dark_text,
                        paddingVertical: percentHeight(2),
                      }}
                    >
                      Жанр
                    </Text>
                    <TouchableFocus
                      focusedStyle={{
                        transform: [{ scale: 1.3 }]
                      }}
                      onPress={() => setShowGenres(false)}
                      setRef={setGenresCloseButtonRef}
                      nextFocusLeftSelf
                      nextFocusUpSelf
                      nextFocusRightSelf
                      nextFocusDown={genresFirstElementRef}
                    >
                      <Icon name="arrow-back-outline" color={colors.dark_text} size={fSize(10)} />
                    </TouchableFocus>
                  </View>
                  <ScrollView
                    style={{
                      paddingBottom: percentHeight(50),
                      marginBottom: percentHeight(9)
                    }}
                  >
                    {genres.map((elem, index) => (
                      <View>
                        <TouchableFocus
                          style={{
                            width: '100%',
                            // backgroundColor: 'green',
                            paddingVertical: percentHeight(0.7),
                            // marginLeft: percentWidth(1),
                            flexDirection: 'row',
                            alignItems: 'center',
                            paddingLeft: percentWidth(2)
                          }}
                          focusedStyle={{
                            // transform: [{ scale: 1.1 }]
                            backgroundColor: '#2C293D'
                          }}
                          focusedOnStart={!index}
                          onPress={() => { setFilters({ ...filters, genre: { value: elem.id, label: elem.name } }); setShowGenres(false); }}
                          setRef={!index ? setGenresFirstElementRef : (() => null)}
                          nextFocusLeftSelf
                          nextFocusRightSelf
                          nextFocusDownSelf={index == (genres.length - 1)}
                          nextFocusUp={!index ? genresCloseButtonRef : null}
                        >
                          <Text
                            style={{
                              color: colors.secondary,
                              fontSize: fSize(6)
                            }}
                          >
                            {elem.name}
                          </Text>
                        </TouchableFocus>
                      </View>
                    ))}
                  </ScrollView>
                </View>)
                :
                null
            }
            {
              showYears ?
                (<View>
                  <View
                    style={{
                      justifyContent: 'space-between',
                      alignItems: 'center',
                      flexDirection: 'row',
                      paddingHorizontal: percentWidth(2)
                    }}
                  >
                    <Text
                      style={{
                        fontSize: fSize(8),
                        color: colors.dark_text,
                        paddingVertical: percentHeight(2),
                      }}
                    >
                      Год
                    </Text>
                    <TouchableFocus
                      focusedStyle={{
                        transform: [{ scale: 1.3 }]
                      }}
                      onPress={() => setShowYears(false)}
                      setRef={setYearsCloseButtonRef}
                      nextFocusLeftSelf
                      nextFocusUpSelf
                      nextFocusRightSelf
                      nextFocusDown={yearsFirstElementRef}
                    >
                      <Icon name="arrow-back-outline" size={fSize(10)} color={colors.dark_text} />
                    </TouchableFocus>
                  </View>
                  <ScrollView
                    style={{
                      marginBottom: percentHeight(10)
                    }}
                  >
                    {years.map((elem, index) => (
                      <View>
                        <TouchableFocus
                          style={{
                            width: '100%',
                            // backgroundColor: 'green',
                            paddingVertical: percentHeight(0.7),
                            // marginLeft: percentWidth(1),
                            flexDirection: 'row',
                            alignItems: 'center',
                            paddingLeft: percentWidth(2)
                          }}
                          focusedStyle={{
                            // transform: [{ scale: 1.1 }]
                            backgroundColor: '#2C293D'
                          }}
                          focusedOnStart={!index}
                          onPress={() => {
                            setFilters({ ...filters, year: elem.year }); setShowYears(false)
                          }}
                          setRef={!index ? setYearsFirstElementRef : (() => null)}
                          nextFocusLeftSelf
                          nextFocusRightSelf
                          nextFocusDownSelf={index == (years.length - 1)}
                          nextFocusUp={!index ? yearsCloseButtonRef : null}
                        >
                          <Text
                            style={{
                              color: colors.secondary,
                              fontSize: fSize(6)
                            }}
                          >
                            {elem.year}
                          </Text>
                        </TouchableFocus>
                      </View>
                    ))}
                  </ScrollView>
                </View>)
                :
                null
            }
            {/* </View> */}
          </View>)
          :
          null
      }

      {/* <View style={{flex: 1}} >
            <Modal
              style={{width: 100, alignItems: 'flex-end'}}
              animationType="slide"
              // presentationStyle='pageSheet'
              transparent={true}
              visible={modalVisible}
              onRequestClose={() => {
                setModalVisible(false);
              }}>
              <View style={{justifyContent: 'flex-end', alignItems: 'flex-end', width: percentWidth(100)}}>
                <View
                  style={{
                    ...globalStyles.modalView,
                    backgroundColor: colors.btn_color,
                    borderRadius: 10,
                    paddingHorizontal: 0,
                    alignItems: 'flex-start'
                  }}>
                  <View
                    style={{
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                      paddingHorizontal: 15,
                      marginBottom: 10,
                    }}>
                    <Text style={{fontSize: fSize(10), color: colors.secondary}}>
                      Фильтр
                    </Text>
                    <TouchableOpacity
                      onPress={() => {
                        setModalVisible(false);
                        setFilterCheck(false);
                      }}>
                      <Icon
                        name="close-outline"
                        size={fSize(10)}
                        color={colors.modal_icon}
                      />
                    </TouchableOpacity>
                  </View>
                  <View>
                    <Text
                      style={{
                        ...styles.filterActive,
                        color: colors.secondary,
                        position: 'absolute',
                        left: percentWidth(5),
                        bottom: 5,
                        elevation: 3,
                      }}>
                      Формат
                    </Text>
                    <DropDownPicker
                      items={items(formats, 'id')}
                      onChangeItem={(item) => {
                        setFormat(item.value);
                      }}
                      style={{
                        backgroundColor: 'transparent',
                        borderWidth: 0,
                        width: percentWidth(31),
                      }}
                      containerStyle={{
                        height: percentHeight(5),
                        width: percentWidth(100),
                      }}
                      arrowColor={colors.secondary}
                      defaultValue={null}
                      placeholder={''}
                      isVisible={false}
                      placeholderStyle={{
                        color: colors.secondary,
                        fontSize: fSize(10),
                      }}
                      itemStyle={{width: percentWidth(100)}}
                    />
                    <Text
                      style={{
                        ...styles.filterActive,
                        position: 'absolute',
                        left: percentWidth(29),
                      }}>
                      {format}
                    </Text>
                  </View>
                  <View style={styles.filterContainer}>
                    <Text
                      style={{
                        ...styles.filterActive,
                        color: colors.secondary,
                        position: 'absolute',
                        left: percentWidth(5),
                        bottom: 5,
                        elevation: 3,
                      }}>
                      Жанр
                    </Text>
                    <DropDownPicker
                      items={items(genres, 'name')}
                      onChangeItem={(item) => {
                        setFilters({
                          ...filters,
                          genre: {value: item.value, label: item.label},
                        });
                      }}
                      containerStyle={{
                        height: percentHeight(5),
                        width: percentWidth(100),
                      }}
                      style={{
                        backgroundColor: 'transparent',
                        borderWidth: 0,
                        width: percentWidth(26),
                      }}
                      arrowColor={colors.secondary}
                      placeholderStyle={{
                        color: colors.secondary,
                        fontSize: fSize(10),
                      }}
                      dropDownMaxHeight={percentHeight(22)}
                      placeholder={''}
                    />
                    <Text
                      style={{
                        ...styles.filterActive,
                        position: 'absolute',
                        left: percentWidth(26),
                      }}>
                      {filters.genre.label}
                    </Text>
                  </View>

                  <View style={styles.filterContainer}>
                    <Text
                      style={{
                        ...styles.filterActive,
                        color: colors.secondary,
                        position: 'absolute',
                        left: percentWidth(5),
                        bottom: 5,
                        elevation: 3,
                      }}>
                      Год
                    </Text>
                    <DropDownPicker
                      items={items(years, 'year')}
                      onChangeItem={(item) =>
                        setFilters({...filters, year: item.label})
                      }
                      containerStyle={{
                        height: percentHeight(5),
                        width: percentWidth(100),
                      }}
                      style={{
                        backgroundColor: 'transparent',
                        borderWidth: 0,
                        width: percentWidth(22),
                      }}
                      arrowColor={colors.secondary}
                      placeholderStyle={{
                        color: colors.secondary,
                        fontSize: fSize(10),
                      }}
                      dropDownMaxHeight={percentHeight(16)}
                      placeholder={''}
                    />
                    <Text
                      style={{
                        ...styles.filterActive,
                        position: 'absolute',
                        left: percentWidth(22),
                      }}>
                      {filters.year}
                    </Text>
                  </View>
                  <View style={{marginTop: 30, paddingHorizontal: 15}}>
                    <TouchableHighlight
                      style={{
                        ...globalStyles.openButton,
                        backgroundColor: colors.dark_red,
                      }}
                      onPress={() => {
                        filter();
                        setModalVisible(!modalVisible);
                      }}>
                      <Text style={globalStyles.textStyle}>Применить</Text>
                    </TouchableHighlight>
                  </View>
                </View>
              </View>
            </Modal>
          </View> */}
    </View>
  );
};
export default MoviesScreen;
const styles = StyleSheet.create({
  active: {
    backgroundColor: colors.dark_red,
  },
  filterContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 5,
  },
  filterActive: {
    color: colors.active_filter,
    fontSize: fSize(5),
  },
});
