import AsyncStorage from '@react-native-community/async-storage'
import React, { useEffect, useState } from 'react'
import { FlatList } from 'react-native'
import { TouchableOpacity } from 'react-native'
import { Text } from 'react-native'
import { View } from 'react-native'
import { BASE_API_URL } from '../api/request'
import { colors } from '../templates/colors'
import { fSize, percentHeight, percentWidth } from '../templates/helper'
import Snackbar from 'react-native-snackbar'
import Replenish from '../components/Replenish'
import { Image } from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import TouchableFocus from '../components/TouchableFocus'

const ProductsScreen = ({ navigation, route }) => {

    const fff = 'Пробная'

    const [products, setProducts] = useState([])
    const [replenish, setReplenish] = useState(false)
    const [refreshProducts, setRefreshProducts] = useState(false)

    const [goBackButtonRef, setGoBackButtonRef] = useState(null)
    const [replenishButtonRef, setReplenishButtonRef] = useState(null)
    const [productRefs, setProductRefs] = useState([])

    const [updateHeader, setUpdateHeader] = useState(false);

    const findRefFromArray = (arr, id) => {
        for (let i of arr) {
            if (i.id == id) {
                return i.ref
            }
        }
        return null
    }

    useEffect(() => {
        navigation.setOptions({
            title: 'Товары',
            headerRight: () => (
                <View
                    style={{
                        alignItems: 'center'
                    }}
                >
                    <TouchableFocus
                        style={{
                            height: percentHeight(4),
                            width: percentWidth(30),
                            justifyContent: 'center',
                            borderRadius: 5
                        }}
                        focusedStyle={{
                            transform: [{ scale: 1.3 }],
                        }}
                        focusedOnStart={!products.length}
                        onPress={() => {
                            navigation.navigate('replenish')
                        }}
                        setRef={setReplenishButtonRef}
                        nextFocusUpSelf
                        nextFocusRightSelf
                        nextFocusDownSelf={!products.length}
                        nextFocusLeft={goBackButtonRef}
                        nextFocusDown={findRefFromArray(productRefs, 0)}
                    >
                        <Text
                            style={{
                                color: colors.dark_text,
                                fontSize: fSize(7),
                                textAlign: 'center',
                                textDecorationColor: colors.dark_text,
                                textDecorationLine: 'underline'
                            }}
                        >
                            Пополнить счёт
                        </Text>
                    </TouchableFocus>
                </View>
            ),
            headerLeft: () => (
                <TouchableFocus
                    style={{
                        marginLeft: percentWidth(2),
                        borderRadius: 100,
                        width: percentWidth(3.5),
                        height: percentWidth(3.5),
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                    focusedStyle={{
                        backgroundColor: colors.dark_red,
                    }}
                    onPress={() => navigation.goBack()}
                    setRef={setGoBackButtonRef}
                    nextFocusLeftSelf
                    nextFocusUpSelf
                    nextFocusDownSelf={!products.length}
                    nextFocusRight={replenishButtonRef}
                    nextFocusDown={findRefFromArray(productRefs, 0)}
                >
                    <Icon name="arrow-back" color="white" size={fSize(10)} />
                </TouchableFocus>
            )
        });
    }, [updateHeader])
    // }, [findRefFromArray(productRefs, 0)])

    useEffect(() => {
        const get_products = async () => {
            try {
                const access_token = await AsyncStorage.getItem('access_token')
                const tempo = await fetch(`${BASE_API_URL}api/subscription_count`, { method: 'POST', headers: { 'Authorization': access_token, 'Accept': 'application/json' } })
                if (tempo.status == 401) {
                    await AsyncStorage.removeItem('access_token')
                    await AsyncStorage.removeItem('sid')
                    dispatch(setSid(''))
                    dispatch(setAuthState(false))
                    Snackbar.show({
                        text: 'Сессия истекла. Войдите заново',
                        duration: Snackbar.LENGTH_LONG,
                    })
                    return
                }
                const tempoJSON = await tempo.json()
                setProducts([])
                if (tempoJSON.success && !tempoJSON.subscriber_count) {
                    const tempoSubscribe = {
                        'name': '1 месяц',
                        'price': fff,
                    }
                    setProducts([tempoSubscribe])
                }
                let response = await fetch(`${BASE_API_URL}products`, { headers: { 'Authorization': access_token, 'Accept': 'application/json' } })
                if (response.status == 401) {
                    await AsyncStorage.removeItem('access_token')
                    await AsyncStorage.removeItem('sid')
                    dispatch(setSid(''))
                    dispatch(setAuthState(false))
                    Snackbar.show({
                        text: 'Сессия истекла. Войдите заново',
                        duration: Snackbar.LENGTH_LONG,
                    })
                    return
                }
                const responseJSON = await response.json()
                if (responseJSON.success) {
                    responseJSON.data.unshift()
                    setProducts((arr) => [...arr, ...responseJSON.data])
                }
            } catch (error) {
            }
        }
        get_products()
    }, [refreshProducts])

    const buyProduct = async (productID) => {
        try {
            const access_token = await AsyncStorage.getItem('access_token')
            const bodyForm = new FormData()
            bodyForm.append('product_id', productID)
            const response = await fetch(`${BASE_API_URL}api/orders`, { method: 'POST', body: bodyForm, headers: { 'Authorization': access_token, 'Content-Type': 'multipart/form-data' } })
            if (response.status == 401) {
                await AsyncStorage.removeItem('access_token')
                await AsyncStorage.removeItem('sid')
                dispatch(setSid(''))
                dispatch(setAuthState(false))
                Snackbar.show({
                    text: 'Сессия истекла. Войдите заново',
                    duration: Snackbar.LENGTH_LONG,
                })
                return
            }
            const responseJSON = await response.json()
            if (!responseJSON.success) {
                Snackbar.show({
                    text: responseJSON.message,
                    duration: Snackbar.LENGTH_LONG,
                    backgroundColor: '#F44336'
                })
                return;
            }
            Snackbar.show({
                text: responseJSON.message,
                duration: Snackbar.LENGTH_LONG,
                backgroundColor: '#4CAF50'
            })
        } catch (error) {
        }
    }

    const activateTemporarySubscription = async () => {
        try {
            const access_token = await AsyncStorage.getItem('access_token')
            const response = await fetch(`${BASE_API_URL}api/temporary_subscription`, { method: "POST", headers: { 'Authorization': access_token, 'Accept': 'application/json' } })
            if (response.status == 401) {
                await AsyncStorage.removeItem('access_token')
                await AsyncStorage.removeItem('sid')
                dispatch(setSid(''))
                dispatch(setAuthState(false))
                Snackbar.show({
                    text: 'Сессия истекла. Войдите заново',
                    duration: Snackbar.LENGTH_LONG,
                })
                return
            }
            const responseJSON = await response.json()
            if (responseJSON.success) {
                // await AsyncStorage.setItem('sid', responseJSON.sid)
                Snackbar.show({
                    text: 'Успешно активировано!',
                    backgroundColor: '#4CAF50',
                    duration: Snackbar.LENGTH_LONG
                })
                setRefreshProducts(!refreshProducts)
                return
            }
            Snackbar.show({
                text: 'Что-то пошло не так',
                backgroundColor: '#F44336',
                duration: Snackbar.LENGTH_LONG
            })
        } catch (error) {
        }
    }

    return (
        <View
            style={{
                flex: 1,
                backgroundColor: colors.primary,
            }}
        >
            <FlatList
                data={products}
                keyExtractor={(_, index) => index.toString()}
                contentContainerStyle={{
                    paddingTop: percentHeight(2),
                    marginHorizontal: percentWidth(2),
                }}
                numColumns={3}
                renderItem={({ item, index }) => (
                    <TouchableFocus
                        style={{
                            width: percentWidth(25),
                            height: percentHeight(30),
                            marginHorizontal: percentWidth(3.5),
                            backgroundColor: colors.dark_blue,
                            marginVertical: percentHeight(3),
                            alignItems: 'center',
                            paddingTop: percentHeight(1.5),
                            borderRadius: 3,
                            borderWidth: 2,
                            borderColor: colors.primary,
                        }}
                        focusedStyle={{
                            borderColor: colors.dark_red,
                        }}
                        focusedOnStart={!index}
                        activeOpacity={0.5}
                        setRef={(ref) => {
                            productRefs.push({ id: index, ref: ref })
                            if (!index) {
                                setUpdateHeader(!updateHeader)
                            }
                        }}
                        onPress={() => {
                            if (item.price == fff) {
                                activateTemporarySubscription()
                                return
                            }
                            buyProduct(item.id)
                        }}
                        nextFocusRightSelf={!((index + 1) % 3)}
                        nextFocusLeftSelf={!(index % 3)}
                        nextFocusDownSelf={((products.length - index) < 4)}
                        nextFocusRight={findRefFromArray(productRefs, index + 1)}
                        nextFocusLeft={findRefFromArray(productRefs, index - 1)}
                        nextFocusUp={((index < 3) ? goBackButtonRef : null)}
                    >
                        <View
                            style={{
                                width: '90%',
                                height: percentHeight(27),
                                backgroundColor: '#141414',
                                borderRadius: 5,
                            }}
                        >
                            <View
                                style={{
                                    alignItems: 'flex-end',
                                    width: '100%',
                                    height: '20%',
                                    paddingRight: percentWidth(2),
                                    paddingTop: percentHeight(1)
                                }}
                            >
                                <Image source={require('../img/logo.png')} style={{ height: '100%', width: '30%' }} resizeMode="center" />
                            </View>
                            <Text
                                style={{
                                    textAlign: 'center',
                                    color: colors.secondary,
                                    fontSize: fSize(7),
                                    marginTop: percentHeight(3)
                                }}
                            >
                                {item.name}
                            </Text>
                            <View
                                style={{
                                    width: '100%',
                                    alignItems: 'flex-end',
                                }}
                            >
                                <View
                                    style={{
                                        backgroundColor: colors.dark_red,
                                        width: '90%',
                                        height: percentHeight(0.2),
                                        marginTop: percentHeight(0.5)
                                    }}
                                />
                            </View>
                            <Text
                                style={{
                                    color: colors.secondary,
                                    fontSize: fSize(7),
                                    textAlign: 'center'
                                }}
                            >
                                {
                                    item.price == fff ? fff : `${Number(item.price).toFixed(0)} сомони`
                                }
                            </Text>
                            <View
                                style={{
                                    width: '20%',
                                    position: 'absolute',
                                    bottom: percentHeight(0.5),
                                    right: percentWidth(0)
                                }}
                            >
                                <Icon name="cart-outline" color={colors.dark_red} size={fSize(10)} />
                            </View>
                        </View>
                    </TouchableFocus>
                )}
            />
        </View>
    )
}

export default ProductsScreen