import React, { useEffect, useLayoutEffect, useState } from 'react';
import { View, Text } from 'react-native';
import {
  FlatList,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
} from 'react-native';
import { useDispatch, useSelector } from "react-redux";
import { request_get, request_post } from '../api/request';
import { IPTV_ADMIN_URL } from '../api/url';
import { colors } from '../templates/colors';
import { fSize, percentHeight, percentWidth } from '../templates/helper';
import Icon from 'react-native-vector-icons/Ionicons';
import Loader from '../components/Loader';
import MusicGroupFlatList from '../components/MusicGroupsFlatList';
import ScreenHeader from '../components/ScreenHeader';
import { SCALE_PARAM } from '../templates/scaleParameters'
import FocusableOpacity from '../components/FocusableOpacity';
import SidNotFound from '../components/SidNotFound';
import TouchableFocus from '../components/TouchableFocus';
import { chechToken } from "../components/helper";
import { setAuthState, setSid } from "../redux/actions";
import AsyncStorage from "@react-native-community/async-storage";
import sidValidation from "../services/sidChecker";

const MusicScreen = ({ navigation }) => {
  const sid = useSelector((state) => state.authReducer.sid);
  const [musicGroups, setMusicGroups] = useState([]);
  const [selectedMusicGroup, setSelectedMusicGroup] = useState({});
  const [musics, setMusics] = useState([]);
  const [searchText, setSearchText] = useState('');
  const [loader, setLoader] = useState(false);
  const [showSearch, setShowSearch] = useState(false);
  const [loadingData, setLoadingData] = useState(false)

  const [musicRefs, setMusicRefs] = useState([])

  const menuRef = useSelector(state => state.refReducer.menuRef);

  const dispatch = useDispatch();

  //Here we will check the access token
  useEffect(() => {
    const ff = async () => {
      let result = await chechToken()
      if (result) {
        dispatch(setAuthState(true))
      } else {
        dispatch(setAuthState(false))
      }
    }
    ff()
  }, []);

  //Here we will check the sid
  useEffect(() => {
    AsyncStorage.getItem('sid')
      .then((sid) => sidValidation(sid))
      .then(async ({ success, sid }) => {
        if (success) {
          dispatch(setSid(sid));
        } else {
          await AsyncStorage.removeItem('sid');
          navigation.navigate('subscribes');
        }
      })

  }, []);

  const findRefFromIndex = (arr, index) => {
    for (let i in arr) {
      if (arr[i].id == index) {
        return arr[i].ref;
      }
    }
    return null
  }

  // useEffect(() => {
  //   // musicRefs.length = 0;
  //   setMusicRefs([])
  // }, [selectedMusicGroup, musicGroups, musics])

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <View style={{ flexDirection: 'row' }}>
          <TouchableOpacity
            style={{ marginRight: 10 }}
            onPress={() => {
              setShowSearch(!showSearch)
              // request_post(
              //   `${IPTV_ADMIN_URL}music_vodfile_search/${sid}?name=${searchText}`,
              // ).then((res) => {
              // });
            }}>
            <Icon
              name={'search-outline'}
              size={25}
              color={colors.header_icons_color}
            />
          </TouchableOpacity>
        </View>
      ),
      headerTitle: () =>
        showSearch ? (
          <TextInput
            style={{
              fontSize: fSize(16),
              color: colors.secondary,
            }}
            value={searchText}
            placeholder="Введите музыку для поиска"
            placeholderTextColor={colors.dark_blue}
            onChangeText={setSearchText}
          />
        ) : (
          <Text
            style={{
              color: colors.secondary,
              fontSize: fSize(20),
            }}>
            Музыка
          </Text>
        ),
    });
  }, [showSearch, searchText]);

  useEffect(() => {
    console.log("Run Useeffect")
    getListGroups();
  },[]);

  const getListGroups = async () => {
    const response = await request_get('list-group')
    if (response.success) {
      setMusicGroups(response.data);
      if(response.data.length > 0){
        changeGroups(response.data[0])
      }
    }
  }

  const changeGroups = async (group) => {
    setMusics([]);
    setLoader(true);
    const response = await request_get(`music-by-group/${group.id}`)
    setLoader(false);
    setSelectedMusicGroup(group)
    if(response.success){
      setMusics(response.data)
    }
  }


       /* const musicGroups = data.map((musicGroup) => ({
          name: musicGroup.name,
          id: musicGroup.id,
        }));
        musicGroups.unshift({
          id: 0,
          name: 'Все альбомы',
        }),
          setMusicGroups(musicGroups);
        if (selectedMusicGroup.id === 0) {
          const musics = data
            .map((musicGroup) => musicGroup.music_datas)
            .flat(2);
          setMusics(musics);
        } else {
          const musics = data.find(
            (musicGroup) => musicGroup.id === selectedMusicGroup.id,
          )['music_datas'];
          setMusics(musics);
        }*/

  /*useEffect(() => {
    if (sid) {
      setLoadingData(true)
      setMusicRefs([])
      request_get(`musics_group/${sid}`).then(({ data, success }) => {
        setLoadingData(false)
        if (success) {
          const musicGroups = data.map((musicGroup) => ({
            name: musicGroup.name,
            id: musicGroup.id,
          }));
          musicGroups.unshift({
            id: 0,
            name: 'Все альбомы',
          }),
            setMusicGroups(musicGroups);
          if (selectedMusicGroup.id === 0) {
            const musics = data
              .map((musicGroup) => musicGroup.music_datas)
              .flat(2);
            setMusics(musics);
          } else {
            const musics = data.find(
              (musicGroup) => musicGroup.id === selectedMusicGroup.id,
            )['music_datas'];
            setMusics(musics);
          }
        }
      });
    }
  }, [selectedMusicGroup, sid]);*/

  // const [categoryFlatList, setCategoryFlatList] = useState(getCategoryFlatlist())

  // useEffect(() => {
  //   setCategoryFlatList(categoryFlatList)
  // }, [musicGroups, selectedMusicGroup])

  // useEffect(() => {
  //   const ttt = musicGroups
  //   setMusicGroups([])
  //   setMusicGroups(ttt)
  // }, [selectedMusicGroup.id])

  const changeNameMusic = (name) => {
    let arr_name = name.split('');
    if (arr_name.length < 15) {
      return name;
    }
    // let cutname = arr_name.splice(0, 17).join('');
    let cutname = arr_name.splice(0, 12).join('');
    return cutname + '...';
  };
  const changeNameMusicSearch = (name) => {
    let arr_name = name.split('');
    if (arr_name.length < 40) {
      return name;
    }
    let cutname = arr_name.splice(0, 40).join('');

    return cutname + ' ...';
  };

  if (searchText.length > 0 && !showSearch) {
    const foundedMusic = musics
      .map((music) => music.music_vodfiles)
      .flat(1)
      .filter((music) => music.file_torrent.includes(searchText));

    const searchResult = foundedMusic.map((music) => {
      const album = musics.find((mu) => +mu.id === +music.data_id);
      return {
        ...music,
        poster: album.poster,
        album,
      };
    });
    return (
      <View
        style={{
          flex: 1,
          backgroundColor: colors.primary,
        }}>
        <ScrollView>
          {searchResult.length === 0 ? (
            <Text
              style={{
                color: colors.secondary,
                fontSize: fSize(16),
              }}>
              По вашему запросу ничего не найдено
            </Text>
          ) : (
            <FlatList
              numColumns={5}
              data={searchResult}
              keyExtractor={(item) => item.id.toString()}
              renderItem={({ item, index }) => (
                <TouchableFocus
                  onPress={() => {
                    navigation.navigate('mplayer', {
                      musics: searchResult,
                      selected: item.id,
                      poster: item.poster,
                      album: item.album,
                    });
                  }}
                  style={{
                    flex: 1,
                    // flexDirection: 'row',
                    alignItems: 'center',
                  }}
                  focusedStyle={{
                    transform: [{ scale: 1.2 }]
                  }}
                  focusedOnStart={index ? false : true}
                >
                  <Image
                    source={{
                      uri: `${IPTV_ADMIN_URL}posters/music/${item.poster}`,
                    }}
                    style={{
                      height: percentHeight(10),
                      width: percentWidth(22),
                      resizeMode: 'cover',
                      borderRadius: 10,
                    }}
                    accessible={false}
                  />
                  <Text
                    style={{
                      color: colors.secondary,
                      fontSize: fSize(7),
                      marginLeft: 10,
                    }}>
                    {changeNameMusicSearch(item.file_torrent)}
                  </Text>
                </TouchableFocus>
                // </View>
              )}
            />
          )}
        </ScrollView>
      </View>
    );
  }
  const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 20;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };


  return !sid ? <SidNotFound /> : (
    <View
      style={{
        flex: 1,
        backgroundColor: '#12101C',
      }}>
      {
        loadingData ?
          <Loader />
          :
          (
            <>
              <View
                style={{
                  paddingLeft: percentWidth(2)
                }}
              >
                <ScreenHeader screenName="Музыка" />
              </View>
              <View>
                <FlatList
                  data={musicGroups}
                  keyExtractor={(item) => item.id.toString()}
                  horizontal={true}
                  style={{
                    marginLeft: percentHeight(4),
                  }}
                  contentContainerStyle={{
                    paddingVertical: percentHeight(3),
                    paddingLeft: percentWidth(1)
                  }}
                  renderItem={({ item, index }) => (
                    <TouchableFocus
                      style={{
                        height: percentHeight(5),
                        borderRadius: percentWidth(5),
                        marginHorizontal: 5,
                        justifyContent: 'center',
                        alignItems: 'center',
                        backgroundColor:
                          selectedMusicGroup.id === item.id
                            ? '#AA0008'
                            : '#12101C',
                      }}
                      focusedStyle={{
                        transform: [{ scale: SCALE_PARAM }]
                      }}
                      onPress={() => {
                        changeGroups(item)
                      }}
                      nextFocusRightSelf={(index == (musicGroups.length - 1))}
                      nextFocusUpSelf
                    >
                      <Text
                        style={{
                          color:
                            selectedMusicGroup.id === item.id ?
                              'white'
                              :
                              '#726A9C',
                          fontSize: fSize(6),
                          padding: 10,
                        }}>
                        {item.name}
                      </Text>
                    </TouchableFocus>
                  )}
                />
              </View>
              {/* <View
            style={{
              // paddingRight: 10,
              paddingBottom: percentHeight(10),
            }}> */}
              <ScrollView
                showsVerticalScrollIndicator={false}
                onScroll={async ({ nativeEvent }) => {
                  if (isCloseToBottom(nativeEvent)) {
                    setLoader(true);
                    setTimeout(() => {
                      setLoader(false);
                    }, 1000);
                  }
                }}
                contentContainerStyle={{
                  alignItems: 'center',
                  // paddingBottom: percentHeight(20)
                  // backgroundColor: 'yellow'
                }}
              >
                <FlatList
                  data={musics}
                  // horizontal
                  numColumns={5}
                  style={{
                    paddingBottom: percentHeight(10)
                  }}
                  keyExtractor={(item) => item.id.toString()}
                  renderItem={({ item, index }) => (
                    <TouchableFocus
                      style={{
                        borderTopEndRadius: 5,
                        borderTopStartRadius: 5,
                        // borderRadius: 10,
                        marginTop: percentHeight(3),
                        justifyContent: 'center',
                        alignItems: 'center',
                        marginRight: percentWidth(1),
                        marginLeft: percentWidth(1),
                        paddingHorizontal: percentWidth(1),
                        width: percentWidth(16),
                        height: percentHeight(37),
                        justifyContent: 'space-around',
                        alignItems: 'center',
                        borderWidth: 1,
                        borderColor: colors.primary,
                      }}
                      focusedStyle={{
                        // transform: [{ scale: SCALE_PARAM }]
                        borderColor: colors.dark_red
                      }}
                      focusedOnStart={!index}
                      onPress={() =>
                        navigation.navigate('albums', {
                          music_id:item.id,
                          /*musics: item.music_vodfiles,
                          poster: item.poster,
                          album: item,*/
                        })
                      }
                      setRef={(ref) => {
                        musicRefs.push({ ref: ref, id: index })
                      }}
                      nextFocusRightSelf={(!((index + 1) % 5) || ((index + 1) == musics.length))}
                      nextFocusDownSelf={((musics.length - index) < 6)}
                      nextFocusLeft={((index % 5) ? findRefFromIndex(musicRefs, index - 1) : menuRef)}
                      nextFocusUp={((index > 4) ? findRefFromIndex(musicRefs, index - 5) : null)}
                      nextFocusRight={(((index + 1) % 5) ? findRefFromIndex(musicRefs, index + 1) : null)}
                      nextFocusDown={(((index + 5) < musics.length) ? findRefFromIndex(musicRefs, index + 5) : findRefFromIndex(musicRefs, index))}
                    >
                      <Image
                        source={{
                          uri: `${IPTV_ADMIN_URL}posters/music/${item.poster}`,
                        }}
                        style={{
                          height: percentHeight(25),
                          width: percentHeight(25),
                          resizeMode: 'cover',
                        }}
                      />
                      <View
                        style={{
                          width: '100%',
                          alignItems: 'flex-start'
                        }}
                      >
                        <Text
                          style={{
                            color: colors.secondary,
                            fontSize: fSize(6),
                            textAlign: 'left',
                            marginTop: percentHeight(1),
                          }}>
                          {changeNameMusic(item.album)}
                        </Text>
                      </View>
                      {/* </View> */}
                    </TouchableFocus>
                  )}
                />
              </ScrollView>
            </>
          )
      }
      {loader ? (
        <View style={{ height: percentHeight(10) }}>
          <Loader />
        </View>
      ) : null}
    </View>
    // </View>
  );
};

export default MusicScreen;
