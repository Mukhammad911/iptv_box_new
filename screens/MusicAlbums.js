import React, { useEffect, useLayoutEffect, useState } from 'react';
import {
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  PermissionsAndroid,
  Modal,
  Alert,
  findNodeHandle,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { request_get, request_post } from '../api/request';
import { IPTV_ADMIN_URL } from '../api/url';
import { colors } from '../templates/colors';
import { globalStyles } from '../templates/globalStyles';
import { fSize, percentHeight, percentWidth } from '../templates/helper';
import Icon from 'react-native-vector-icons/Ionicons';
import RNFetchBlob from 'rn-fetch-blob';
import { ScrollView } from 'react-native-gesture-handler';
import Loader from '../components/Loader';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import { getFavoritesMusic, setAuthState, setSid } from "../redux/actions";
import MusicPlayer from './MusicPlayer';
import FocusableOpacity from '../components/FocusableOpacity';
import TouchableFocus from '../components/TouchableFocus';
import { chechToken } from "../components/helper";
import AsyncStorage from "@react-native-community/async-storage";
import sidValidation from "../services/sidChecker";

const MusicAlbums = ({ navigation, route }) => {
  const { sid } = useSelector((state) => state.authReducer);
  //const { musics, poster, album } = route.params;

  const [musics, setMusics] = useState();
  const [poster, setPoster] = useState();
  const [album, setAlbum] = useState();

  const { music_id } = route.params

  useEffect(() => {
      getMusic()
  },[]);

  const getMusic = async () => {
    console.log(music_id)
    const response = await request_get(`music-once-by-group/${music_id}`)
    console.log(response);
    if (response.success) {
      setPoster(response.data[0].poster);
      setMusics(response.data[0].music_vodfiles)
      setAlbum(response.data[0]);
    }
  }

  useEffect(() => {

  },[musics, poster, album])

  const [musicId, setMusicId] = useState('');
  const [modalVisible, setModalVisible] = useState(false);
  const [listloader, setlistloader] = useState(false);
  const [showMPlayer, setShowMPlayer] = useState(false);
  const [selected, setSelected] = useState(0)
  const dispatch = useDispatch();

  const [skipBackButtonRef, setSkipBackButtonRef] = useState(null)
  const [albumFirstElementRef, setAlbumFirstElementRef] = useState(null)
  const [albumRefs, setAlbumRefs] = useState([])

  // -----------------------------------

  const [previousButtonRef, setPreviousButtonRef] = useState(null)
  const [playBackButtonRef, setPlayBackButtonRef] = useState(null)
  const [playButtonRef, setPlayButtonRef] = useState(null)
  const [playForwardButtonRef, setPlayForwardButtonRef] = useState(null)
  const [nextButtonRef, setNextButtonRef] = useState(null)
  const [repeatButtonRef, setRepeatButtonRef] = useState(null)
  const [favoriteButtonRef, setFavoriteButtonRef] = useState(null)
  const [downloadButtonRef, setDownloadButtonRef] = useState(null)

  //Here we will check the access token
  useEffect(() => {
    const ff = async () => {
      let result = await chechToken()
      if (result) {
        dispatch(setAuthState(true))
      } else {
        dispatch(setAuthState(false))
      }
    }
    ff()
  }, []);

  //Here we will check the sid
  useEffect(() => {
    AsyncStorage.getItem('sid')
      .then((sid) => sidValidation(sid))
      .then(async ({ success, sid }) => {
        if (success) {
          dispatch(setSid(sid));
        } else {
          await AsyncStorage.removeItem('sid');
          navigation.navigate('subscribes');
        }
      })

  }, []);

  // -----------------------------------

  const findRefFromIndex = (arr, index) => {
    for (let i in arr) {
      if (arr[i].id == index) {
        return arr[i].ref;
      }
    }
    return null
  }

  useEffect(() => {
    if (showMPlayer) {
      for (let i = 0; i < 5; i++) {
        if (musics.length > i) {
          findRefFromIndex(albumRefs, i).setNativeProps({
            nextFocusUp: findNodeHandle(playButtonRef)
          })
        }
      }
    }
  }, [playButtonRef])

  useLayoutEffect(() => {
    navigation.setOptions({
      headerShown: true,
      title: 'Альбомы',
      headerLeft: () => (
        <TouchableFocus
          style={{
            marginLeft: percentWidth(2),
            height: percentWidth(4),
            width: percentWidth(4),
            justifyContent: 'center',
            alignItems: 'center',
            borderRadius: 100,
          }}
          focusedStyle={{
            backgroundColor: colors.dark_red
          }}
          setRef={setSkipBackButtonRef}
          nextFocusLeftSelf
          nextFocusUpSelf
          nextFocusRightSelf
          // nextFocusDown={albumFirstElementRef}
          nextFocusDown={findRefFromIndex(albumRefs, 0)}
          onPress={() => {
            navigation.goBack()
          }}
        >
          <Icon name={'arrow-back'} color='white' size={fSize(10)} />
        </TouchableFocus>
      )
    });
  }, [navigation, route, showMPlayer, playButtonRef]);

  /*useEffect(() => {
    if (musics.length == albumRefs.length) {
      setAlbumRefs(albumRefs)
    }
  }, [musics, albumRefs.length])*/

  const changeNameMusic = (name) => {
    let arr_name = name.split('');
    if (arr_name.length < 30) {
      return name;
    }
    let cutname = arr_name.splice(0, 28).join('');

    return cutname + ' ...';
  };

  /*if (!musics.length) {
    return (
      <View
        style={{
          ...globalStyles.container,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text style={{ fontSize: fSize(7), color: colors.secondary }}>
          В данном альбоме нет музыки
        </Text>
      </View>
    );
  }*/

  const downloadMusic = async () => {
    const { data } = await request_get(`music_vodfile_url/${musicId}/${sid}`);
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      {
        title: 'Storage Permission Required',
        message: 'App needs access to your storage to download Photos',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      // Once user grant the permission start downloading
      const date = new Date();
      const { config, fs } = RNFetchBlob;
      let PictureDir = fs.dirs.MusicDir;
      let options = {
        fileCache: true,
        addAndroidDownloads: {
          // Related to the Android only
          useDownloadManager: true,
          notification: true,
          path:
            PictureDir +
            '/music_' +
            Math.floor(date.getTime() + date.getSeconds() / 2) +
            '.mp3',
          description: 'Music',
        },
      };
      config(options)
        .fetch('GET', data)
        .then((res) => {
          // Showing alert after successful downloading
          Alert.alert('Успех', 'Успешно скачан');
        });
    }
  };

  const addToFavorite = () => {
    request_post(`musics_favorite/${sid}?music_vodfile_id=${musicId}`).then(
      (res) => {
        if (res.success) {
          dispatch(getFavoritesMusic(sid));
          Alert.alert('Успех', 'Успешно добавлен в избранное');
        }
      },
    );
  };
  const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
    const paddingToBottom = 20;
    return (
      layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom
    );
  };

  return (
    <View
      style={{
        ...globalStyles.container,
        paddingRight: 10,
        paddingBottom: 30,
      }}>
      {
        showMPlayer ?
          <View
            style={{
              backgroundColor: colors.dark_blue,
              width: '100%',
              height: percentHeight(20)
            }}
            focusable={false}
          >
            <MusicPlayer
              navigation={navigation}
              route={route}
              album={album}
              musics={musics}
              selected={selected}
              previousButtonRef={previousButtonRef}
              setPreviousButtonRef={setPreviousButtonRef}
              playBackButtonRef={playBackButtonRef}
              setPlayBackButtonRef={setPlayBackButtonRef}
              playButtonRef={playButtonRef}
              setPlayButtonRef={setPlayButtonRef}
              playForwardButtonRef={playForwardButtonRef}
              setPlayForwardButtonRef={setPlayForwardButtonRef}
              nextButtonRef={nextButtonRef}
              setNextButtonRef={setNextButtonRef}
              repeatButtonRef={repeatButtonRef}
              setRepeatButtonRef={setRepeatButtonRef}
              favoriteButtonRef={favoriteButtonRef}
              setFavoriteButtonRef={setFavoriteButtonRef}
              downloadButtonRef={downloadButtonRef}
              setDownloadButtonRef={setDownloadButtonRef}
              albumFirstElementRef={findRefFromIndex(albumRefs, 0)}
              skipBackButtonRef={skipBackButtonRef}
            />
          </View>
          :
          null
      }
      <ScrollView
        onScroll={async ({ nativeEvent }) => {
          if (isCloseToBottom(nativeEvent)) {
            setlistloader(true);
            setTimeout(() => {
              setlistloader(false);
            }, 1000);
          }
        }}
        contentContainerStyle={{
          alignItems: "center"
        }}
      >
        <FlatList
          data={musics}
          keyExtractor={(item) => item.id.toString()}
          numColumns={5}
          contentContainerStyle={{
            // paddingTop: percentHeight(3.6)
            paddingHorizontal: percentWidth(2)
          }}
          renderItem={({ item, index }) => (
            <TouchableFocus
              focusedOnStart={!index}
              onPress={() => {
                // navigation.navigate('mplayer', {
                //   musics,
                //   selected: item.id,
                //   poster,
                //   album,
                // });
                setSelected(item.id)
                setShowMPlayer(true)
              }}
              style={{
                // flex: 1,
                alignItems: 'center',
                width: percentWidth(16),
                height: percentHeight(37),
                marginHorizontal: percentWidth(1),
                marginTop: percentHeight(3.7),
                paddingTop: percentHeight(1.5),
                borderWidth: 1,
                borderColor: colors.primary,
                paddingHorizontal: percentWidth(1),
                borderTopLeftRadius: 7,
                borderTopRightRadius: 7,
              }}
              // focusedStyle={{
              //   transform: [{ scale: 1.2 }]
              // }}
              focusedStyle={{
                borderColor: colors.dark_red,
              }}
              // setRef={(!index ? setAlbumFirstElementRef : (() => null))}
              setRef={(reff) => {
                albumRefs.push({ id: index, ref: reff })
              }}
              nextFocusLeftSelf={(!(index % 5))}
              nextFocusRightSelf={!((index + 1) % 5)}
              nextFocusDownSelf={((musics.length - index) < 6)}
              nextFocusLeft={findRefFromIndex(albumRefs, index - 1)}
              nextFocusRight={findRefFromIndex(albumRefs, index + 1)}
              nextFocusUp={((musics.length - index) < 6 ? (showMPlayer ? playButtonRef : skipBackButtonRef) : null)}
              // nextFocusUp={((musics.length - index) < 6 ? (showMPlayer ? skipBackButtonRef : skipBackButtonRef) : null)}
            >
              <Image
                source={{
                  uri: `${IPTV_ADMIN_URL}posters/music/${poster}`,
                }}
                style={{
                  height: percentHeight(25),
                  width: percentHeight(25),
                  resizeMode: 'cover',
                  borderRadius: 10,
                }}
              />
              <Text
                style={{
                  color: colors.secondary,
                  fontSize: fSize(5),
                  // marginLeft: 10,
                }}>
                {changeNameMusic(item.file_torrent)}
              </Text>
            </TouchableFocus>
          )}
        />
      </ScrollView>
      {listloader ? (
        <View style={{ backgroundColor: colors.primary }}>
          <Loader />
        </View>
      ) : null}
    </View>
  );
};

export default MusicAlbums;
