import React, { useEffect, useState } from 'react';
import { globalStyles } from '../templates/globalStyles';
import { View } from 'react-native';
import InfoUser from '../components/InfoUser';
import { request_post } from '../api/request';
import Loader from '../components/Loader';
import { useDispatch, useSelector } from "react-redux";
import sidChecker from '../services/sidChecker';
import { chechToken } from "../components/helper";
import { setAuthState, setSid } from "../redux/actions";
import AsyncStorage from "@react-native-community/async-storage";
import sidValidation from "../services/sidChecker";

const ProfileScreen = ({ navigation, route, HOCREF = null }) => {
  const [auth, setAuth] = useState(false);
  const [loader, setLoader] = useState(true);
  const sid = useSelector((state) => state.authReducer.sid);
  useEffect(() => {
    navigation.setOptions({
      title: 'Профиль',
    });
    sidChecker(sid);
  }, [sid]);

  const dispatch = useDispatch();

  //Here we will check the access token
  useEffect(() => {
    const ff = async () => {
      let result = await chechToken()
      if (result) {
        dispatch(setAuthState(true))
      } else {
        dispatch(setAuthState(false))
      }
    }
    ff()
  }, []);

  //Here we will check the sid
  useEffect(() => {
    AsyncStorage.getItem('sid')
      .then((sid) => sidValidation(sid))
      .then(async ({ success, sid }) => {
        if (success) {
          dispatch(setSid(sid));
        } else {
          await AsyncStorage.removeItem('sid');
          navigation.navigate('subscribes');
        }
      })

  }, []);

  return (
    <>
      {/* // <View style={globalStyles.container}> */}
      {loader ? <Loader /> : null}
      <InfoUser sid={sid} navigate={navigation.navigate} navigation={navigation} HOCREF={HOCREF} />
      {/* // </View> */}
    </>
  );
};

export default ProfileScreen;
