import React, { useEffect, useLayoutEffect, useRef, useState } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  PermissionsAndroid,
  Alert,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { request_get, request_post } from '../api/request';
import { IPTV_ADMIN_URL } from '../api/url';
import { colors } from '../templates/colors';
import { globalStyles } from '../templates/globalStyles';
import { fSize, percentHeight, percentWidth } from '../templates/helper';
import Icon from 'react-native-vector-icons/Ionicons';
import VideoPlayer from 'react-native-video'
import RNFetchBlob from 'rn-fetch-blob';
import { getFavoritesMusic, setAuthState, setSid } from "../redux/actions";
import FocusableOpacity from '../components/FocusableOpacity';
import { VLCPlayer } from '@nghinv/react-native-vlc';
import TouchableFocus from '../components/TouchableFocus';
import { chechToken } from "../components/helper";
import AsyncStorage from "@react-native-community/async-storage";
import sidValidation from "../services/sidChecker";

const MusicPlayer = ({
  navigation,
  route,
  musics,
  selected,
  poster,
  album,
  previousButtonRef = null,
  setPreviousButtonRef = () => null,
  playBackButtonRef = null,
  setPlayBackButtonRef = () => null,
  playButtonRef = null,
  setPlayButtonRef = () => null,
  playForwardButtonRef = null,
  setPlayForwardButtonRef = () => null,
  nextButtonRef = null,
  setNextButtonRef = () => null,
  repeatButtonRef = null,
  setRepeatButtonRef = () => null,
  favoriteButtonRef = null,
  setFavoriteButtonRef = () => null,
  downloadButtonRef = null,
  setDownloadButtonRef = () => null,
  albumFirstElementRef = null,
  skipBackButtonRef = null
}) => {
  const { sid } = useSelector((state) => state.authReducer);
  // const { musics, selected, poster, album } = route.params;
  const [selectedMusicId, setSelectedMusicId] = useState(selected);
  const [selectedMusic, setSelectedMusic] = useState(null);
  const [url, setUrl] = useState('');
  const [play, setPlay] = useState(true);
  const [currentTime, setCurrentTime] = useState(0.0);
  const [duration, setDuration] = useState(0);
  const [playerWidth, setPlayerWidth] = useState(0);
  const playerRef = useRef(null);
  const [muted, setMuted] = useState(false);
  const [repeat, setRepeat] = useState(false);
  const dispatch = useDispatch();
  const getCurrentTimePercentage = () => {
    if (currentTime > 0 && duration > 0) {
      return parseFloat(currentTime) / parseFloat(duration);
    }
    return 0;
  };

  //Here we will check the access token
  useEffect(() => {
    const ff = async () => {
      let result = await chechToken()
      if (result) {
        dispatch(setAuthState(true))
      } else {
        dispatch(setAuthState(false))
      }
    }
    ff()
  }, []);

  //Here we will check the sid
  useEffect(() => {
    AsyncStorage.getItem('sid')
      .then((sid) => sidValidation(sid))
      .then(async ({ success, sid }) => {
        if (success) {
          dispatch(setSid(sid));
        } else {
          await AsyncStorage.removeItem('sid');
          navigation.navigate('subscribes');
        }
      })

  }, []);

  useEffect(() => {
    setSelectedMusicId(selected)
  }, [selected])

  const changeNameMusic = (name) => {
    if (!name) {
      return
    }
    let arr_name = name.split('');
    if (arr_name.length < 30) {
      return name;
    }
    let cutname = arr_name.splice(0, 30).join('');

    return cutname + ' ...';
  };
  const onProgress = ({ currentTime, playableDuration }) => { setCurrentTime(currentTime) }
  useLayoutEffect(() => {
    navigation.setOptions({
      title: 'Плеер',
    });
  }, []);
  useEffect(() => {
    request_get(`music_vodfile_url/${selectedMusicId}/${sid}`).then(
      ({ data, success }) => {
        if (success) {
          const music = musics.find((music) => +music.id === +selectedMusicId);
          setSelectedMusic(music);
          setUrl(data);
          setPlay(true);
        }
      },
    );
  }, [selectedMusicId]);

  const twodigit = (digit) => (+digit > 9 ? `${digit}` : `0${digit}`);

  const iconSize = 8

  const getAudioTimeFormat = (duration) => {
    if (duration < 1) {
      return '00:00:00';
    }

    let timeFormat = '';

    const durationHours = Math.floor(duration / 3600);

    if (durationHours > 0) {
      timeFormat = twodigit(durationHours);
    }

    const durationMinutes = Math.floor((duration % 3600) / 60);

    if (durationMinutes > 0) {
      timeFormat =
        timeFormat.length > 0
          ? `${timeFormat}:${twodigit(durationMinutes)}`
          : `00:${twodigit(durationMinutes)}`;
    }

    const durationSeconds = Math.floor(
      duration - durationHours * 3600 - durationMinutes * 60,
    );

    if (durationSeconds > 0) {
      timeFormat =
        timeFormat.length > 0
          ? `${timeFormat}:${twodigit(durationSeconds)}`
          : `00:00:${twodigit(durationSeconds)}`;
    } else {
      timeFormat += ':00';
    }

    return timeFormat;
  };

  const getGenres = () => {
    return album.music_data_genres.map((genr) => genr.name).join(',');
  };

  const prevBtn = () => {
    let idx = musics.findIndex((music) => +music.id === +selectedMusicId);
    if (idx === 0) {
      setSelectedMusicId(musics[musics.length - 1].id);
      setSelectedMusic(musics[musics.length - 1]);
      playerRef.current.seek(0);
    } else if (idx > 0) {
      const music = musics[--idx];
      setSelectedMusicId(music.id);
      setSelectedMusic(music);
    }
  };

  const nextBtn = () => {
    let idx = musics.findIndex((music) => music.id === selectedMusicId);
    if (idx === musics.length - 1) {
      setSelectedMusicId(musics[0].id);
      setSelectedMusic(musics[0]);
      // playerRef.current.seek(0);
    } else if (idx < musics.length - 1) {
      const music = musics[++idx];
      setSelectedMusicId(music.id);
      setSelectedMusic(music);
    }
  };

  const downloadMusic = async (id) => {
    const { data } = await request_get(`music_vodfile_url/${id}/${sid}`);
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      {
        title: 'Storage Permission Required',
        message: 'App needs access to your storage to download Photos',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      // Once user grant the permission start downloading
      const date = new Date();
      const { config, fs } = RNFetchBlob;
      let PictureDir = fs.dirs.MusicDir;
      let options = {
        fileCache: true,
        addAndroidDownloads: {
          // Related to the Android only
          useDownloadManager: true,
          notification: true,
          path:
            PictureDir +
            '/music_' +
            Math.floor(date.getTime() + date.getSeconds() / 2) +
            '.mp3',
          description: 'Music',
        },
      };
      config(options)
        .fetch('GET', data)
        .then((res) => {
          // Showing alert after successful downloading
          Alert.alert('Успех', 'Музыка успешно загружена');
        });
    }
  };

  const addToFavorite = () => {
    request_post(
      `musics_favorite/${sid}?music_vodfile_id=${selectedMusicId}`,
    ).then((res) => {
      if (res.success) {
        dispatch(getFavoritesMusic(sid))
        Alert.alert('Успех', 'Успешно добавлен в избранное');

      }
    });
  };

  return (
    <View
      style={{
        flex: 1,
        height: percentHeight(20),
        backgroundColor: colors.dark_blue,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: percentWidth(3)
      }}
      focusable={false}
    >
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          marginBottom: percentHeight(2)
        }}
        focusable={false}
      >
        <View
          style={{
            width: percentWidth(33),
          }}
          focusable={false}
        >
          <Text
            style={{
              color: colors.secondary,
              fontSize: fSize(7),
            }}>
            {changeNameMusic(selectedMusic?.file_torrent)}
            {/* {changeNameMusic(selectedMusic?.file_torrent.split('-')[1].trim())} */}
          </Text>
          <Text
            style={{
              color: colors.dark_text,
              fontSize: fSize(5)
            }}
          >
            {changeNameMusic(selectedMusic?.file_torrent)}
            {/* {selectedMusic?.file_torrent.split('-')[0].trim()} */}
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            width: percentWidth(33),
          }}
          focusable={false}
        >
          <TouchableFocus
            style={{ borderRadius: 100, width: percentWidth(4), height: percentWidth(4), justifyContent: 'center', alignItems: 'center' }}
            // focusedStyle={{ backgroundColor: colors.dark_red }}
            focusedStyle={{ transform: [{ scale: 1.3 }] }}
            onPress={prevBtn}
            setRef={setPreviousButtonRef}
            nextFocusLeftSelf
            nextFocusUp={skipBackButtonRef}
            nextFocusRight={playBackButtonRef}
            nextFocusDown={albumFirstElementRef}
          >
            <Icon
              name={'play-back'}
              size={fSize(iconSize)}
              color={colors.secondary}
            />
          </TouchableFocus>
          <TouchableFocus
            style={{ borderRadius: 100, width: percentWidth(4), height: percentWidth(4), justifyContent: 'center', alignItems: 'center' }}
            // focusedStyle={{ backgroundColor: colors.dark_red }}
            focusedStyle={{ transform: [{ scale: 1.3 }] }}
            onPress={() => playerRef.current.seek(currentTime - 10)}
            style={{ marginLeft: percentWidth(3) }}
            setRef={setPlayBackButtonRef}
            nextFocusLeft={previousButtonRef}
            nextFocusUp={skipBackButtonRef}
            nextFocusRight={playButtonRef}
            nextFocusDown={albumFirstElementRef}
          >
            <Icon
              name={'play-skip-back'}
              size={fSize(iconSize)}
              color={colors.secondary}
            />
          </TouchableFocus>
          <TouchableFocus
            style={{ borderRadius: 100, width: percentWidth(4), height: percentWidth(4), justifyContent: 'center', alignItems: 'center' }}
            // focusedStyle={{ backgroundColor: colors.dark_red }}
            focusedStyle={{ transform: [{ scale: 1.3 }] }}
            onPress={() => setPlay(!play)}
            style={{
              marginHorizontal: percentWidth(3)
            }}
            focusedOnStart
            setRef={setPlayButtonRef}
            nextFocusLeft={playBackButtonRef}
            nextFocusUp={skipBackButtonRef}
            nextFocusRight={playForwardButtonRef}
            nextFocusDown={albumFirstElementRef}
          >
            {!play ? (
              <Icon name={'play'} size={fSize(iconSize)} color={colors.secondary} />
            ) : (
              <Icon
                name={'pause-outline'}
                color={colors.secondary}
                size={fSize(iconSize)}
              />
            )}
          </TouchableFocus>
          <TouchableFocus
            style={{ borderRadius: 100, width: percentWidth(4), height: percentWidth(4), justifyContent: 'center', alignItems: 'center' }}
            // focusedStyle={{ backgroundColor: colors.dark_red }}
            focusedStyle={{ transform: [{ scale: 1.3 }] }}
            onPress={() => playerRef.current.seek(+currentTime + +10)}
            style={{ marginRight: percentWidth(3) }}
            setRef={setPlayForwardButtonRef}
            nextFocusLeft={playButtonRef}
            nextFocusUp={skipBackButtonRef}
            nextFocusRight={nextButtonRef}
            nextFocusDown={albumFirstElementRef}
          >
            <Icon
              name={'play-skip-forward'}
              size={fSize(iconSize)}
              color={colors.secondary}
            />
          </TouchableFocus>
          <TouchableFocus
            style={{ borderRadius: 100, width: percentWidth(4), height: percentWidth(4), justifyContent: 'center', alignItems: 'center' }}
            // focusedStyle={{ backgroundColor: colors.dark_red }}
            focusedStyle={{ transform: [{ scale: 1.3 }] }}
            onPress={nextBtn}
            setRef={setNextButtonRef}
            nextFocusLeft={playForwardButtonRef}
            nextFocusUp={skipBackButtonRef}
            nextFocusRight={repeatButtonRef}
            nextFocusDown={albumFirstElementRef}
          >
            <Icon
              name={'play-forward'}
              size={fSize(iconSize)}
              color={colors.secondary}
            />
          </TouchableFocus>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-end',
            alignItems: 'center',
            width: percentWidth(33),
          }}
          focusable={false}
        >
          <TouchableFocus
            style={{ borderRadius: 100, width: percentWidth(4), height: percentWidth(4), justifyContent: 'center', alignItems: 'center' }}
            // focusedStyle={{ backgroundColor: colors.dark_red }}
            focusedStyle={{ transform: [{ scale: 1.3 }] }}
            onPress={() => {
              setRepeat(!repeat);
            }}
            setRef={setRepeatButtonRef}
            nextFocusLeft={nextButtonRef}
            nextFocusUp={skipBackButtonRef}
            nextFocusRight={favoriteButtonRef}
            nextFocusDown={albumFirstElementRef}
          >
            <Icon
              name={'repeat-outline'}
              color={repeat ? colors.dark_red : colors.secondary}
              size={fSize(10)}
            />
          </TouchableFocus>
          <TouchableFocus
            style={{ borderRadius: 100, width: percentWidth(4), height: percentWidth(4), justifyContent: 'center', alignItems: 'center' }}
            // focusedStyle={{ backgroundColor: colors.dark_red }}
            focusedStyle={{ transform: [{ scale: 1.3 }] }}
            onPress={addToFavorite}
            style={{ marginLeft: percentWidth(3) }}
            setRef={setFavoriteButtonRef}
            nextFocusLeft={repeatButtonRef}
            nextFocusUp={skipBackButtonRef}
            nextFocusRight={downloadButtonRef}
            nextFocusDown={albumFirstElementRef}
          >
            <Icon
              name={'star-outline'}
              size={fSize(iconSize)}
              color={colors.secondary}
            />
          </TouchableFocus>
          <TouchableFocus
            style={{ borderRadius: 100, width: percentWidth(4), height: percentWidth(4), justifyContent: 'center', alignItems: 'center' }}
            // focusedStyle={{ backgroundColor: colors.dark_red }}
            focusedStyle={{ transform: [{ scale: 1.3 }] }}
            onPress={() => downloadMusic(selectedMusicId)}
            style={{ marginLeft: percentWidth(3) }}
            setRef={setDownloadButtonRef}
            nextFocusRightSelf
            nextFocusLeft={favoriteButtonRef}
            nextFocusUp={skipBackButtonRef}
            nextFocusDown={albumFirstElementRef}
          >
            <Icon
              name={'download-outline'}
              size={fSize(iconSize)}
              color={colors.secondary}
            />
          </TouchableFocus>
        </View>
      </View>
      <View
        style={{
          position: 'relative',
          width: '100%',
          height: percentHeight(5),
        }}
        focusable={false}
      >
        <View
          style={{
            position: 'absolute',
            width: '100%',
          }}
          focusable={false}
        >
          <TouchableOpacity
            onLayout={({ nativeEvent }) => {
              const { layout } = nativeEvent;
              setPlayerWidth(layout.width);
            }}
            onPress={({ nativeEvent }) => {
              const { pageX, locationX } = nativeEvent;
              const seek = parseFloat(pageX / playerWidth) * duration - 10;
              playerRef.current.seek(seek);
              setPlay(true);
            }}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                borderRadius: 500,
                alignItems: 'center',
                overflow: 'hidden',
              }}
              focusable={false}
            >
              <View
                style={{
                  flex: getCurrentTimePercentage() * 100,
                  backgroundColor: colors.dark_red,
                  height: percentHeight(1),
                }}
                focusable={false}
              />
              <View
                style={{
                  borderRadius: 15,
                  borderWidth: 1,
                  borderColor: colors.secondary,
                  backgroundColor: colors.dark_red,
                  width: percentHeight(3),
                  height: percentHeight(3),
                  zIndex: 999,
                }}
                focusable={false}
              />
              <View
                style={{
                  flex: (1 - getCurrentTimePercentage()) * 100,
                  height: percentHeight(1),
                  backgroundColor: colors.dark_text,
                }}
                focusable={false}
              />
            </View>
          </TouchableOpacity>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '100%',
            }}
            focusable={false}
          >
            <Text
              style={{
                color: colors.secondary,
                fontSize: fSize(5)
              }}
            >
              {getAudioTimeFormat(currentTime)}
            </Text>
            <Text
              style={{
                color: colors.secondary,
                fontSize: fSize(5)
              }}>
              {getAudioTimeFormat(duration)}
            </Text>
          </View>
        </View>
      </View>
      {url ? (
        <VideoPlayer
          onLoad={({ duration }) => {
            setDuration(duration)
          }}
          muted={muted}
          ref={playerRef}
          onProgress={onProgress}
          source={{ uri: url.replace('https', 'http') }}
          audioOnly={true}
          paused={!play}
          repeat={repeat}
          onEnd={() => {
            nextBtn();
          }}
          onError={err => {
          }}
        />
      ) : null}
    </View>
  );
};

export default MusicPlayer;
