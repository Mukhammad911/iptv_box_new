import AsyncStorage from "@react-native-community/async-storage";
import React, { useEffect, useState } from "react";
import { FlatList } from "react-native";
import { Text } from "react-native";
import { View } from "react-native";
import { TouchableOpacity } from "react-native";
import { BASE_API_URL } from "../api/request";
import { colors } from "../templates/colors";
import { fSize, percentHeight, percentWidth } from "../templates/helper";
import Snackbar from "react-native-snackbar";
import { useDispatch, useSelector } from "react-redux";
import { setAuthState, setSid } from "../redux/actions";
import Icon from "react-native-vector-icons/Ionicons";
import { Image } from "react-native";
import TouchableFocus from "../components/TouchableFocus";
import { chechToken } from "../components/helper";
import sidValidation from "../services/sidChecker";

const SubscribesScreen = ({ navigation, route }) => {

  const dispatch = useDispatch();
  const sid = useSelector((state) => state.authReducer.sid);
  const [subscribes, setSubscribes] = useState([]);
  const [sidIsCorrect, setSidIsCorrect] = useState(false);
  const [activatedSubscription, setActivatedSubscription] = useState(Infinity);
  const [showTempo, setShowTempo] = useState(false);

  const [firstSubscribeRef, setFirstSubscribeRef] = useState(null);

  const [goBackButtonRef, setGoBackButtonRef] = useState(null);

  const [noSubscribeButtonRef, setNoSubscribeButtonRef] = useState(null);

  useEffect(() => {
    navigation.setOptions({
      title: "Подписки",
      headerRight: () => (
        <View
          style={{
            alignItems: "center",
          }}
        >
          <TouchableFocus
            setRef={setNoSubscribeButtonRef}
            style={{
              height: percentHeight(4),
              width: percentWidth(30),
              justifyContent: "center",
              borderRadius: 5,
            }}
            focusedStyle={{
              transform: [{ scale: 1.3 }],
            }}
            onPress={() => {
              navigation.navigate("products");
            }}
            focusedOnStart={!subscribes.length}
            nextFocusUpSelf
            nextFocusRightSelf
            nextFocusDownSelf={!subscribes.length}
            nextFocusLeft={goBackButtonRef}
            nextFocusDown={firstSubscribeRef}
          >
            <Text
              style={{
                color: colors.dark_text,
                fontSize: fSize(7),
                textAlign: "center",
                textDecorationColor: colors.dark_text,
                textDecorationLine: "underline",
              }}
            >
              Нет подписки?
            </Text>
          </TouchableFocus>
        </View>
      ),
      headerLeft: () => (
        <TouchableFocus
          setRef={setGoBackButtonRef}
          style={{
            marginLeft: percentWidth(2),
            borderRadius: 100,
            width: percentWidth(3.5),
            height: percentWidth(3.5),
            justifyContent: "center",
            alignItems: "center",
          }}
          focusedStyle={{
            backgroundColor: colors.dark_red,
          }}
          onPress={() => {
            navigation.goBack();
          }}
          nextFocusLeftSelf
          nextFocusUpSelf
          nextFocusDown={firstSubscribeRef}
          nextFocusDownSelf={(subscribes.length ? false : true)}
          nextFocusRight={noSubscribeButtonRef}
        >
          <Icon name="arrow-back" color="white" size={fSize(10)} />
        </TouchableFocus>
      ),
    });
  }, [firstSubscribeRef]);

  useEffect(() => {
    const get_subscriptions = async () => {
      try {
        let access_token = await AsyncStorage.getItem("access_token");
        const response = await fetch(`${BASE_API_URL}api/subscriptions`, {
          headers: {
            "Authorization": access_token,
            "Accept": "application/json",
          },
        });
        if (response.status == 401) {
          await AsyncStorage.removeItem("access_token");
          await AsyncStorage.removeItem("sid");
          dispatch(setSid(""));
          dispatch(setAuthState(false));
          Snackbar.show({
            text: "Сессия истекла. Войдите заново",
            duration: Snackbar.LENGTH_LONG,
          });
          return;
        }
        let responseJSON = await response.json();
        if (responseJSON.success) {
          setSubscribes(responseJSON.data);
        }
      } catch (error) {
      }
    };
    get_subscriptions();
  }, []);

  const activateSubscription = async (login, password, index, id, device = 1) => {
    try {
      const access_token = await AsyncStorage.getItem("access_token");
      const bodyForm = new FormData();
      bodyForm.append("login", login);
      bodyForm.append("password", password);
      bodyForm.append("subscription_id", id);
      bodyForm.append("device", device);
      const response = await fetch(`${BASE_API_URL}api/auth`, {
        method: "POST",
        body: bodyForm,
        headers: { "Authorization": access_token, "Content-Type": "multipart/form-data", "Accept": "application/json" },
      });
      if (response.status == 401) {
        await AsyncStorage.removeItem("access_token");
        await AsyncStorage.removeItem("sid");
        dispatch(setSid(""));
        dispatch(setAuthState(false));
        Snackbar.show({
          text: "Сессия истекла. Войдите заново",
          duration: Snackbar.LENGTH_LONG,
        });
        return;
      }
      const responseJSON = await response.json();
      if (responseJSON.success) {
        await AsyncStorage.setItem("sid", responseJSON.sid);
        dispatch(setSid(responseJSON.sid));
        Snackbar.show({
          text: "Успешно активировано!",
          backgroundColor: "#4CAF50",
          duration: Snackbar.LENGTH_LONG,
        });
        setActivatedSubscription(index);
        setTimeout(() => {
          navigation.goBack();
        }, 500);
        return;
      }
      Snackbar.show({
        text: "Что-то пошло не так",
        backgroundColor: "#F44336",
        duration: Snackbar.LENGTH_LONG,
      });
    } catch (error) {
    }
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: colors.primary,
      }}
    >
      <FlatList
        data={subscribes}
        keyExtractor={(_, index) => index.toString()}
        contentContainerStyle={{
          paddingTop: percentHeight(2),
          marginHorizontal: percentWidth(2),
        }}
        numColumns={3}
        renderItem={({ item, index }) => (
          <TouchableFocus
            style={{
              width: percentWidth(25),
              height: percentHeight(30),
              marginHorizontal: percentWidth(3.5),
              backgroundColor: colors.dark_blue,
              marginBottom: percentHeight(3),
              alignItems: "center",
              paddingTop: percentHeight(1.5),
              borderRadius: 3,
              borderWidth: 2,
              borderColor: colors.primary,
            }}
            focusedStyle={{
              borderColor: colors.dark_red,
            }}
            focusedOnStart={!index}
            // activeOpacity={0.5}
            setRef={(!index ? setFirstSubscribeRef : () => null)}
            onPress={() => {
              if (activatedSubscription != index) {
                activateSubscription(item.login, item.password, index, item.id);
              }
            }}
            nextFocusLeftSelf={!(index % 3)}
            nextFocusRightSelf={(!((index + 1) % 3) || (index == (subscribes.length - 1)))}
            nextFocusDownSelf={((subscribes.length - index) < 4)}
          >
            <View
              style={{
                width: "90%",
                height: percentHeight(27),
                backgroundColor: "#141414",
                borderRadius: 5,
              }}
            >
              <View
                style={{
                  alignItems: "flex-end",
                  width: "100%",
                  height: "20%",
                  paddingRight: percentWidth(2),
                  paddingTop: percentHeight(1),
                }}
              >
                <Image source={require("../img/logo.png")} style={{ height: "100%", width: "30%" }}
                       resizeMode="center" />
              </View>
              <Text
                style={{
                  textAlign: "center",
                  color: colors.secondary,
                  fontSize: fSize(7),
                  marginTop: percentHeight(2),
                }}
              >
                {item.expire_date.toString().split(" ")[0]}
              </Text>
              <View
                style={{
                  width: "100%",
                  alignItems: "flex-end",
                }}
              >
                <View
                  style={{
                    backgroundColor: colors.dark_red,
                    width: "90%",
                    height: percentHeight(0.2),
                    marginTop: percentHeight(0.5),
                  }}
                />
              </View>
              <Text
                style={{
                  color: colors.secondary,
                  fontSize: fSize(7),
                  textAlign: "center",
                }}
              >
                {
                  `Устройств: ${item.count_equipment}`
                }
              </Text>
              <View
                style={{
                  // width: '20%',
                  width: "100%",
                  paddingTop: percentHeight(0.5),
                  // position: 'absolute',
                  // bottom: percentHeight(0.5),
                  // right: percentWidth(0)
                }}
              >
                {/* <Icon name="cart-outline" color={colors.dark_red} size={fSize(18)} /> */}
                <Text
                  style={{
                    fontSize: fSize(7),
                    color: activatedSubscription == index ? "#4CAF50" : colors.dark_red,
                    textAlign: "center",
                  }}
                >
                  {
                    activatedSubscription == index ?
                      "Активирован"
                      :
                      "Активировать"
                  }
                </Text>
              </View>
            </View>
          </TouchableFocus>
        )}
      />
    </View>
  );
};

export default SubscribesScreen;
