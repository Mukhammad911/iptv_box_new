import React, { useEffect, useLayoutEffect, useRef, useState } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  PermissionsAndroid,
  Alert,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { request_get, request_post } from '../api/request';
import { IPTV_ADMIN_URL } from '../api/url';
import { colors } from '../templates/colors';
import { globalStyles } from '../templates/globalStyles';
import { fSize, percentHeight, percentWidth } from '../templates/helper';
import Icon from 'react-native-vector-icons/Ionicons';
import Video from 'react-native-video';
import RNFetchBlob from 'rn-fetch-blob';
import { getFavoritesMusic } from '../redux/actions';

const MusicPlayerFavorites = ({ navigation, route, selectedMusicData }) => {
  const { sid } = useSelector((state) => state.authReducer);
  // const { musics, selected, album } = selectedMusicData;
  const [musics, setMusics] = useState(selectedMusicData.musics)
  const [selected, setSelected] = useState(selectedMusicData.selected)
  const [album, setAlbum] = useState(selectedMusicData.album)
  const [poster, setPoster] = useState('');
  const [selectedMusicId, setSelectedMusicId] = useState(selectedMusicData.selected);
  const [selectedMusic, setSelectedMusic] = useState(null);
  const [url, setUrl] = useState('');
  const [play, setPlay] = useState(true);
  const [currentTime, setCurrentTime] = useState(0.0);
  const [duration, setDuration] = useState(0);
  const [playerWidth, setPlayerWidth] = useState(0);
  const playerRef = useRef(null);
  const [muted, setMuted] = useState(false);
  const [repeat, setRepeat] = useState(false);
  const [favorite, setFavorite] = useState(true);
  const dispatch = useDispatch();
  const getCurrentTimePercentage = () => {
    if (currentTime > 0) {
      return parseFloat(currentTime) / parseFloat(duration);
    }
    return 0;
  };

  useEffect(() => {
    setMusics(selectedMusicData.musics)
    setSelected(selectedMusicData.selected)
    setSelectedMusicId(selectedMusicData.selected)
    setAlbum(selectedMusicData.album)
  }, [selectedMusicData])

  const onProgress = ({ currentTime }) => setCurrentTime(currentTime);
  useLayoutEffect(() => {
    navigation.setOptions({
      title: 'Плеер',
    });
  }, []);
  useEffect(() => {
    request_get(`music_vodfile_url/${selectedMusicId}/${sid}`).then(
      ({ data, success }) => {
        if (success) {
          const music = musics.find(
            (music) => +music.music.id === +selectedMusicId,
          );
          setPoster(music.poster);
          setSelectedMusic(music.music);
          setUrl(data);
          setPlay(true);
        }
      },
    );
  }, [selectedMusicId]);

  const changeNameMusic = (name) => {
    if (!name){
      return
    }
    let arr_name = name.split('');
    if (arr_name.length < 30) {
      return name;
    }
    let cutname = arr_name.splice(0, 30).join('');

    return cutname + ' ...';
  };

  const iconSize = 8

  const twodigit = (digit) => (+digit > 9 ? `${digit}` : `0${digit}`);

  const getAudioTimeFormat = (duration) => {
    if (duration < 1) {
      return '00:00:00';
    }

    let timeFormat = '';

    const durationHours = Math.floor(duration / 3600);

    if (durationHours > 0) {
      timeFormat = twodigit(durationHours);
    }

    const durationMinutes = Math.floor((duration % 3600) / 60);

    if (durationMinutes > 0) {
      timeFormat =
        timeFormat.length > 0
          ? `${timeFormat}:${twodigit(durationMinutes)}`
          : `00:${twodigit(durationMinutes)}`;
    }

    const durationSeconds = Math.floor(
      duration - durationHours * 3600 - durationMinutes * 60,
    );

    if (durationSeconds > 0) {
      timeFormat =
        timeFormat.length > 0
          ? `${timeFormat}:${twodigit(durationSeconds)}`
          : `00:00:${twodigit(durationSeconds)}`;
    } else {
      timeFormat += ':00';
    }

    return timeFormat;
  };

  const getGenres = () => {
    return album.music_data_genres.map((genr) => genr.name).join(',');
  };

  const prevBtn = () => {
    let idx = musics.findIndex((music) => +music.music.id === +selectedMusicId);

    if (idx === 0) {
      setSelectedMusicId(musics[musics.length - 1].music.id);
      setPoster(musics[musics.length - 1].music.poster);
      setSelectedMusic(musics[musics.length - 1].music);
      playerRef.current.seek(0);
    } else if (idx > 0) {
      const music = musics[--idx];
      setSelectedMusicId(music.music.id);
      setPoster(music.poster);
      setSelectedMusic(music.music);
    }
  };

  const nextBtn = () => {
    let idx = musics.findIndex((music) => +music.music.id === selectedMusicId);
    if (idx === musics.length - 1) {
      setSelectedMusicId(musics[0].music.id);
      setSelectedMusic(musics[0].music);
      setPoster(musics[0].music.poster);
      // playerRef.current.seek(0);
    } else if (idx < musics.length - 1) {
      const music = musics[++idx];
      setSelectedMusicId(music.music.id);
      setSelectedMusic(music.music);
      setPoster(music.music.poster);
    }
  };

  const downloadMusic = async (id) => {
    const { data } = await request_get(`music_vodfile_url/${id}/${sid}`);
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      {
        title: 'Storage Permission Required',
        message: 'App needs access to your storage to download Photos',
      },
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      // Once user grant the permission start downloading
      const date = new Date();
      const { config, fs } = RNFetchBlob;
      let PictureDir = fs.dirs.MusicDir;
      let options = {
        fileCache: true,
        addAndroidDownloads: {
          // Related to the Android only
          useDownloadManager: true,
          notification: true,
          path:
            PictureDir +
            '/music_' +
            Math.floor(date.getTime() + date.getSeconds() / 2) +
            '.mp3',
          description: 'Music',
        },
      };
      config(options)
        .fetch('GET', data)
        .then((res) => {
          // Showing alert after successful downloading
          Alert.alert('Успех', 'Музыка успешно загружена');
        });
    }
  };

  const addToFavorite = () => {
    request_post(
      `musics_favorite/${sid}?music_vodfile_id=${selectedMusicId}`,
    ).then((res) => {
      if (res.success) {
        setFavorite(false);
        dispatch(getFavoritesMusic(sid));
        Alert.alert('Успех', 'Успешно удален из избранного');
      }
    });
  };

  return (
    <View
      style={{
        // width: '100%',
        flex: 1,
        height: percentHeight(20),
        backgroundColor: colors.dark_blue,
        // flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: percentWidth(3)
      }}>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          marginBottom: percentHeight(2)
        }}
      >
        <View
          style={{
            width: percentWidth(33),
          }}
        >
          <Text
            style={{
              color: colors.secondary,
              fontSize: fSize(7),
            }}>
            {changeNameMusic(selectedMusic?.file_torrent.split('-')[1].trim())}
          </Text>
          <Text
            style={{
              color: colors.dark_text,
              fontSize: fSize(5)
            }}
          >
            {selectedMusic?.file_torrent.split('-')[0].trim()}
          </Text>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            width: percentWidth(33),
          }}>
          <TouchableOpacity onPress={prevBtn}>
            <Icon
              name={'play-back'}
              size={fSize(iconSize)}
              color={colors.secondary}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => playerRef.current.seek(currentTime - 10)} style={{ marginLeft: percentWidth(3) }}>
            <Icon
              name={'play-skip-back'}
              size={fSize(iconSize)}
              color={colors.secondary}
            />
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => setPlay(!play)}
            style={{
              marginHorizontal: percentWidth(3)
            }}
          >
            {!play ? (
              <Icon name={'play'} size={fSize(iconSize)} color={colors.secondary} />
            ) : (
                <Icon
                  name={'pause-outline'}
                  color={colors.secondary}
                  size={fSize(iconSize)}
                />
              )}
          </TouchableOpacity>
          <TouchableOpacity onPress={() => playerRef.current.seek(+currentTime + +10)} style={{ marginRight: percentWidth(3) }}>
            <Icon
              name={'play-skip-forward'}
              size={fSize(iconSize)}
              color={colors.secondary}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={nextBtn}>
            <Icon
              name={'play-forward'}
              size={fSize(iconSize)}
              color={colors.secondary}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            justifyContent: 'flex-end',
            alignItems: 'center',
            width: percentWidth(33),
          }}
        >
          <TouchableOpacity
            onPress={() => {
              setRepeat(!repeat);
            }}>
            <Icon
              name={'repeat-outline'}
              color={repeat ? colors.dark_red : colors.secondary}
              size={fSize(10)}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={addToFavorite} style={{ marginLeft: percentWidth(3) }} >
            <Icon
              name={'star-outline'}
              size={fSize(iconSize)}
              color={colors.secondary}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => downloadMusic(selectedMusicId)} style={{ marginLeft: percentWidth(3) }} >
            <Icon
              name={'download-outline'}
              size={fSize(iconSize)}
              color={colors.secondary}
            />
          </TouchableOpacity>
        </View>
      </View>
      <View
        style={{
          position: 'relative',
          width: '100%',
          height: percentHeight(5),
          // backgroundColor: 'pink'
        }}>
        <View
          style={{
            position: 'absolute',
            // left: 20,
            // right: 20,
            width: '100%',
            // backgroundColor: 'yellow'
          }}>
          <TouchableOpacity
            onLayout={({ nativeEvent }) => {
              const { layout } = nativeEvent;
              setPlayerWidth(layout.width);
            }}
            onPress={({ nativeEvent }) => {
              const { pageX, locationX } = nativeEvent;
              const seek = parseFloat(pageX / playerWidth) * duration - 10;
              playerRef.current.seek(seek);
              setPlay(true);
            }}>
            <View
              style={{
                flex: 1,
                flexDirection: 'row',
                borderRadius: 500,
                alignItems: 'center',
                overflow: 'hidden',
                // backgroundColor: colors.dark_text,
              }}>
              <View
                style={{
                  flex: getCurrentTimePercentage() * 100,
                  backgroundColor: colors.dark_red,
                  height: percentHeight(1),
                }}
              />
              <View
                style={{
                  borderRadius: 15,
                  borderWidth: 1,
                  borderColor: colors.secondary,
                  backgroundColor: colors.dark_red,
                  width: percentHeight(3),
                  height: percentHeight(3),
                  zIndex: 999,
                }}></View>
              <View
                style={{
                  flex: (1 - getCurrentTimePercentage()) * 100,
                  height: percentHeight(1),
                  backgroundColor: colors.dark_text,
                }}></View>
            </View>
          </TouchableOpacity>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
              width: '100%',
            }}>
            <Text
              style={{
                color: colors.secondary,
                fontSize: fSize(5)
              }}>
              {getAudioTimeFormat(currentTime)}
            </Text>
            <Text
              style={{
                color: colors.secondary,
                fontSize: fSize(5)
              }}>
              {getAudioTimeFormat(duration)}
            </Text>
          </View>
        </View>
      </View>

      {/* <View
        style={{
          flex: 1,
          flexDirection: 'row',
          justifyContent: 'space-between',
          width: percentWidth(90),
        }}>
        <TouchableOpacity
          onPress={() => {
            setMuted(!muted);
          }}>
          {!muted ? (
            <Icon
              name={'volume-high-outline'}
              color={colors.secondary}
              size={fSize(10)}
            />
          ) : (
              <Icon
                name={'volume-mute-outline'}
                color={colors.secondary}
                size={fSize(10)}
              />
            )}
        </TouchableOpacity>

      </View> */}

      {url ? (
        <Video
          muted={muted}
          ref={playerRef}
          onLoad={({ duration }) => {
            setDuration(duration);
          }}
          onProgress={onProgress}
          source={{ uri: url }}
          audioOnly={true}
          paused={!play}
          repeat={repeat}
        />
      ) : null}
    </View>
  );
};

export default MusicPlayerFavorites;
