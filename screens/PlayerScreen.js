import React, { useEffect, useRef, useState } from 'react';
import { View, StyleSheet, DeviceEventEmitter, TVEventHandler, Text, Image } from 'react-native';

import { request_get } from '../api/request';
import AsyncStorage from '@react-native-community/async-storage';

import { colors } from '../templates/colors';

import TvPlayer from '../components/TvPlayer3';
import { useDispatch, useSelector } from "react-redux";
import EpgTabs from '../navigations/Epg';
import SimilarChannels from '../components/Tv/SimilarChannels';
import { fSize, percentHeight, percentWidth } from '../templates/helper';
import Snackbar from 'react-native-snackbar';
import TouchableFocus from '../components/TouchableFocus';
import { IPTV_ADMIN_URL } from '../api/url';
import { FlatList } from 'react-native';
import Loader from '../components/Loader';
import Icon from 'react-native-vector-icons/Ionicons';
import { chechToken } from "../components/helper";
import { setAuthState, setSid } from "../redux/actions";
import sidValidation from "../services/sidChecker";

const groupChannelsArray = [];

// Токенизация проведена

const PlayerScreen = ({ navigation, route }) => {
  console.log("route");
  console.log(route);
  const sid = useSelector(state => state.authReducer.sid);
  const [uri, setUri] = useState('');
  const [id, setId] = useState(route.params.id);
  const [showSimilarChannels, setShowSimilarChannels] = useState(false)
  const [showEpgTabs, setShowEpgTabs] = useState(false)
  const [showControls, setShowControls] = useState(false);
  const [showSwitchChannel, setShowSwitchChannel] = useState(false);

  const [firstChannelRef, setFirstChannelRef] = useState(null);

  let channelIDChanged = useRef(null);

  const [channelsu, setChannels] = useState([]);
  const [groupChannels, setGroupChannels] = useState(route.params.groupChannelsOut);
  const [readyToShowChannels, setReadyToShowChannels] = useState(0);
  const [channelsType, setChannelsType] = useState(null);
  const [openChannelsTypes, setOpenChannelsTypes] = useState(false);
  const [openChannelsTypesRef, setOpenChannelsTypesRef] = useState(null);
  const [channelsCategoryRef, setChannelsCategoryRef] = useState(null);

  let flatRef = useRef(null);

  let switchChannelTimer = useRef(null);

  const channelsArray = route.params.channelsArray;
  let [selectedChannelIndex, setSelectedChannelIndex] = useState(route.params.selectedChannelIndex);

  const dispatch = useDispatch();

  //Here we will check the access token
  useEffect(() => {
    const ff = async () => {
      let result = await chechToken()
      if (result) {
        dispatch(setAuthState(true))
      } else {
        dispatch(setAuthState(false))
      }
    }
    ff()
  }, []);

  //Here we will check the sid
  useEffect(() => {
    AsyncStorage.getItem('sid')
      .then((sid) => sidValidation(sid))
      .then(async ({ success, sid }) => {
        if (success) {
          dispatch(setSid(sid));
        } else {
          await AsyncStorage.removeItem('sid');
          navigation.navigate('subscribes');
        }
      })

  }, []);

  // ------------------------------------------------------------------

  // const isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
  //   const paddingToBottom = 20;
  //   return (
  //     +layoutMeasurement.height + +contentOffset.y >=
  //     contentSize.height - paddingToBottom
  //   );
  // };

  useEffect(() => {
    (async () => {
      console.log("[PlayerScreen][useEffect][220]")

      let channelGroupId = await AsyncStorage.getItem('channel_group_in_memory');
      // console.log('channel_group_in_memory', channelGroupId, channelGroupId || 0);
      setChannelsType(groupChannels[channelGroupId || 0]);
    })()
  }, [])

  useEffect(() => {
    console.log("[PlayerScreen][useEffect][channelsType]")
    if (channelsType) {
      (async () => {
        setReadyToShowChannels(0);
        clearTimeout(switchChannelTimer.current);
        try {
          let page = 1;
          let data = 1;
          let tmp = [];
          while (data) {
            // console.log(`getChannels/${channelsType.id}?sid=${sid}&page=${page}`);
            console.log(`getChannels/${channelsType.id}?sid=${sid}&page=${page}`);
            const response = await request_get(
              `getChannels/${channelsType.id}?sid=${sid}&page=${page}`,
            );
            console.log(response);
            page++;
            data = response.data.length;
            // tmp = [...tmp, ...data];
            tmp.push(...response.data);
            // console.log(response.data.length);
            // console.log(tmp.length);
          };
          setReadyToShowChannels(1);
          // console.log('tmp length === ' + tmp.length);
          setChannels(tmp);
        } catch (error) {
          // console.log('errooooooooooor =========', error);
          setReadyToShowChannels[2];
          setChannels([]);
        }
        switchChannelTimer.current = setTimeout(() => {
          setShowSwitchChannel(false);
        }, 3000)
      })()
    }
  }, [channelsType])

  useEffect(() => {
    console.log("[PlayerScreen][useEffect][selectedChannelIndex]")
    setId(channelsArray[selectedChannelIndex].id);
  }, [selectedChannelIndex])

  const _tvEventHandler = new TVEventHandler();

  let controlsTimer = null;

  useEffect(() => {
    console.log("[PlayerScreen][useEffect][showControls]")
    if (showControls) {
      controlsTimer = setTimeout(() => {
        setShowControls(false);
      }, 5000);
    }
  }, [showControls]);

  const _enableTVEventHandler = () => {
    _tvEventHandler.enable(this, function (cmp, evt) {
      if (showSwitchChannel && evt.eventKeyAction == 1) {
        clearTimeout(switchChannelTimer.current);
        switchChannelTimer.current = setTimeout(() => {
          setShowSwitchChannel(false);
        }, 3000)
      }
      if (showControls) {
        clearTimeout(controlsTimer);
        controlsTimer = setTimeout(() => {
          setShowControls(false);
        }, 5000);
      }
      if (!showControls && !showEpgTabs && !showSimilarChannels && !showSwitchChannel) {
        if (evt.eventKeyAction == 1) {
          if (evt && evt.eventType === 'right') {
            setShowControls(true);
          } else if (evt && evt.eventType === 'up') {
            // setShowSwitchChannel(true);
          } else if (evt && evt.eventType === 'left') {
            setShowControls(true);
          } else if (evt && evt.eventType === 'down') {
            // setShowSwitchChannel(true);
          }
        }
      }
    });
  };

  const _disableTVEventHandler = () => {
    if (_tvEventHandler) {
      _tvEventHandler.disable();
    }
  }

  useEffect(() => {
    _enableTVEventHandler();
    return () => _disableTVEventHandler();
  });

  const [showControlsActivatorRef, setShowControlsActivatorRef] = useState(null)

  useEffect(() => {
    if (!showControls && !showEpgTabs && !showSimilarChannels && !showSwitchChannel && showControlsActivatorRef) {
      clearTimeout(controlsTimer);
      clearTimeout(switchChannelTimer.current);
      showControlsActivatorRef.setNativeProps({
        hasTVPreferredFocus: true,
      })
    }
  }, [showControls, showEpgTabs, showSimilarChannels, showSwitchChannel]);

  useEffect(() => {
    navigation.setOptions({
      headerShown: false,
    });
    DeviceEventEmitter.addListener('goBack', (e) => {
      getUri();
    });

    async function getUri() {
      console.log(`get_url/${id}/${sid}`);
      const response = await request_get(`get_url/${id}/${sid}`);
      console.log("response get_url", response);


    //  console.log("prev prew", response.data);
     // let replace_https_to_http = response.data.replace('https://','http://');
    //  console.log("new route", replace_https_to_http);
      setUri(response.data);
    }
    clearTimeout(channelIDChanged.current);
     channelIDChanged.current = setTimeout(() => {
      if (sid) {
        getUri();
      }
    }, 2000);
    getUri()

  }, [id, sid]);

  return (
    <View style={styles.container}>
      {
        showSwitchChannel ?
          (
            <View
              style={{
                height: percentHeight(80),
                backgroundColor: colors.primary,
                position: 'absolute',
                left: percentWidth(2),
                bottom: percentHeight(3),
                zIndex: 1000,
                flexDirection: 'row',
              }}
            >
              {
                readyToShowChannels <= 1 ?
                  <View
                    style={{
                      width: percentWidth(20),
                    }}
                  >
                    <View>
                      <Text
                        style={{
                          color: colors.secondary,
                          fontSize: fSize(5.5),
                          textAlign: 'center',
                          paddingVertical: percentHeight(1)
                        }}
                      >
                        Переключение канала
                      </Text>
                      {
                        readyToShowChannels == 0 ?
                          <View
                            style={{
                              width: percentWidth(20),
                              height: percentHeight(70),
                              justifyContent: 'center',
                              alignItems: 'center',
                              // flex: 1
                            }}
                          >
                            <Loader />
                          </View>
                          :
                          <FlatList
                            ref={e => flatRef.current = e}
                            data={channelsu}
                            keyExtractor={(_, index) => index.toString()}
                            // showsVerticalScrollIndicator
                            // initialScrollIndex={channelsu.findIndex((elem) => elem?.id == id)}
                            // initialNumToRender={30}
                            // getItemLayout={(data, index) => {
                            //   return {
                            //     // length: (percentWidth(1) + percentHeight(3)),
                            //     // offset: (percentWidth(1) + percentHeight(3)) * index,
                            //     length: percentHeight(5),
                            //     offset: (percentHeight(5.5) * index),
                            //     index: Number.parseFloat(index)
                            //   }
                            // }}
                            // onScrollToIndexFailed={info => {
                            //   alert('got to error promise')
                            //   const wait = new Promise(resolve => setTimeout(resolve, 500));
                            //   wait.then(() => {
                            //     flatRef.current?.scrollToIndex({ index: info.index, animated: true });
                            //   });
                            // }}
                            removeClippedSubviews={true}
                            style={{
                              height: percentHeight(72),
                            }}
                            renderItem={({ item: elem, index }) => (
                              (
                                <TouchableFocus
                                  style={{
                                    paddingLeft: percentWidth(1),
                                    paddingVertical: percentHeight(0.5),
                                    marginBottom: percentHeight(1),
                                    flexDirection: 'row'
                                  }}
                                  setRef={(!index ? setFirstChannelRef : (() => null))}
                                  key={index}
                                  focusedStyle={{
                                    backgroundColor: colors.dark_red
                                  }}
                                  onPress={() => {
                                    setId(elem.id);
                                    Snackbar.show({
                                      text: elem.name,
                                      duration: Snackbar.LENGTH_LONG,
                                      backgroundColor: '#000000',
                                      textColor: '#ffffff',
                                    });
                                  }}
                                  focusedOnStart={(elem?.id == id)}
                                  nextFocusRight={(!openChannelsTypes ? openChannelsTypesRef : channelsCategoryRef)}
                                  nextFocusLeftSelf
                                  // nextFocusRightSelf
                                  nextFocusDownSelf={((1 + +index) == channelsu.length)}
                                >
                                  <Image
                                    source={{
                                      uri: `${IPTV_ADMIN_URL}images/channel/${elem.image}`
                                    }}
                                    style={{
                                      height: percentHeight(3),
                                      width: percentHeight(3),
                                      marginRight: percentWidth(0.5)
                                    }}
                                  />
                                  <Text
                                    style={{
                                      color: colors.secondary,
                                      fontSize: fSize(5),
                                    }}
                                  >
                                    {
                                      elem.id + ". " + elem.name
                                    }
                                  </Text>
                                </TouchableFocus>
                              )
                            )}
                          />
                      }
                    </View>
                  </View>
                  :
                  <Text
                    style={{
                      paddingTop: percentHeight(10),
                      color: colors.secondary,
                    }}
                  >
                    Не удалось загрузить данные
                  </Text>
              }

              <View
                style={{
                  borderLeftWidth: 1,
                  borderColor: colors.dark_blue
                }}
              />
              {
                openChannelsTypes ? (
                  <View>
                    <View
                      style={{
                        paddingVertical: percentHeight(1),
                        width: percentWidth(15),
                      }}
                    >
                      <Text
                        style={{
                          color: colors.secondary,
                          fontSize: fSize(5.5),
                          textAlign: 'center',
                        }}
                      >
                        Категория каналов
                      </Text>
                    </View>
                    <FlatList
                      data={groupChannels}
                      keyExtractor={(_, index) => index.toString()}
                      renderItem={({ item, index }) => (
                        <TouchableFocus
                          style={{
                            paddingLeft: percentWidth(1),
                            paddingVertical: percentHeight(0.5),
                            marginBottom: percentHeight(1),
                            flexDirection: 'row',
                            backgroundColor: channelsType?.id == item.id ? colors.dark_blue : colors.primary,
                          }}
                          setRef={(!index ? setChannelsCategoryRef : () => null)}
                          focusedStyle={{
                            backgroundColor: colors.dark_red,
                          }}
                          onPress={() => {
                            setChannelsType(item);
                            (async () => {
                              await AsyncStorage.setItem('channel_group_in_memory', index.toString());
                            })()
                          }}
                          nextFocusUpSelf={!index}
                          nextFocusRight={openChannelsTypesRef}
                          nextFocusDownSelf={index == 13}
                          nextFocusLeft={(channelsu.length < 14 ? firstChannelRef : null)}
                        >
                          <Text
                            style={{
                              color: colors.secondary,
                              fontSize: fSize(5),
                            }}
                          >
                            {
                              item.name
                            }
                          </Text>
                        </TouchableFocus>
                      )}
                    />
                  </View>
                ) : null
              }
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'center',
                  alignItems: 'center',
                  padding: percentWidth(0.5)
                }}
              >
                <TouchableFocus
                  style={{
                    flexDirection: 'row',
                    justifyContent: 'center',
                    alignItems: 'center',
                    width: percentWidth(3),
                    height: percentWidth(3),
                    borderRadius: 1000,
                  }}
                  focusedStyle={{
                    backgroundColor: colors.dark_red,
                  }}
                  setRef={setOpenChannelsTypesRef}
                  onPress={() => {
                    setOpenChannelsTypes(!openChannelsTypes);
                  }}
                  nextFocusUpSelf
                  nextFocusRightSelf
                  nextFocusDownSelf
                  focusedOnStart
                >
                  {
                    openChannelsTypes ? (
                      <>
                        <Icon
                          name="chevron-back"
                          color="white"
                          size={percentWidth(1.5)}
                          style={{
                            position: 'relative',
                            left: percentWidth(0.5)
                          }}
                        />
                        <Icon
                          name="chevron-back"
                          color="white"
                          size={percentWidth(1.5)}
                          style={{
                            position: 'relative',
                            right: percentWidth(0.5)
                          }}
                        />
                      </>
                    ) : (
                      <>
                        <Icon
                          name="chevron-forward"
                          color="white"
                          size={percentWidth(1.5)}
                          style={{
                            position: 'relative',
                            left: percentWidth(0.5)
                          }}
                        />
                        <Icon
                          name="chevron-forward"
                          color="white"
                          size={percentWidth(1.5)}
                          style={{
                            position: 'relative',
                            right: percentWidth(0.5)
                          }}
                        />
                      </>
                    )
                  }
                </TouchableFocus>
              </View>
            </View>
          )
          :
          null
      }
      <View style={styles.videoFull}>
        <TvPlayer
          uri={uri}
          navigation={navigation}
          autoShowHeader={false}
          back={navigation.goBack}
          autoPlay={true}
          id={id}
          setShowSimilarChannels={setShowSimilarChannels}
          setShowEpgTabs={setShowEpgTabs}
          showControlsActivatorRef={showControlsActivatorRef}
          setShowControlsActivatorRef={setShowControlsActivatorRef}
          showControls={showControls}
          setShowControls={setShowControls}
          showSwitchChannel={showSwitchChannel}
          setShowSwitchChannel={setShowSwitchChannel}
          setSwitchChannelTimer={(timer) => switchChannelTimer.current = timer}
        />
      </View>
      {
        showSimilarChannels ?
          <SimilarChannels
            group_channel_id={route.params.group_channel_id}
            setId={setId}
            id={id}
            setShowSimilarChannels={setShowSimilarChannels}
            showControlsActivatorRef={showControlsActivatorRef}
          />
          :
          null
      }
      {
        showEpgTabs ?
          <EpgTabs channel_id={id} setShowEpgTabs={setShowEpgTabs} setUri={setUri} showControlsActivatorRef={showControlsActivatorRef} />
          :
          null
      }
    </View>
  );
};

export default PlayerScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000',
  },
  videoFull: {
    flex: 1,
    backgroundColor: '#000',
  },
  video: {
    height: percentHeight(35),
  },
});
