package com.somontv;

import com.facebook.react.ReactActivity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle; // here
import com.facebook.react.ReactActivity;
import org.devio.rn.splashscreen.SplashScreen;// here
import android.view.KeyEvent;
// import com.github.kevinejohn.keyevent.KeyEventModule;

public class MainActivity extends ReactActivity {

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "somontv";
  }

  // @Override  // <--- Add this method if you want to react to keyDown
  //     public boolean onKeyDown(int keyCode, KeyEvent event) {

  //       // A. Prevent multiple events on long button press
  //       //    In the default behavior multiple events are fired if a button
  //       //    is pressed for a while. You can prevent this behavior if you
  //       //    forward only the first event:
  //       //        if (event.getRepeatCount() == 0) {
  //       //            KeyEventModule.getInstance().onKeyDownEvent(keyCode, event);
  //       //        }
  //       //
  //       // B. If multiple Events shall be fired when the button is pressed
  //       //    for a while use this code:
  //       //        KeyEventModule.getInstance().onKeyDownEvent(keyCode, event);
  //       //
  //       // Using B.
  //       KeyEventModule.getInstance().onKeyDownEvent(keyCode, event);

  //       // There are 2 ways this can be done:
  //       //  1.  Override the default keyboard event behavior
  //       //    super.onKeyDown(keyCode, event);
  //       //    return true;

  //       //  2.  Keep default keyboard event behavior
  //       //    return super.onKeyDown(keyCode, event);

  //       // Using method #1 without blocking multiple
  //       super.onKeyDown(keyCode, event);
  //       return true;
  //     }

     @Override
     public void onConfigurationChanged(Configuration newConfig) {
         super.onConfigurationChanged(newConfig);
         Intent intent = new Intent("onConfigurationChanged");
         intent.putExtra("newConfig", newConfig);
         this.sendBroadcast(intent);
     }
       @Override
         protected void onCreate(Bundle savedInstanceState) {
             SplashScreen.show(this);  // here
             super.onCreate(savedInstanceState);
         }
}
