import React, { useEffect, useState } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import { request_get } from '../../api/request';
import {
  Image,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  StyleSheet,
  Dimensions,
  FlatList
} from 'react-native';
import { colors } from '../../templates/colors';
import Icon from 'react-native-vector-icons/Ionicons';
import { fSize, percentHeight, percentWidth } from '../../templates/helper';
import { IPTV_ADMIN_URL } from '../../api/url';
import Loader from '../Loader';
import FilmCard from '../films/FilmCard';
import FocusableOpacity from '../FocusableOpacity';
import ScreenHeader from '../ScreenHeader';
import TouchableFocus from '../TouchableFocus';

const Favorite = ({
  route,
  navigation,
  loader,
  setLoader,
  setSearchCheck,
  searchCheck,
  searchText,
  setSearchText,
  searchShow,
  setSearchShow,
  data,
  firstFocusedButtonRef = null,
  setFirstFocusedButtonRef = () => null,
  navigationButtonRef = null,
}) => {

  const [channelRefs, setChannelRefs] = useState([])
  const [filmRefs, setFilmRefs] = useState([])

  useEffect(() => {
    // setFilmRefs([])
    if (data.films.length == 0) {
      setFilmRefs([])
    }
  }, [data.films])

  useEffect(() => {
    if (data.channels.length == 0) {
      setChannelRefs([])
    }
    // setFilmRefs([])
  }, [data.channels])

  const findRefFromArray = (arr = [], id) => {
    for (let i of arr) {
      if (i.id == id) {
        return i.ref
      }
    }
    return null
  }

  const changeNameFilm = (name = '') => {

    let arr_name = name.split("");
    if (arr_name.length < 14) {
      return name;
    }
    let cutname = arr_name.splice(0, 14).join('');

    return cutname + ' ...';

  }
  const changeNameChannel = (name = '') => {
    let arr_name = name.split('');
    if (arr_name.length < 10) {
      return name;
    }
    let cutname = arr_name.splice(0, 9).join('');

    return cutname + ' ...';
  };

  const channel = (item, index, length) => {
    return (
      <TouchableFocus
        // key={item.id}
        style={{
          flexDirection: 'column',
          height: percentHeight(30),
          marginTop: 20,
          marginLeft: percentWidth(1),
          marginRight: percentWidth(1),
          borderWidth: 2,
          borderColor: colors.primary,
          borderTopStartRadius: 8,
          borderTopEndRadius: 8
        }}
        focusedOnStart={!index}
        focusedStyle={{
          // transform: [{ scale: 1.2 }]
          borderColor: colors.dark_red
        }}
        onPress={() =>
          navigation.navigate('player', {
            id: item.id,
            group_channel_id: item.group_channel_id,
          })
        }
        setRef={(reff) => {
          channelRefs.push({ id: index, ref: reff })
          if (!index && data.channels.length) {
            setFirstFocusedButtonRef(reff)
          }
        }}
        nextFocusUp={(index < 5 ? navigationButtonRef : null)}
        nextFocusLeftSelf={!(index % 5)}
        nextFocusRightSelf={(!((index + 1) % 5) || ((index + 1) == length))}
        nextFocusDownSelf={(((length - index) < 6) && !data.films.length)}
        nextFocusLeft={findRefFromArray(channelRefs, index - 1)}
        nextFocusRight={findRefFromArray(channelRefs, index + 1)}
      >
        <View style={{ flexDirection: 'column', height: percentHeight(20) }}>
          <View
            style={{
              width: percentWidth(15),
              height: percentHeight(18),
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: '#2C2A3C',
              borderRadius: 6,
              overflow: 'hidden'
            }}>
            <Image
              style={{ height: '100%', width: '100%' }}
              source={{ uri: `${IPTV_ADMIN_URL}images/channel/${item.image}` }}
              resizeMode={'center'}
            />
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'flex-start',
              width: percentWidth(10),
              marginLeft: 10,
            }}>
            <View>
              {/* <Text style={{color: colors.secondary, fontSize: fSize(6)}}>
                {changeNameChannel(item.name)}
              </Text> */}
              <Text
                style={{
                  color: colors.secondary,
                  fontSize: fSize(6),
                  textAlign: 'left'
                }}>
                {changeNameChannel(item.name)}
              </Text>
              <Text
                style={{
                  color: colors.dark_text,
                  fontSize: fSize(5),
                  textAlign: 'left'
                }}>
                {item.program_time}
              </Text>
            </View>
          </View>
        </View>
      </TouchableFocus>
    );
  };

  // const film = (item) => {
  //   const changeNameFilm = (name) => {
  //     let arr_name = name.split('');
  //     if (arr_name.length < 20) {
  //       return name;
  //     }
  //     let cutname = arr_name.splice(0, 20).join('');
  //     return cutname + ' ...';
  //   };
  //   return (
  //     // <TouchableOpacity
  //     //   style={styles.itemContainer}
  //     //   key={item.id}
  //     //   onPress={() =>
  //     //     navigation.navigate('vplayer', {id: item.id, format: 'standart'})
  //     //   }>
  //     //   <View style={{justifyContent: 'center', alignItems: 'center'}}>
  //     //     <Image
  //     //       source={{uri: IPTV_ADMIN_URL + 'posters/' + item.poster}}
  //     //       style={{
  //     //         height: percentHeight(13),
  //     //         width: percentWidth(16),
  //     //         borderRadius: 5,
  //     //       }}
  //     //     />
  //     //   </View>
  //     //   <View style={{marginLeft: 20, justifyContent: 'center'}}>
  //     //     <Text style={{color: colors.secondary, fontSize: fSize(20)}}>
  //     //       {changeNameFilm(item.name)}
  //     //     </Text>
  //     //     <Text style={{color: '#312D44', fontSize: fSize(15)}}>
  //     //       {item.genres.length > 0 ? item.genres[0].name : ''}
  //     //     </Text>
  //     //     <Text style={{color: '#312D44', fontSize: fSize(15)}}>
  //     //       {item.year}
  //     //     </Text>
  //     //   </View>
  //     // </TouchableOpacity>

  //     // <FlatList
  //     //   numColumns={5}
  //     //   data={films}
  //     //   renderItem={({item}) => (
  //     //     <FilmCard item={item} navigation={navigation} />
  //     //   )}
  //     //   keyExtractor={(item) => item.id.toString()}
  //     //   style={{marginTop: percentHeight(3)}}
  //     // />
  //   );
  // };
  // const channels = () => {
  //   const { channels } = data;
  //   const item = [];
  //   for (const key in channels) {
  //     item.push(channel(channels[key].channel, key));
  //   }
  //   return item;
  // };

  const channels = () => {
    const { channels } = data;
    const item = [];
    return (
      <FlatList
        numColumns={5}
        data={channels}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({ item, index }) => channel(item.channel, index, channels.length)}
        contentContainerStyle={{
          // justifyContent: 'center',
          paddingHorizontal: percentWidth(2)
        }}
      />
    )
  };

  const films = () => {
    const { films } = data;
    return (
      <FlatList
        numColumns={5}
        data={films}
        renderItem={({ item, index }) => (
          // <FilmCard focusedStyle={{ transform: [{ scale: 1.2 }] }} style={{ marginBottom: percentHeight(3) }} item={item.voddata} navigation={navigation} />
          <TouchableFocus
            focusedOnStart={false}
            style={{
              // flex: 1,
              marginBottom: percentHeight(3),
              backgroundColor: colors.primary,
              borderTopEndRadius: 5,
              borderTopStartRadius: 5,
              marginTop: 5,
              overflow: 'hidden',
              width: percentWidth(15),
              height: percentHeight(50),
              marginHorizontal: percentWidth(2),
              alignItems: 'center',
              justifyContent: "flex-start",
              borderWidth: 2,
              borderColor: colors.primary
            }}
            focusedStyle={{
              // transform: [{ scale: 1.2 }]
              borderColor: colors.dark_red,
            }}
            onPress={() =>
              navigation.navigate('vplayer', { id: item.voddata.id, format: item.voddata.format })
            }
            setRef={(reff) => {
              filmRefs.push({ id: index, ref: reff })
              if (!index && data.films.length && !data.channels.length) {
                setFirstFocusedButtonRef(reff)
              }
            }}
            nextFocusLeftSelf={!(index % 5)}
            nextFocusRightSelf={(!((index + 1) % 5) || ((index + 1) == films.length))}
            nextFocusDownSelf={((films.length - index) < 6)}
            nextFocusLeft={findRefFromArray(filmRefs, index - 1)}
            nextFocusRight={findRefFromArray(filmRefs, index + 1)}
            nextFocusUp={(((index < 5) && !data.channels.length) ? navigationButtonRef : null)}
          // nextFocusDown
          >
            <View>
              <Image
                source={{ uri: IPTV_ADMIN_URL + 'posters/' + item.voddata.poster }}
                style={{
                  height: percentHeight(40),
                  width: percentWidth(15),
                }}
              />
            </View>
            <View
              style={{
                marginTop: 5,
                width: percentWidth(15),
                maxHeight: percentHeight(4),
                overflow: 'hidden',
              }}>
              <Text style={{ color: '#fff', fontSize: fSize(7) }}>{changeNameFilm(item.voddata.name)}</Text>
            </View>
            <View
              style={{
                width: percentWidth(15),
                maxHeight: percentHeight(20),
                overflow: 'hidden',
              }}>
              <Text style={{ color: colors.dark_text, fontSize: fSize(5) }}>
                {item.voddata.year} - {item.voddata.genres.length > 0 ? item.voddata.genres[0].name : ''}
              </Text>
            </View>
          </TouchableFocus>
        )}
        keyExtractor={(item) => item.voddata.id.toString()}
        style={{ marginTop: percentHeight(3) }}
      />
    )
  };

  return (
    <>
      {/* <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center'
          }}
        >
          {
            !searchShow ?
              <ScreenHeader screenName="Избранные" />
              :
              searchInput()
          }
          <TouchableOpacity
            style={{ marginRight: percentWidth(2) }}
            onPress={() => {
              searchShow ? Search() : null;
              setSearchShow(!searchShow);
              setSearchCheck(true);
            }}>
            <Icon
              name={'search-outline'}
              size={20}
              color={colors.header_icons_color}
            />
          </TouchableOpacity>
        </View> */}
      {!searchShow ? (
        <>
          {loader ? <Loader /> : null}
          <ScrollView focusable={false} style={{ flex: 1, backgroundColor: colors.primary, marginTop: percentHeight(5), paddingHorizontal: percentWidth(2) }} >
            {
              data.channels.length ?
                <>
                  <Text style={{ color: colors.secondary, fontSize: fSize(7), paddingLeft: percentWidth(2) }} >
                    Телеканалы
                  </Text>
                  {channels()}
                </>
                :
                null
            }
            {
              data.films.length ?
                <>
                  <Text style={{ color: colors.secondary, fontSize: fSize(7) }} >
                    Фильмы
                  </Text>
                  {films()}
                </>
                :
                null
            }
          </ScrollView>
        </>
      ) : null}
    </>
  );
};
export default Favorite;

const styles = StyleSheet.create({
  imageContainer: {
    width: percentWidth(17),
    maxHeight: percentHeight(13),
    minHeight: percentHeight(9),
    backgroundColor: '#2C2A3C',
    justifyContent: 'center',
  },
  itemContainer: {
    flexDirection: 'row',
    marginVertical: 5,
  },
});
