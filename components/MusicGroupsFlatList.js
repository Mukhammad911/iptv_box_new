import React, { useEffect, useState } from 'react'
import { Text } from 'react-native';
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler';
import { fSize, percentHeight, percentWidth } from '../templates/helper';

const MusicGroupFlatList = ({musicGroups, selectedMusicGroup, setSelectedMusicGroup}) => {
    const [selectedColor, setSelectedColor] = useState('#AA0008')
    const [notSelectedColor, setNotSelectedColor] = useState('none')
    const [selected, setSelected] = useState(0)
    useEffect(() => {
        setNotSelectedColor('none')
    }, [selectedMusicGroup])
    return (
        <FlatList
          data={musicGroups}
          keyExtractor={(item) => item.id.toString()}
          horizontal={true}
          renderItem={({item}) => (
            <TouchableOpacity
              style={{
                height: percentHeight(5),
                borderRadius: percentWidth(5),
                marginHorizontal: 5,
                justifyContent: 'center',
                alignItems: 'center',
                borderWidth:
                selected === item.id
                    ? 0
                    : 0.5,
                backgroundColor:
                  selected === item.id
                    ? selectedColor
                    : notSelectedColor,
                borderColor: 'white'
              }}
              onPress={() => {
                setSelectedMusicGroup(item);
                setSelected(item.id)
              }}>
              <Text
                style={{
                  color: 'white',
                  fontSize: fSize(7),
                  padding: 10,
                }}>
                {item.name}
              </Text>
            </TouchableOpacity>
          )}
        />
    )
}

export default MusicGroupFlatList