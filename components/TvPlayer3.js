import {
  BackHandler,
  StyleSheet,
  TouchableOpacity,
  View,
  StatusBar,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import React, { Fragment, useCallback, useEffect, useRef, useState } from 'react';
import { bindActionCreators } from 'redux';
import * as actions from '../redux/actions';
import { header } from './helper';
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../templates/colors';
import { fSize, percentHeight, percentWidth } from '../templates/helper';
import Loader from './Loader';
import { Button } from 'react-native';
import { Text } from 'react-native';
import { VLCPlayer } from '@nghinv/react-native-vlc';
import { useFocusEffect } from '@react-navigation/native';
import Favorite from './General/Favorite';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import TouchableFocus from './TouchableFocus';
import * as Animatable from 'react-native-animatable';
import mainLogo from '../img/ic_launcher_round_new.png'
import Video from 'react-native-video'



const TvPlayer = ({
  navigation = null,
  autoShowHeader = false,
  back = null,
  uri = null,
  onFull = null,
  onPlay,
  id,
  autoPlay = true,
  isTv = true,
  vidid,
  setShowPlayer,
  setShowEpgTabs = (arg) => null,
  setShowSimilarChannels = (arg) => null,
  showControlsActivatorRef = null,
  setShowControlsActivatorRef = () => null,
  showControls = false,
  setShowControls = () => null,
  showSwitchChannel = false,
  setShowSwitchChannel = () => null,
  setSwitchChannelTimer = () => null,
}) => {
  const [rUri, setRUri] = useState(null);
  const dispatch = useDispatch();
  const [visibleStatusBar, setVisibleStatusBar] = useState(false);
  const [play, setPlay] = useState(autoPlay);
  const [loader, setLoader] = useState(false);
  const [currentTime, setCurrentTime] = useState(-1);
  const playerRef = useRef(null);
  const sid = useSelector((state) => state.authReducer.sid);
  const [showLeftSkipButton, setShowLeftSkipButton] = useState(false);
  const [showRightSkipButton, setShowRightSkipButton] = useState(false);
  const [duration, setDuration] = useState(0);
  const [playerWidth, setPlayerWidth] = useState(0);
  // const [showControls, setShowControls] = useState(false);
  const [showPlayBtn, setShowPlayBtn] = useState(false);
  let timeOutID;
  let controlTimerId;
  const getCurrentTimePercentage = () => {
    if (currentTime > 0 && duration > 0) {
      return parseFloat(currentTime) / parseFloat(duration);
    }
    return 0;
  };



  const [closePlayerButtonRef, setClosePlayerButtonRef] = useState(null)

  const [playerSkipBackButtonRef, setPlayerSkipBackButtonRef] = useState(null)
  const [playerFavoriteButtonRef, setPlayerFavoriteButtonRef] = useState(null)
  const [playerPlayBackButtonRef, setPlayerPlayBackButtonRef] = useState(null)
  const [playerPlayButtonRef, setPlayerPlayButtonRef] = useState(null)
  const [playerPlayForwardButtonRef, setPlayerPlayForwardRef] = useState(null)
  const [playerEPGMenuButtonRef, setPlayerEPGMenuButtonRef] = useState(null)
  const [playerSimilarChannelsButtonRef, setPlayerSimilarChannelsButtonRef] = useState(null)

  useEffect(() => {
    return () => {
      playerRef.current = null;
    };
  }, []);
  useFocusEffect(
    useCallback(() => {
      // BackHandler.addEventListener('hardwareBackPress', () => {
      //   dispatch(actions.setNavigationVisibility(true));
      // });
      return () => {
        // setRUri(null);
        // dispatch(actions.setNavigationVisibility(true));
        // dispatch(actions.getHistory(sid));
        // if (playerRef.current) {
        //   playerRef.current.clear();
        // }
      };
    }, []),
  );
  useEffect(() => {
    if (uri) {
      setRUri(uri);
    }
    setCurrentTime(-1);
  }, [uri]);

  // const showFull = () => {
  //   if (interval) {
  //     clearTimeout(interval);
  //     interval = setTimeout(hideFull, 3790);
  //     return;
  //   }
  //   interval = setTimeout(hideFull, 3790);
  // };

  const onProgress = ({ currentTime, duration }) => {
    setDuration(duration);
    setCurrentTime(currentTime);
  };

  const twodigit = (digit) => (+digit > 9 ? `${digit}` : `0${digit}`);

  const getVideoTimeFormat = (s) => {
    let ms = s % 1000;
    s = (s - ms) / 1000;
    let secs = s % 60;
    s = (s - secs) / 60;
    let mins = s % 60;
    let hrs = (s - mins) / 60;
    if (hrs.toString() === '0') {
      return twodigit(mins) + ':' + twodigit(secs);
    }
    return twodigit(hrs) + ':' + twodigit(mins) + ':' + twodigit(secs);
  };

  const showStatusBar = () => {
    setShowControls(true);
    // setShowPlayBtn(true);
  };

  if (!uri) {
    return <Loader />;
  }

  return (
    <View>
      {
        currentTime < 0 ?
          (
            <View
              style={{
                width: '100%',
                height: '100%',
                flex: 1,
                backgroundColor: '#12101C',
                position: 'absolute',
                zIndex: 1000,
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              <Animatable.View
                animation="pulse"
                easing="ease-out"
                style={{
                  width: 100,
                  height: 100,
                  justifyContent: "center",
                  alignItems: 'center'
                }}
                iterationCount="infinite"
              >
                <Image source={mainLogo} resizeMode="center" width={50} height={50} style={{ width: percentHeight(40), height: percentHeight(40) }} />
              </Animatable.View>
            </View>
          )
          :
          null
      }
      <StatusBar hidden={visibleStatusBar} backgroundColor={colors.primary} />
      <View style={styles.videoFull}>
        <TouchableFocus
          onPress={() => {
            setShowSwitchChannel(true);
            setSwitchChannelTimer(
              setTimeout(() => {
                setShowSwitchChannel(false);
              }, 3000)
            );
          }}
          style={{
            zIndex: -999,
          }}
          focusedOnStart
          setRef={setShowControlsActivatorRef}
          nextFocusLeftSelf
          nextFocusUpSelf
          nextFocusRightSelf
          nextFocusDownSelf
        >
          <VLCPlayer
            ref={playerRef}
            style={{
              height: '100%',
              width: '100%',
              zIndex: 500
            }}
            videoAspectRatio="16:9"
            source={{ uri: rUri }}
            resizeMode="center"
            fullscreen={true}
            volume={100}
            paused={!play}
            onProgress={onProgress}
            // onBuffer={(buffer) => console.buffer('buffer:', buffer)}
            onReadyForDisplay={() => setLoader(false)}
            onError={(err) => {
              setLoader(false);
            }}
          />
          {/* <Video
            ref={playerRef}
            style={{
              width: '100%',
              height: '100%',
              zIndex: 500,
            }}
            resizeMode="cover"
            source={{uri: uri ?? ""}}
            paused={!play}
            onProgress={({currentTime, seekableDuration}) => {
              // alert('123');
              setCurrentTime(currentTime);
              setDuration(seekableDuration);
            }}
            onReadyForDisplay={() => setLoader(false)}
          /> */}
        </TouchableFocus>
      </View>
      {showControls ? (
        <TouchableFocus
          style={{
            width: percentHeight(10),
            height: percentHeight(10),
            position: 'absolute',
            top: percentHeight(5),
            left: percentHeight(5),
            borderRadius: 100,
            justifyContent: 'center',
            alignItems: 'center'
          }}
          focusedStyle={{
            backgroundColor: colors.dark_red
          }}
          onPress={() => {
            // navigation.navigate('group_channels')
            navigation.goBack()
          }}
          setRef={setClosePlayerButtonRef}
          nextFocusLeftSelf
          nextFocusUpSelf
          nextFocusRightSelf
          nextFocusDown={playerSkipBackButtonRef}
        >
          <Icon name="arrow-back-outline" style={{ color: colors.secondary, fontSize: fSize(10) }} />
        </TouchableFocus>
      )
        :
        null
      }
      {showControls ? (
        <View style={styles.mainProgressContainer} >
          <View style={styles.visibleProgressContainer} >
            <View style={styles.trackingControls}>
              <TouchableOpacity
                onLayout={({ nativeEvent }) => {
                  const { layout } = nativeEvent;
                  setPlayerWidth(layout.width);
                }}
                onPress={({ nativeEvent }) => {
                  const { pageX } = nativeEvent;
                  // const seek = parseFloat(parseFloat(pageX / playerWidth) * duration - 20) / 1000;
                  const seek = parseFloat(parseFloat((pageX - percentWidth(10)) / percentWidth(80)) * duration - percentHeight(0.75)) / 1000;
                  // const seek = parseFloat(parseFloat((pageX - percentWidth(10)) / percentWidth(80)) * duration) / 1000;
                  playerRef.current.seek(seek);
                  // nativeEvent.changedTouches[0]
                  setPlay(true);
                }}>
                <View style={styles.progress}>
                  <View
                    style={[
                      styles.innerProgressCompleted,
                      {
                        flex: getCurrentTimePercentage() * 100,
                      },
                    ]}
                  />
                  <View
                    style={{
                      borderRadius: 15,
                      borderColor: 'white',
                      borderWidth: 1,
                      backgroundColor: colors.dark_red,
                      width: percentHeight(1.5),
                      height: percentHeight(1.5),
                      zIndex: 999,
                      position: 'relative',
                    }} />
                  <View
                    style={[
                      styles.innerProgressRemaining,
                      {
                        flex: (1 - getCurrentTimePercentage()) * 100,
                      },
                    ]}
                  />
                </View>
              </TouchableOpacity>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Text
                  style={{
                    color: colors.dark_text,
                    fontSize: fSize(4),
                    paddingLeft: percentWidth(1)
                  }}>
                  {getVideoTimeFormat(currentTime) == '00:00' ? '- - : - -' : getVideoTimeFormat(currentTime)}
                </Text>

                <Text
                  style={{
                    color: colors.dark_text,
                    fontSize: fSize(4),
                    paddingRight: percentWidth(1),
                  }}>
                  {getVideoTimeFormat(duration) == '00:00' ? '- - : - -' : getVideoTimeFormat(duration)}
                </Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingLeft: percentWidth(2),
                paddingRight: percentWidth(2),
                paddingTop: percentHeight(1),
                paddingBottom: percentHeight(1)
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center'
                }}
              >
                <TouchableFocus
                  style={styles.settingButton}
                  focusedStyle={{
                    backgroundColor: colors.dark_red
                  }}
                  onPress={() => {
                    setShowControls(false);
                    showControlsActivatorRef.setNativeProps({
                      hasTVPreferredFocus: true
                    })
                  }}
                  setRef={setPlayerSkipBackButtonRef}
                  nextFocusLeftSelf
                  nextFocusDownSelf
                  nextFocusRight={playerFavoriteButtonRef}
                  nextFocusUp={closePlayerButtonRef}
                >
                  <Icon name={'close'} color={colors.secondary} size={fSize(7)} />
                </TouchableFocus>
                <Favorite
                  Check={`channel/favorite/check/${id}`}
                  Add={`channel/favorite/${id}`}
                  Remove={`channel/favorite/${id}`}
                  iconSize={fSize(6)}
                  style={styles.settingButton}
                  focusedStyle={{
                    backgroundColor: colors.dark_red
                  }}
                  setRef={setPlayerFavoriteButtonRef}
                  nextFocusDownSelf
                  nextFocusLeft={playerSkipBackButtonRef}
                  nextFocusRight={playerPlayBackButtonRef}
                  nextFocusUp={closePlayerButtonRef}
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center'
                }}
              >
                <Text style={styles.secondsText} >
                  -10 сек
                </Text>
                <TouchableFocus
                  style={styles.settingButton}
                  focusedStyle={{
                    backgroundColor: colors.dark_red
                  }}
                  onPress={() => {
                    if (currentTime > 10000) {
                      playerRef.current.seek((currentTime - currentTime % 1000) / 1000 - 10);
                    }
                  }}
                  setRef={setPlayerPlayBackButtonRef}
                  nextFocusDownSelf
                  nextFocusLeft={playerFavoriteButtonRef}
                  nextFocusRight={playerPlayButtonRef}
                  nextFocusUp={closePlayerButtonRef}
                >
                  <Icon name={'play-back-outline'} color={colors.secondary} size={fSize(5)} />
                </TouchableFocus>
                <TouchableFocus
                  style={{ ...styles.settingButton, width: percentHeight(6), height: percentHeight(6) }}
                  focusedStyle={{
                    backgroundColor: colors.dark_red
                  }}
                  onPress={() => {
                    setPlay(!play);
                    if (isTv) {
                      playerRef.current.resume(!play);
                    }
                  }}
                  setRef={setPlayerPlayButtonRef}
                  focusedOnStart
                  nextFocusDownSelf
                  nextFocusLeft={playerPlayBackButtonRef}
                  nextFocusRight={playerPlayForwardButtonRef}
                  nextFocusUp={closePlayerButtonRef}
                >
                  {
                    !play ? (
                      <Icon name={'play'} color={colors.secondary} size={fSize(7)} />
                    ) : (
                      <Icon
                        name={'pause'}
                        color={colors.secondary}
                        size={fSize(7)}
                      />
                    )
                  }
                </TouchableFocus>
                <TouchableFocus
                  style={styles.settingButton}
                  focusedStyle={{
                    backgroundColor: colors.dark_red
                  }}
                  onPress={() => {
                    if (duration - currentTime > 10000) {
                      playerRef.current.seek((currentTime - currentTime % 1000) / 1000 + 10);
                    }
                  }}
                  setRef={setPlayerPlayForwardRef}
                  nextFocusUp={closePlayerButtonRef}
                  nextFocusDownSelf
                  nextFocusLeft={playerPlayButtonRef}
                  nextFocusRight={playerEPGMenuButtonRef}
                >
                  <Icon name={'play-forward-outline'} color={colors.secondary} size={fSize(5)} />
                </TouchableFocus>
                <Text style={styles.secondsText}>
                  +10 сек
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center'
                }}
              >
                <TouchableFocus
                  style={styles.settingButton}
                  focusedStyle={{
                    backgroundColor: colors.dark_red
                  }}
                  onPress={() => {
                    setShowEpgTabs(true)
                    setShowControls(false)
                  }}
                  setRef={setPlayerEPGMenuButtonRef}
                  nextFocusDownSelf
                  nextFocusLeft={playerPlayForwardButtonRef}
                  nextFocusUp={closePlayerButtonRef}
                  nextFocusRight={playerSimilarChannelsButtonRef}
                >
                  <Icon name={'list-outline'} color={colors.secondary} size={fSize(5)} />
                </TouchableFocus>
                <TouchableFocus
                  style={styles.settingButton}
                  focusedStyle={{
                    backgroundColor: colors.dark_red
                  }}
                  onPress={() => {
                    setShowSimilarChannels(true);
                    setShowControls(false)
                  }}
                  setRef={setPlayerSimilarChannelsButtonRef}
                  nextFocusDownSelf
                  nextFocusRightSelf
                  nextFocusLeft={playerEPGMenuButtonRef}
                  nextFocusUp={closePlayerButtonRef}
                >
                  <Icon name={'tv-outline'} color={colors.secondary} size={fSize(5)} />
                </TouchableFocus>
              </View>
            </View>
          </View>
        </View>
      ) : null}
    </View>
  );
};
export default TvPlayer;

const styles = StyleSheet.create({
  mainProgressContainer: {
    width: percentWidth(100),
    // backgroundColor: 'green',
    zIndex: 1000,
    position: 'absolute',
    bottom: percentHeight(20),
    alignItems: 'center'
  },
  visibleProgressContainer: {
    backgroundColor: colors.primary,
    width: percentWidth(80),
    paddingBottom: percentHeight(1),
  },
  videoFull: {
    width: '100%',
    height: '100%',
    backgroundColor: '#000',
  },
  hidden: {
    height: 0,
    opacity: 0,
  },
  fullIcon: {
    position: 'absolute',
    top: StatusBar.currentHeight,
    right: percentHeight(8),
    elevation: 1,
  },
  progress: {
    //   flex: 1,
    flexDirection: 'row',
    borderRadius: 3,
    alignItems: 'center',
    overflow: 'hidden',
    marginTop: -4,
  },
  trackingControls: {
    // backgroundColor: 'transparent',
    borderRadius: 5,
    //   position: 'absolute',
    //   bottom: 20,
    //   left: 20,
    //   right: 20,
  },
  innerProgressCompleted: {
    height: percentHeight(0.5),
    backgroundColor: colors.dark_red,
  },
  innerProgressRemaining: {
    height: percentHeight(0.5),
    backgroundColor: '#2C2C2C',
  },
  settingButton: {
    backgroundColor: '#2C293D',
    borderRadius: 500,
    width: percentHeight(5),
    height: percentHeight(5),
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: percentWidth(0.4),
    marginRight: percentWidth(0.4)
  },
  secondsText: {
    color: colors.dark_text,
    fontSize: fSize(4)
  }
});
