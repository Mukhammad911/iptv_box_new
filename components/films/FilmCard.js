import React from 'react';
import { Image, Text, TouchableOpacity, View } from 'react-native';
import { colors } from '../../templates/colors';
import { IPTV_ADMIN_URL } from '../../api/url';
import { fSize, percentHeight, percentWidth } from '../../templates/helper';
import FocusableOpacity from '../FocusableOpacity';

const FilmCard = ({ item, navigation, style = {}, format = 'standart', focusedStyle = {}, focusedOnStart = false }) => {

  const changeNameFilm = (name) => {

    let arr_name = name.split("");
    if (arr_name.length < 14) {
      return name;
    }
    let cutname = arr_name.splice(0, 14).join('');

    return cutname + ' ...';

  }
  const { id, name, poster, year, genres } = item;
  return (
    // <View
    //   style={{
    //     flex: 1,
    //     alignItems: 'center',
    //     justifyContent: "flex-start",
    //     marginTop: 5,
    //     ...style,
    //     width: percentWidth(15),
    //     height: percentHeight(50)
    //   }}>
    <FocusableOpacity
      focusedOnStart={focusedOnStart}
      style={{
        ...style,
        // flex: 1,
        backgroundColor: colors.primary,
        borderTopEndRadius: 5,
        borderTopStartRadius: 5,
        marginTop: 5,
        overflow: 'hidden',
        width: percentWidth(15),
        height: percentHeight(50),
        marginHorizontal: percentWidth(2),
        alignItems: 'center',
        justifyContent: "flex-start",
      }}
      focusedStyle={focusedStyle}
      onPress={() =>
        navigation.navigate('vplayer', { id: id, format: format })
      }>
      <View>
        <Image
          source={{ uri: IPTV_ADMIN_URL + 'posters/' + poster }}
          style={{
            height: percentHeight(40),
            width: percentWidth(15),
          }}
        />
      </View>
      <View
        style={{
          marginTop: 5,
          width: percentWidth(15),
          maxHeight: percentHeight(4),
          overflow: 'hidden',
        }}>
        <Text style={{ color: '#fff', fontSize: fSize(7) }}>{changeNameFilm(name)}</Text>
      </View>
      <View
        style={{
          width: percentWidth(15),
          maxHeight: percentHeight(20),
          overflow: 'hidden',
        }}>
        <Text style={{ color: colors.dark_text, fontSize: fSize(5) }}>
          {year} - {genres.length > 0 ? genres[0].name : ''}
        </Text>
      </View>
    </FocusableOpacity>
    // </View>
  );
};
export default FilmCard;
