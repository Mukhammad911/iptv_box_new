import React, { useState } from 'react';
import { FlatList, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { colors } from '../../templates/colors';
import { fSize, percentHeight, percentWidth } from '../../templates/helper';
import { DEFAULT_SCALE, SCALE_PARAM } from '../../templates/scaleParameters';
import FocusableOpacity from '../FocusableOpacity';
import TouchableFocus from '../TouchableFocus';

const ButtonsNavigationMovie = ({
  data,
  setType,
  setLoader,
  setFilterCheck = null,
  type,
  Default = false,
  setDataEmpty = null,
  onChange = null,
  setRef = () => null,
  nextFocusUpRef = null,
  nextFocusDownRef = null,
  blockFocusLeft = false,
  blockFocusUp = false,
  blockFocusRight = false,
  blockFocusDown = false,
}) => {

  const vodType = (item, index) => {
    return (
      <>
        <TouchableFocus
          style={
            item.id === type.id
              ? { ...styles.vodTypes, ...styles.active }
              : { ...styles.vodTypes }
          }
          focusedStyle={{
            transform: [{ scale: SCALE_PARAM }]
          }}
          onPress={ () => {
            setType(item.name);
            setLoader(true);
            setType({ ...type, id: item.id, title: item.name });
            filterCheck();
            change();
          }}
          setRef={!index ? setRef : (() => null)}
          nextFocusLeftSelf={(!index ? blockFocusLeft : false)}
          nextFocusUp={nextFocusUpRef}
          nextFocusUpSelf={blockFocusUp}
          nextFocusRightSelf={(index == (data.length - 1)) ? blockFocusRight : false}
          nextFocusDown={nextFocusDownRef}
          nextFocusDownSelf={blockFocusDown}
        >
          <Text style={{ color: item.id === type.id ? 'white' : '#726A9C', fontSize: fSize(6) }}>
            {item.name}
          </Text>
        </TouchableFocus>
      </>
    );
  };

  const SetData = () => {
    if (setDataEmpty) {
      setDataEmpty([]);
    }
  };

  const filterCheck = () => {
    if (setFilterCheck) {
      setFilterCheck(false);
    }
  };

  const change = () => {
    if (onChange) {
      onChange();
    }
  };
  return (
    <View style={{ marginLeft: 10, marginBottom: 5, marginTop: percentHeight(3) }}>
      <FlatList
        data={data}
        contentContainerStyle={{
          paddingVertical: percentHeight(1.2),
          paddingLeft: percentWidth(1)
        }}
        renderItem={({ item, index }) => vodType(item, index)}
        keyExtractor={(item) => item.id.toString()}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
      />
    </View>
  );
};

export default ButtonsNavigationMovie;
const styles = StyleSheet.create({
  vodTypes: {
    marginHorizontal: 5,
    borderRadius: 18,
    paddingHorizontal: 10,
    justifyContent: 'center',
    alignItems: 'center',
    height: percentHeight(5),
  },
  active: {
    backgroundColor: '#AA0008',
    borderWidth: 0
  },
});
