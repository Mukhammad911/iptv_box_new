import React, { useEffect, useState } from 'react';
import { TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { fSize } from '../../templates/helper';
import { colors } from '../../templates/colors';
import AsyncStorage from '@react-native-community/async-storage';
import { request_delete, request_post } from '../../api/request';
import FocusableOpacity from '../FocusableOpacity';
import TouchableFocus from '../TouchableFocus';

const Favorite = ({
    Check,
    Remove,
    Add,
    autoCheck = true,
    exists = false,
    iconSize = fSize(10),
    setFavoriteButtonRef = () => null,
    nextFocusLeft = null,
    nextFocusUp = null,
    nextFocusRight = null,
    nextFocusDown = null,
    nextFocusLeftSelf = false,
    nextFocusUpSelf = false,
    nextFocusRightSelf = false,
    nextFocusDownSelf = false,
    focusedStyle = null,
    style = {},
}) => {

    const [favorite, setFavorite] = useState(autoCheck ? false : exists)

    useEffect(() => {
        if (autoCheck) {
            check()
        }

    }, [Check])

    const check = async () => {
        const sid = await AsyncStorage.getItem('sid')
        const response = await request_post(`${Check}?sid=${sid}`)
        if (response.success) {
            setFavorite(true)
        } else {
            setFavorite(false)
        }
    }

    const remove = async () => {
        const sid = await AsyncStorage.getItem('sid')
        const response = await request_delete(`${Remove}?sid=${sid}`)
        if (response.success) {
            setFavorite(false)
        } else {
            setFavorite(true)
        }
    }

    const add = async () => {
        const sid = await AsyncStorage.getItem('sid')
        const response = await request_post(`${Add}?sid=${sid}`)
        if (response.success) {
            setFavorite(true)
        } else {
            setFavorite(false)
        }
    }

    return (
        <>
            <TouchableFocus
                focusedStyle={focusedStyle ? focusedStyle : { transform: [{ scale: 1.3 }] }}
                style={style}
                onPress={() => favorite ? remove() : add()}
                setRef={setFavoriteButtonRef}
                nextFocusLeft={nextFocusLeft}
                nextFocusUp={nextFocusUp}
                nextFocusRight={nextFocusRight}
                nextFocusDown={nextFocusDown}
                nextFocusLeftSelf={nextFocusLeftSelf}
                nextFocusUpSelf={nextFocusUpSelf}
                nextFocusRightSelf={nextFocusRightSelf}
                nextFocusDownSelf={nextFocusDownSelf}
            >
                {
                    favorite ?
                        <Icon name={'star'} size={iconSize} color={'white'} />
                        :
                        <Icon name={'star-outline'} size={iconSize} color={'white'} />
                }
            </TouchableFocus>
        </>
    );
};
export default Favorite
