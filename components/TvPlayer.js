import {
  BackHandler,
  StyleSheet,
  TouchableOpacity,
  View,
  StatusBar,
} from 'react-native';
import Video from 'react-native-video';
import Icon from 'react-native-vector-icons/Ionicons';
import React, {Fragment, useEffect, useRef, useState} from 'react';
import ScreenOrientation from 'react-native-orientation-locker/ScreenOrientation';
import {bindActionCreators} from 'redux';
import * as actions from '../redux/actions';
import {header} from './helper';
import {useDispatch, useSelector} from 'react-redux';
import {colors} from '../templates/colors';
import {fSize, percentHeight, percentWidth} from '../templates/helper';
import Loader from './Loader';
import {Button} from 'react-native';
import {Text} from 'react-native';

const TvPlayer = ({
  navigation = null,
  autoShowHeader = false,
  back = null,
  uri = null,
  onFull = null,
  onPlay,
  autoPlay = true,
}) => {
  const dispatch = useDispatch();
  const [orientation, setOrientation] = useState('LANDSCAPE');
  const [showFullIcon, setShowFullIcon] = useState(false);
  const [fullSize, setFullSize] = useState(false);
  const [visibleStatusBar, setVisibleStatusBar] = useState(false);
  const [play, setPlay] = useState(autoPlay);
  const [loader, setLoader] = useState(false);
  const [currentTime, setCurrentTime] = useState(0.0);
  const playerRef = useRef(null);
  const sid = useSelector((state) => state.authReducer.sid);
  const [showLeftSkipButton, setShowLeftSkipButton] = useState(false);
  const [showRightSkipButton, setShowRightSkipButton] = useState(false);
  const [duration, setDuration] = useState(0);
  const [playerWidth, setPlayerWidth] = useState(0);
  const [showControls, setShowControls] = useState(false);
  const [showPlayBtn, setShowPlayBtn] = useState(false);
  let interval = null;
  let timeOutID;
  let controlTimerId;
  const getCurrentTimePercentage = () => {
    if (currentTime > 0) {
      return parseFloat(currentTime) / parseFloat(duration);
    }
    return 0;
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', () => {
      headerShown(true);
      dispatch(actions.setNavigationVisibility(true));
      // setOrientation('PORTRAIT');
    });
    dispatch(actions.getHistory(sid));
  }, [play, uri]);

  const hideFull = () => {
    setShowFullIcon(false);
  };

  const showFull = () => {
    setShowFullIcon(true);
    if (interval) {
      clearTimeout(interval);
      interval = setTimeout(hideFull, 3790);
      return;
    }
    interval = setTimeout(hideFull, 3790);
  };

  const headerShown = (show) => {
    if (autoShowHeader) {
      header(navigation, show);
    }
  };
  const videoSizeChange = () => {
    if (orientation === 'PORTRAIT') {
      setOrientation('LANDSCAPE');
      headerShown(false);
      dispatch(actions.setNavigationVisibility(false));
      setVisibleStatusBar(true);
    } else {
      // setOrientation('PORTRAIT');
      headerShown(true);
      dispatch(actions.setNavigationVisibility(true));
      setVisibleStatusBar(false);
    }
  };

  const onProgress = ({currentTime}) => setCurrentTime(currentTime);

  const twodigit = (digit) => (+digit > 9 ? `${digit}` : `0${digit}`);

  const getVideoTimeFormat = (duration) => {
    if (duration < 1) {
      return '00:00:00';
    }

    let timeFormat = '';

    const durationHours = Math.floor(duration / 3600);

    if (durationHours > 0) {
      timeFormat = twodigit(durationHours);
    }

    const durationMinutes = Math.floor((duration % 3600) / 60);

    if (durationMinutes > 0) {
      timeFormat =
        timeFormat.length > 0
          ? `${timeFormat}:${twodigit(durationMinutes)}`
          : `00:${twodigit(durationMinutes)}`;
    }

    const durationSeconds = Math.floor(
      duration - durationHours * 3600 - durationMinutes * 60,
    );

    if (durationSeconds > 0) {
      timeFormat =
        timeFormat.length > 0
          ? `${timeFormat}:${twodigit(durationSeconds)}`
          : `00:00:${twodigit(durationSeconds)}`;
    } else {
      timeFormat += ':00';
    }

    return timeFormat;
  };

  const showStatusBar = () => {
    setShowControls(true);
    setShowPlayBtn(true);
    if (controlTimerId) {
      clearTimeout(controlTimerId);
      controlTimerId = setTimeout(() => {
        setShowControls(false);
        setShowPlayBtn(false);
      }, 3000);
      return;
    }
    controlTimerId = setTimeout(() => {
      setShowControls(false);
      setShowPlayBtn(false);
    }, 3000);
  };

  return (
    <View>
      <StatusBar hidden={visibleStatusBar} backgroundColor={colors.primary} />
      <ScreenOrientation orientation={orientation} />
      <View style={fullSize ? styles.videoFull : styles.video}>
        <TouchableOpacity
          onPressIn={() => {
            showFull();
            showStatusBar();
            setShowPlayBtn(!showPlayBtn);
          }}>
          <Video
            ref={playerRef}
            style={{
              height: '100%', 
              width: '100%'}}
            // source={require('../video/1.mp4')}
            source={{uri}}
            resizeMode="cover"
            controls={false}
            fullscreen={true}
            // onFullscreenPlayerDidPresent={(evet) => console.log('event:', evet)}
            paused={!play}
            onProgress={onProgress}
            // onBuffer={(buffer) => console.buffer('buffer:', buffer)}
            onReadyForDisplay={() => setLoader(false)}
            onLoad={({duration}) => {
              setDuration(duration);
            }}
            onError={(err) => {
              setLoader(false);
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            position: 'absolute',
            bottom: percentHeight(5),
            top: percentHeight(5),
            left: percentWidth(10),
            width: percentWidth(20),
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => {
            playerRef.current.seek(currentTime - 10);
            setShowLeftSkipButton(true);
            if (timeOutID) {
              clearTimeout(timeOutID);
            }

            timeOutID = setTimeout(() => setShowLeftSkipButton(false), 2000);
          }}>
          {showLeftSkipButton && (
            <Fragment>
              <Icon
                name="play-back-outline"
                color={colors.secondary}
                size={fSize(10)}
              />
              <Text
                style={{
                  color: colors.secondary,
                  fontSize: fSize(10),
                }}>
                -10 сек
              </Text>
            </Fragment>
          )}
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            position: 'absolute',
            bottom: percentHeight(5),
            top: percentHeight(5),
            right: percentWidth(10),
            width: percentWidth(20),
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}
          onPress={() => {
            playerRef.current.seek(currentTime + 10);
            setShowRightSkipButton(true);
            if (timeOutID) {
              clearTimeout(timeOutID);
            }

            timeOutID = setTimeout(() => setShowRightSkipButton(false), 2000);
          }}>
          {showRightSkipButton && (
            <Fragment>
              <Icon
                name="play-forward-outline"
                color={colors.secondary}
                size={fSize(10)}
              />
              <Text
                style={{
                  color: colors.secondary,
                  fontSize: fSize(10),
                }}>
                +10 сек
              </Text>
            </Fragment>
          )}
        </TouchableOpacity>
        {showFullIcon ? (
          <>
            <TouchableOpacity
              style={{
                ...styles.fullIcon,
                right:
                  orientation === 'PORTRAIT'
                    ? percentHeight(2)
                    : percentHeight(3),
                top: 10
              }}
              onPress={() => {
                setFullSize(!fullSize);
                videoSizeChange();
                if (onFull) {
                  onFull(!fullSize);
                }
              }}>
              <Icon name={'md-expand'} size={fSize(12)} color={'#fff'} />
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                position: 'absolute',
                // top: StatusBar.currentHeight,
                top: 10,
                left: 15,
              }}
              onPress={() => {
                back();
                dispatch(actions.setNavigationVisibility(true));
                // setOrientation('PORTRAIT');
              }}>
              <Icon
                name={'arrow-back'}
                color={colors.secondary}
                size={fSize(13)}
              />
            </TouchableOpacity>
          </>
        ) : null}
        {loader ? <Loader /> : null}
        {showPlayBtn ? (
          <>
            <TouchableOpacity
              style={{
                position: 'absolute',
                // top: StatusBar.currentHeight,
                top: 10,
                left: 15,
              }}
              onPress={() => {
                back();
                actions.setNavigationVisibility(true);
                // setOrientation('PORTRAIT');
              }}>
              <Icon
                name={'arrow-back'}
                color={colors.secondary}
                size={fSize(13)}
              />
            </TouchableOpacity>
            <TouchableOpacity
              style={{
                position: 'absolute',
                top:
                  orientation !== 'PORTRAIT'
                    ? percentHeight(25)
                    : percentHeight(13),
                alignSelf: 'center',
              }}
              onPress={() => {
                setPlay(!play);
              }}>
              {!play ? (
                <Icon name={'play'} color={colors.secondary} size={fSize(15)} />
              ) : (
                <Icon
                  name={'pause'}
                  color={colors.secondary}
                  size={fSize(15)}
                />
              )}
            </TouchableOpacity>
          </>
        ) : null}
      </View>
      {showControls ? (
        <View style={styles.trackingControls}>
          <TouchableOpacity
            onLayout={({nativeEvent}) => {
              const {layout} = nativeEvent;

              setPlayerWidth(layout.width);
            }}
            onPress={({nativeEvent}) => {
              const {pageX} = nativeEvent;
              const seek = parseFloat(pageX / playerWidth) * duration - 20;
              playerRef.current.seek(seek);
              setPlay(true);
            }}>
            <View style={styles.progress}>
              <View
                style={[
                  styles.innerProgressCompleted,
                  {
                    flex: getCurrentTimePercentage() * 100,
                  },
                ]}
              />
              <View
                style={{
                  borderRadius: 100,
                  backgroundColor: colors.dark_red,
                  width: percentHeight(2),
                  height: percentHeight(2),
                  zIndex: 999,
                }}></View>
              <View
                style={[
                  styles.innerProgressRemaining,
                  {
                    flex: (1 - getCurrentTimePercentage()) * 100,
                  },
                ]}
              />
            </View>
          </TouchableOpacity>
          <View
            style={{
              flex: 1,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <Text
              style={{
                color: '#fff',
                fontSize: fSize(5),
              }}>
              {getVideoTimeFormat(currentTime)}
            </Text>

            <Text
              style={{
                color: '#fff',
                fontSize: fSize(5)
              }}>
              {getVideoTimeFormat(duration)}
            </Text>
          </View>
        </View>
      ) : null}
    </View>
  );
};
export default TvPlayer;

const styles = StyleSheet.create({
  videoFull: {
    width: '100%',
    height: '100%',
    backgroundColor: '#000',
  },
  video: {
    height: percentHeight(70),
    width: '100%',
  },
  hidden: {
    height: 0,
    opacity: 0,
  },
  fullIcon: {
    position: 'absolute',
    top: StatusBar.currentHeight,
    right: percentHeight(8),
    elevation: 1,
  },
  progress: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 3,
    alignItems: 'center',
    overflow: 'hidden',
  },
  trackingControls: {
    backgroundColor: 'transparent',
    borderRadius: 5,
    position: 'absolute',
    bottom: 20,
    left: 20,
    right: 20,
  },
  innerProgressCompleted: {
    height: percentHeight(0.7),
    backgroundColor: colors.dark_red,
  },
  innerProgressRemaining: {
    height: percentHeight(0.7),
    backgroundColor: '#2C2C2C',
  },
});
