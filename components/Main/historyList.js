import React from 'react';
import {View, Text, FlatList, Button} from 'react-native';
import {colors} from '../../templates/colors';
import {fSize} from '../../templates/helper';
import HistoryItem from './historyItem';

const HistoryList = ({data, navigate}) => {
  const newData = data.slice(data.length - 10, data.length);
  newData.push({
    id: new Date().toISOString(),
  });
  return (
    !!data.length && (
      <View>
        <Text style={{fontSize: fSize(14), color: colors.secondary}}>
          Последние просмотренные
        </Text>
        <FlatList
          data={newData}
          renderItem={({item}) => (
            <HistoryItem filmHistory={item} navigate={navigate} />
          )}
          keyExtractor={(item) => item.id.toString()}
          horizontal={true}
        />
      </View>
    )
  );
};

export default HistoryList;
