import AsyncStorage from '@react-native-community/async-storage'
import React, { useEffect, useRef, useState } from 'react'
import { View, Text, Image, TouchableOpacity } from 'react-native'
import { Button } from 'react-native-elements'
import { ScrollView } from 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/Ionicons'
import { request_get } from '../api/request'
import { colors } from '../templates/colors'
import { fSize, percentHeight, percentWidth } from '../templates/helper'
import { DEFAULT_SCALE, SCALE_PARAM } from '../templates/scaleParameters'
import FocusableOpacity from './FocusableOpacity'
import Favorite from './General/Favorite'
import KeyEvent from 'react-native-keyevent'
import TouchableFocus from './TouchableFocus'
import { StackActions } from '@react-navigation/native'

const MoviesInfo = ({ details, setShowPlayer, data, setUri, format, navigation }) => {
    // KeyEvent.removeKeyDownListener()
    const [showModal, setShowModal] = useState(false)
    const [showSeasons, setShowSeasons] = useState(false)
    const [choosenSeason, setChoosenSeason] = useState(1)
    const [showSeries, setShowSeries] = useState(false)

    const [watchFocused, setWatchFocused] = useState(false)
    const [favoriteFocused, setFavoriteFocused] = useState(false)
    const watchRef = useRef(null)
    const favoriteRef = useRef(null)

    const [watchButtonRef, setWatchButtonRef] = useState(null)
    const [favoriteButtonRef, setFavoriteButtonRef] = useState(null)

    const [goBackButtonRef, setGoBackButtonRef] = useState(null)

    // --------------------------------------------------------------------------------------------------------------

    useEffect(() => {
        KeyEvent.onKeyDownListener((keyEvent) => {
            //   alert(
            //     `
            //     onKeyUp keyCode: ${keyEvent.keyCode}
            //     Action: ${keyEvent.action}
            //     Key: ${keyEvent.pressedKey}
            //     `
            //   )
      
        })
        // return () => KeyEvent.removeKeyDownListener()
    }, [])

    // --------------------------------------------------------------------------------------------------------------

    const changeSeries = async (data_id) => {
        const sid = await AsyncStorage.getItem('sid');
        const response = await request_get(
            `get_series_url/${data_id}/${format}?sid=${sid}`,
        );
        const file_uri = response.data.file_server;
        if (response.success) {
            setUri(file_uri)
            setShowModal(false)
            setShowSeasons(true)
            setShowSeries(false)
            setShowPlayer(true)
        }
    };

    const getSeriesList = (list) => {
        const listArr = Object.entries(list);
        return listArr.map((elem, index) => (
            <TouchableFocus
                style={{
                    width: '100%',
                    paddingVertical: percentHeight(1),
                    flexDirection: 'row',
                    alignItems: 'center',
                    paddingLeft: percentWidth(2)
                }}
                onPress={() => {
                    changeSeries(elem[1]['id'])
                }}
                focusedOnStart={!index}
                focusedStyle={{
                    backgroundColor: '#2C293D'
                }}
                nextFocusLeftSelf
                nextFocusRightSelf
                nextFocusDownSelf={(index == (listArr.length - 1))}
            >
                <Text
                    style={{
                        color: colors.secondary,
                        fontSize: fSize(6),
                    }}
                >
                    {elem[0] + ' серия'}
                </Text>
            </TouchableFocus>
        ))
    }

    return (
        <View
            style={{
                flex: 1,
                flexDirection: 'row',
                paddingBottom: percentHeight(5)
            }}
        >
            <TouchableFocus
                setRef={setGoBackButtonRef}
                style={{
                    position: 'absolute',
                    marginTop: percentWidth(2),
                    marginLeft: percentWidth(2),
                    borderRadius: 100,
                    height: percentWidth(3.5),
                    width: percentWidth(3.5),
                    justifyContent: 'center',
                    alignItems: 'center',
                }}
                focusedStyle={{
                    backgroundColor: colors.dark_red,
                }}
                onPress={() => {
                    navigation.dispatch(StackActions.pop(1));
                }}
                nextFocusLeftSelf
                nextFocusUpSelf
                nextFocusRightSelf
                nextFocusDown={watchButtonRef}
            >
                <Icon name="arrow-back" color={colors.secondary} size={fSize(10)} />
            </TouchableFocus>
            {/* ---------------------------------------------------------------------- */}
            {
                showModal ?
                    <View
                        style={{
                            position: 'absolute',
                            right: 0,
                            width: percentWidth(25),
                            height: percentHeight(100),
                            zIndex: 1000,
                            backgroundColor: '#191723',
                        }}
                    >
                        <View
                            style={{
                                borderBottomWidth: 1,
                                borderBottomColor: colors.dark_text,
                                flexDirection: 'row',
                                alignItems: 'center',
                                justifyContent: 'space-between',
                                paddingHorizontal: percentWidth(2)
                            }}
                        >
                            <Text
                                style={{
                                    color: colors.secondary,
                                    fontSize: fSize(10),
                                    paddingVertical: percentHeight(2)
                                }}
                            >
                                Серии
                            </Text>
                            <TouchableFocus
                                onPress={() => {
                                    if (showSeries) {
                                        setShowSeasons(true)
                                        setShowSeries(false)
                                    } else if (showSeasons) {
                                        setShowSeasons(false)
                                        setShowModal(false)
                                        watchButtonRef.setNativeProps({
                                            hasTVPreferredFocus: true
                                        })
                                    }
                                }}
                                focusedStyle={{
                                    transform: [{ scale: 1.3 }]
                                }}
                                nextFocusLeftSelf
                                nextFocusUpSelf
                                nextFocusRightSelf
                            >
                                <Icon name={showSeasons ? "close-outline" : "arrow-undo-outline"} size={fSize(10)} color={colors.dark_text} />
                            </TouchableFocus>
                        </View>
                        <ScrollView
                            style={{
                                marginBottom: percentHeight(2)
                            }}
                        >
                            {
                                showSeasons ?
                                    data.map((elem, index) => (
                                        <TouchableFocus
                                            style={{
                                                width: '100%',
                                                paddingVertical: percentHeight(1),
                                                flexDirection: 'row',
                                                alignItems: 'center',
                                                paddingLeft: percentWidth(2),
                                            }}
                                            onPress={() => {
                                                setChoosenSeason(+elem[0])
                                                setShowSeasons(false)
                                                setShowSeries(true)
                                            }}
                                            focusedOnStart={!index}
                                            focusedOnStart
                                            focusedStyle={{
                                                backgroundColor: '#2C293D'
                                            }}
                                            nextFocusLeftSelf
                                            nextFocusRightSelf
                                            nextFocusDownSelf={(index == (data.length - 1))}
                                        >
                                            <Text
                                                style={{
                                                    color: colors.secondary,
                                                    fontSize: fSize(6),
                                                }}
                                            >
                                                {elem[0] + " сезон"}
                                            </Text>
                                        </TouchableFocus>
                                    ))
                                    :
                                    null
                            }
                            {
                                showSeries ?
                                    getSeriesList(data[0][choosenSeason])
                                    :
                                    null
                            }
                        </ScrollView>
                    </View>
                    :
                    null
            }
            {/* ------------------------------------------------------------------------------- */}
            <View
                style={{
                    width: percentWidth(65),
                    paddingLeft: percentWidth(7),
                    paddingTop: percentHeight(8)
                }}
            >
                <Text
                    style={{
                        color: colors.secondary,
                        fontSize: fSize(10),
                        marginBottom: percentHeight(6)
                    }}
                >
                    {details.name}
                </Text>
                <View
                    style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                    }}
                >
                    <View
                        style={{
                            borderRightColor: colors.dark_text,
                            borderRightWidth: 1,
                            paddingRight: percentWidth(2),
                            marginRight: percentWidth(2)
                        }}
                    >
                        <TouchableFocus
                            style={{
                                backgroundColor: colors.dark_red,
                                borderRadius: 100,
                                width: percentWidth(10),
                                height: percentHeight(5),
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginRight: percentWidth(2),
                            }}
                            onPress={() => {
                                if (data.length > 0) {
                                    setShowModal(true)
                                    setShowSeries(false)
                                    setShowSeasons(true)
                                    return
                                }
                                setShowPlayer(true)
                            }}
                            focusedStyle={{
                                transform: [{ scale: SCALE_PARAM }]
                            }}
                            setRef={setWatchButtonRef}
                            nextFocusLeftSelf
                            nextFocusDownSelf
                            nextFocusUp={goBackButtonRef}
                            nextFocusRight={favoriteButtonRef}
                            focusedOnStart
                        >
                            <Text
                                style={{
                                    color: colors.secondary,
                                    fontSize: fSize(6),
                                    textAlign: 'center'
                                }}
                            >
                                Смотреть
                            </Text>
                        </TouchableFocus>
                    </View>
                    <Favorite
                        Check={`favorite/check/${details.id}`}
                        Add={`favorite/${details.id}`}
                        Remove={`favorite/${details.id}`}
                        setFavoriteButtonRef={setFavoriteButtonRef}
                        nextFocusRightSelf
                        nextFocusDownSelf
                        nextFocusLeft={watchButtonRef}
                        nextFocusUp={goBackButtonRef}
                    />
                </View>
                <ScrollView
                    style={{
                        marginTop: percentHeight(5),
                    }}
                >
                    <View
                        style={{
                            flexDirection: 'row',
                            marginBottom: percentHeight(1)
                        }}
                    >
                        <View
                            style={{
                                width: percentWidth(15)
                            }}
                        >
                            <Text
                                style={{
                                    color: colors.dark_text,
                                    fontSize: fSize(5)
                                }}
                            >
                                Год
                            </Text>
                        </View>
                        <View>
                            <Text
                                style={{
                                    color: colors.secondary,
                                    fontSize: fSize(5)
                                }}
                            >
                                {details.year}
                            </Text>
                        </View>
                    </View>
                    <View
                        style={{
                            flexDirection: 'row',
                            marginBottom: percentHeight(1)
                        }}
                    >
                        <View
                            style={{
                                width: percentWidth(15)
                            }}
                        >
                            <Text
                                style={{
                                    color: colors.dark_text,
                                    fontSize: fSize(5)
                                }}
                            >
                                Страна
                            </Text>
                        </View>
                        <View>
                            <Text
                                style={{
                                    color: colors.secondary,
                                    fontSize: fSize(5)
                                }}
                            >
                                {details.country}
                            </Text>
                        </View>
                    </View>
                    <View
                        style={{
                            flexDirection: 'row',
                            marginBottom: percentHeight(1)
                        }}
                    >
                        <View
                            style={{
                                width: percentWidth(15)
                            }}
                        >
                            <Text
                                style={{
                                    color: colors.dark_text,
                                    fontSize: fSize(5)
                                }}
                            >
                                Жанр
                            </Text>
                        </View>
                        <View>
                            <Text
                                style={{
                                    color: colors.secondary,
                                    fontSize: fSize(5)
                                }}
                            >
                                {details.genres.map((elem, ind) => ind == details.genres.length - 1 ? elem.name.toString() : elem.name.toString() + ", ")}
                            </Text>
                        </View>
                    </View>
                    <View
                        style={{
                            flexDirection: 'row',
                            marginBottom: percentHeight(1)
                        }}
                    >
                        <View
                            style={{
                                width: percentWidth(15)
                            }}
                        >
                            <Text
                                style={{
                                    color: colors.dark_text,
                                    fontSize: fSize(5)
                                }}
                            >
                                Рейтинг Кинопоиск
                            </Text>
                        </View>
                        <View>
                            <Text
                                style={{
                                    color: colors.secondary,
                                    fontSize: fSize(5)
                                }}
                            >
                                {details.r_kinopoisk}
                            </Text>
                        </View>
                    </View>
                    <View
                        style={{
                            flexDirection: 'row',
                            marginBottom: percentHeight(1)
                        }}
                    >
                        <View
                            style={{
                                width: percentWidth(15)
                            }}
                        >
                            <Text
                                style={{
                                    color: colors.dark_text,
                                    fontSize: fSize(5)
                                }}
                            >
                                Рейтинг IMDb
                            </Text>
                        </View>
                        <View>
                            <Text
                                style={{
                                    color: colors.secondary,
                                    fontSize: fSize(5)
                                }}
                            >
                                {details.r_imdb}
                            </Text>
                        </View>
                    </View>
                    <View
                        style={{
                            marginTop: percentHeight(5),
                            width: percentWidth(55)
                        }}
                    >
                        <Text
                            style={{
                                color: colors.secondary,
                                fontSize: fSize(6),
                            }}
                        >
                            {details.description}
                        </Text>
                    </View>
                </ScrollView>
            </View>
            <View
                style={{
                    width: percentWidth(30),
                    // backgroundColor: 'red',
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    paddingTop: percentHeight(15),
                }}
            >
                <View>
                    <Image
                        source={{ uri: details.poster }}
                        style={{
                            height: percentHeight(60),
                            width: percentWidth(23),
                        }}
                        resizeMode="cover"
                    />
                </View>
            </View>
        </View>
    )
}

export default MoviesInfo