/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useEffect } from 'react'
import { Text } from 'react-native'
import { View } from 'react-native'
import { useDispatch, useSelector } from 'react-redux';
import { setFirstElementRef } from '../redux/actions';
import { colors } from '../templates/colors';
import { fSize, percentHeight, percentWidth } from '../templates/helper';
import TouchableFocus from './TouchableFocus';
import {  } from 'react-native';

const SidNotFound = ({ navigate }) => {

    const dispatch = useDispatch();

    const { menuRef, firstElementRef } = useSelector(state => state.refReducer);

    const li = firstElementRef?._nativeTag;

    let currentTag = 0;

    useEffect(() => {
        try {
            if ((firstElementRef != null) && (firstElementRef._nativeTag == currentTag)) {
                firstElementRef.setNativeProps({
                    hasTVPreferredFocus: true,
                });
            }
        } catch (error) {
        }
        // return () => dispatch(setFirstElementRef(null))
    }, [firstElementRef]);

    return (
        <View
            style={{
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            <View
                style={{
                    marginBottom: percentHeight(4),
                }}
            >
                <Text
                    style={{
                        fontSize: fSize(7),
                        textAlign: 'center',
                        color: 'white',
                    }}
                >
                    У вас не активирована ни одна подписка
                </Text>
            </View>
            <TouchableFocus
                focusedOnStart
                setRef={reff => {
              
                    currentTag = reff._nativeTag;
                    dispatch(setFirstElementRef(reff));
                }}
                onPress={() => navigate('subscribes')}
                style={{
                    width: percentWidth(20),
                }}
                focusedStyle={{
                    transform: [{ scale: 1.5 }],
                }}
                nextFocusUpSelf
                nextFocusRightSelf
                nextFocusDownSelf
                nextFocusLeft={menuRef}
            >
                <Text
                    style={{
                        color: colors.dark_red,
                        fontSize: fSize(7),
                        textAlign: 'center',
                    }}
                >
                    Активировать
                </Text>
            </TouchableFocus>
        </View>
    )
}

export default SidNotFound;
