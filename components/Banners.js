import React, { useState } from 'react';
import { View, FlatList, Dimensions, TouchableWithoutFeedback } from 'react-native';
import { IPTV_ADMIN_URL } from '../api/url';
import { Divider, Card, Image, Text } from 'react-native-elements';
import { fSize, percentHeight, percentWidth } from '../templates/helper';
import { DEFAULT_SCALE, SCALE_PARAM } from '../templates/scaleParameters';
import FocusableOpacity from './FocusableOpacity';
import TouchableFocus from './TouchableFocus';
const { width } = Dimensions.get('window');

const Banners = ({ banners, search = '', navigate, neededFocus = false }) => {
    const bannerCard = (img, index) => {
        const child = <View>
            <Image
                style={{ width: percentWidth(30), height: percentHeight(25) }}
                source={{ uri: `${IPTV_ADMIN_URL}banners/${img.banner}` }}
                resizeMode={'cover'}
            />
            <View style={{
                position: 'absolute',
                top: 10,
                left: 10,
                bottom: 0,
                right: 0,
            }}>
                <Text style={{ color: "#FFF", fontSize: fSize(7), fontWeight: 'bold' }}>{img.name}</Text>
            </View>
        </View>

        return (
            img.id ?
                <TouchableFocus
                    focusedStyle={{
                        transform: [{ scale: SCALE_PARAM }],
                    }}
                    style={{
                        marginHorizontal: percentWidth(2),
                    }}
                    onPress={() => navigate('vplayer', { "id": img.id, "format": "standart" })}
                    // focusedOnStart={!index}
                >
                    {child}
                </TouchableFocus> : child
        )
    };

    return (
        search == "" && banners.length > 0 &&
        <View style={{ marginVertical: 10 }}>
            <FlatList
                data={banners}
                contentContainerStyle={{
                    paddingVertical: percentHeight(2),
                    paddingLeft: percentWidth(2)
                }}
                renderItem={({ item, index }) => bannerCard(item, index)}
                horizontal={true}
                showsHorizontalScrollIndicator={false}
                keyExtractor={item => item.id.toString()}
            />
        </View>
    )
};

export default Banners;
