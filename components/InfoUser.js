import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Button, findNodeHandle } from 'react-native';
import { BASE_API_URL, request_get } from '../api/request';
import { colors } from '../templates/colors';
import Loader from './Loader';
import { fSize, percentHeight, percentWidth } from '../templates/helper';
import Icon from 'react-native-vector-icons/Ionicons';
import { useDispatch, useSelector } from 'react-redux';
import { setAuthState, setFirstElementRef, setSid } from '../redux/actions';
import AsyncStorage from '@react-native-community/async-storage';
import ScreenHeader from './ScreenHeader';
import { DEFAULT_SCALE, SCALE_PARAM } from '../templates/scaleParameters';
import FocusableOpacity from './FocusableOpacity';
import TouchableFocus from './TouchableFocus';

// Токенизация проведена

const InfoUser = ({ navigate, sid, HOCREF = null, navigation }) => {
  const dispatch = useDispatch();
  const [user, setUser] = useState({
    name: null,
    phone: null,
    protected_code: null,
    expire_date: null,
    count_equipment: null,
  });
  const [loader, setLoader] = useState(true);
  const [refreshUserInfo, setRefreshUserInfo] = useState(false)
  
  const menuRef = useSelector(state => state.refReducer.menuRef);

  // -------------------------------------------------------------------------------------------------------------------------------

  const [replenishButtonRef, setReplenishButtonRef] = useState(null)

  // -------------------------------------------------------------------------------------------------------------------------------

  const userExit = async () => {
    dispatch(setAuthState(false));
    dispatch(setSid(''))
    await AsyncStorage.removeItem('sid');
    await AsyncStorage.removeItem('access_token')
  };


  useEffect(() => {
    async function getData() {
      try {
        const access_token = await AsyncStorage.getItem('access_token')
        let response = await fetch(`${BASE_API_URL}api/get_user_info/${sid ? sid : ''}`, { headers: { 'Authorization': access_token, 'Accept': 'application/json' } })
        let responseJSON = await response.json()
        if (!responseJSON.success) {
          // dispatch(setAuthState(false));
          setLoader(false)
          return;
        }
        const data = responseJSON.data;
        if (!sid) {
          setUser({
            name: data.name,
            phone: data.phone,
            balance: data.balance_sum,
          });
        } else {
          setUser({
            name: data.name,
            phone: data.phone,
            protected_code: data.protected_code,
            expire_date: data.expire_date,
            count_equipment: data.count_equipment,
            balance: data.balance_sum,
          });
        }
        setLoader(false);
      } catch (e) {
        console.log('error ', e);
      }
    }
    getData();
  }, [refreshUserInfo, sid]);

  const itemView = (iconName, text) => {
    return (
      <View style={styles.itemView}>
        <Icon name={iconName} color={'#39354D'} size={fSize(6)} />
        <Text
          style={{ color: colors.secondary, marginLeft: 5, fontSize: fSize(6) }}>
          {text}
        </Text>
      </View>
    );
  };

  const itemTouch = (iconName, text, route = '', params = {}) => {
    return (
      <TouchableFocus
        setRef={(reff) => {
          if (params.id == 0) {
            dispatch(setFirstElementRef(reff));
          }
        }}
        focusedStyle={{
          transform: [{ scale: SCALE_PARAM }]
        }}
        focusedOnStart={!params.id}
        style={{ ...styles.itemTouch }}
        // onPress={() => params.id == 4 ? userExit() : navigation.push(route, params)}
        onPress={() => params.id == 4 ? userExit() : navigation.navigate(route)}
        nextFocusDownSelf
        nextFocusRightSelf={(params.id == 4)}
        nextFocusUp={replenishButtonRef}
        // nextFocusLeft={findNodeHandle(HOCREF)}
        // nextFocusLeft={(HOCREF ? findNodeHandle(HOCREF) : null)}
        nextFocusLeft={((params.id == 0) ? menuRef : null)}
      >
        <Icon name={iconName} color={'#39354D'} size={fSize(12)} />
        <Text
          style={{ color: colors.dark_text, fontSize: fSize(5) }}>
          {text}
        </Text>
      </TouchableFocus>
    );
  };

  return (
    <View style={styles.container}>
      <ScreenHeader screenName="Профиль" />
      {loader ? (
        <Loader />
      ) : (
        <View style={styles.content}>
          <View style={styles.card}>
            <View style={styles.userInfo}>
              <View style={{ width: percentHeight(13) }}>
                <View style={styles.profileIcon}>
                  <Icon
                    name={'person-outline'}
                    size={fSize(12)}
                    color={'#4B4563'}
                  />
                </View>
              </View>
              <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start' }}>
                <Text style={styles.userName}>{user.name}</Text>
                <Text style={{ ...styles.userName, color: '#3A3752', alignItems: 'flex-start' }}>
                  {user.phone}
                </Text>
              </View>
            </View>
            <View
              style={{
                borderRightWidth: 1,
                borderRightColor: '#201C30',
                marginHorizontal: percentWidth(3)
              }} />
            <View
              style={{
                justifyContent: 'space-evenly',
                height: percentHeight(15)
              }}
            >
              {user.expire_date ? itemView('hourglass-outline', 'Годен до:  ' + user.expire_date) : null}
              {user.count_equipment ? itemView(
                'wifi-outline',
                'Количество устройств:  ' + user.count_equipment,
              ) : null}
              <View style={styles.item}>
                <Icon name={'wallet-outline'} color={'#39354D'} size={fSize(6)} />
                <Text
                  style={{ color: colors.secondary, marginLeft: 5, fontSize: fSize(6) }}>
                  {`Баланс:  ${user.balance}`}
                </Text>
                <TouchableFocus
                  style={{
                    position: 'absolute',
                    right: 0
                  }}
                  focusedStyle={{
                    transform: [{ scale: SCALE_PARAM }]
                  }}
                  onPress={() => {
                    // setReplenish(true)
                    navigate('replenish')
                  }}
                  setRef={setReplenishButtonRef}
                  nextFocusUpSelf
                  nextFocusRightSelf
                  nextFocusLeft={menuRef}
                >
                  <Text
                    style={{
                      textAlign: 'center',
                      color: colors.dark_red,
                      fontSize: fSize(6)
                    }}
                  >
                    Пополнить
                  </Text>
                </TouchableFocus>
              </View>
            </View>
          </View>
          <View
            style={{
              paddingHorizontal: 15,
              paddingVertical: percentHeight(8),
              flexDirection: 'row',
              justifyContent: 'center'
            }}
          >
            {itemTouch('cash-outline', 'Подписки', 'subscribes', { id: 0 })}
            {itemTouch('cart-outline', 'Товары', 'products', { id: 1 })}
            {sid ? itemTouch('star-outline', 'Избранные', 'favorite', { id: 2 }) : null}
            {itemTouch('information-circle-outline', 'О нас', 'contacts', { id: 3 })}
            {itemTouch('exit-outline', 'Выход', 'contacts', { id: 4 })}
            {/* <Button color="red" title={'Выход'} onPress={userExit} /> */}
          </View>
        </View>
      )}
    </View>
  );
};

export default InfoUser;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.primary,
  },
  content: {
    marginTop: percentHeight(5),
  },
  card: {
    marginVertical: 5,
    backgroundColor: '#191623',
    paddingVertical: 20,
    paddingHorizontal: percentWidth(7),
    flexDirection: 'row'
  },
  cardContent: {
    paddingVertical: 12.5,
    paddingHorizontal: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  userInfo: {
    flexDirection: 'row',
    height: percentHeight(15),
    alignItems: 'center',
  },
  cardTitle: {
    color: colors.secondary,
    fontWeight: 'bold',
  },
  userName: {
    color: colors.secondary,
    fontSize: fSize(7)
  },
  profileIcon: {
    height: percentHeight(10),
    width: percentHeight(10),
    backgroundColor: '#2C2A3C',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
  },
  itemView: {
    // height: percentHeight(13),
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemTouch: {
    height: percentHeight(8),
    alignItems: "center",
    marginHorizontal: percentWidth(2)
  },
  item: {
    // height: percentHeight(7),
    flexDirection: 'row',
    alignItems: 'center',
    minWidth: percentWidth(25)
  },
});
