import React, {useEffect, useState} from 'react';
import {BackHandler} from 'react-native';
import ScreenOrientation from 'react-native-orientation-locker/ScreenOrientation';
import VideoPlayer from 'react-native-video-controls';
import {header} from './helper';
import {useDispatch} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../redux/actions';

const Player = ({uri, navigation, full}) => {
  const [orientation, setOrientation] = useState('LANDSCAPE');

  const dispatch = useDispatch();

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', () => {
      header(navigation, true);
      actions.setNavigationVisibility(true);
      // setOrientation('PORTRAIT');
    });
  }, []);

  const videoSizeChange = () => {
    if (orientation === 'PORTRAIT') {
      setOrientation('LANDSCAPE');
      actions.setNavigationVisibility(false);
    } 
    // else {
    //   setOrientation('PORTRAIT');
    //   actions.setNavigationVisibility(true);
    // }
  };
  return (
    <>
      <ScreenOrientation
        orientation={orientation}
        onDeviceChange={(orientation) => {
          orientation !== 'PORTRAIT'
            ? setOrientation('LANDSCAPE')
            : setOrientation('LANDSCAPE');
            // : setOrientation('PORTRAIT');
          videoSizeChange();
        }}
      />
      <VideoPlayer
        source={{uri: uri, type: 'm3u8'}}
        navigator={false}
        onEnterFullscreen={() => full(true)}
        onExitFullscreen={() => full(false)}
      />
    </>
  );
};

export default Player;
