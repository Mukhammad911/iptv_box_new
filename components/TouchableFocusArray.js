import React, { useState, useEffect, useRef } from 'react'
import { findNodeHandle, TouchableOpacity } from 'react-native'

const TouchableFocusArray = ({
    children,
    index = 0,
    style = {},
    focusedStyle = {},
    nextFocusLeft = null,
    nextFocusUp = null,
    nextFocusRight = null,
    nextFocusDown = null,
    nextFocusLeftSelf = false,
    nextFocusUpSelf = false,
    nextFocusRightSelf = false,
    nextFocusDownSelf = false,
    onPress = () => null,
    refs = [],
    setRefs = () => null,
    readyToGo = true,
    focusedOnStart = false
}) => {

    const ref = useRef(null);

    const [focused, setFocused] = useState(false);

    const checkRefs = (refs) => {
        for (let i in refs) {
            if (refs[i].id == index) {
                return true
            }
        }
        return false
    }

    useEffect(() => {
        if (!checkRefs(refs)) {
            setRefs([
                ...refs,
                {
                    id: index,
                    ref: ref.current
                }
            ])
        }
    }, [refs])

    return (
        <TouchableOpacity
            hasTVPreferredFocus={focusedOnStart}
            ref={e => ref.current = e}
            style={focused ? { ...style, ...focusedStyle } : { ...style }}
            onFocus={() => { setFocused(true) }}
            onBlur={() => setFocused(false)}
            nextFocusLeft={nextFocusLeftSelf ? findNodeHandle(ref.current) : (!readyToGo ? null : (nextFocusLeft == null ? null : findNodeHandle(nextFocusLeft)))}
            nextFocusUp={nextFocusUpSelf ? findNodeHandle(ref.current) : (!readyToGo ? null : (nextFocusUp == null ? null : findNodeHandle(nextFocusUp)))}
            nextFocusRight={nextFocusRightSelf ? findNodeHandle(ref.current) : (!readyToGo ? null : (nextFocusRight == null ? null : findNodeHandle(nextFocusRight)))}
            nextFocusDown={nextFocusDownSelf ? findNodeHandle(ref.current) : (!readyToGo ? null : (nextFocusDown == null ? null : findNodeHandle(nextFocusDown)))}
            onPress={onPress}
        >
            {children}
        </TouchableOpacity>
    )
}

export default TouchableFocusArray