import React, { useRef, useState, useEffect } from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'

const FocusableOpacity = ({ 
    children,
    style = {},
    focusedStyle = {},
    onPress,
    // key = "",
    // ref = null,
    focusedOnStart = false,
    setMenuFocused = () => null
}) => {
    const [focused, setFocused] = useState(false)
    const ref = useRef(null)
    useEffect(() => {
        ref.current.setNativeProps({hasTVPreferredFocus: focusedOnStart})
    }, [])
    return (
        <TouchableOpacity
            // hasTVPreferredFocus={focusedOnStart}
            ref={ref}
            onFocus={() => {
                setFocused(true)
                setMenuFocused(true)
                // alert('focused')
            }}
            onBlur={() => {
                setFocused(false)
                setMenuFocused(false)
            }}
            onPress={onPress}
            style={!focused ? { ...style } : { ...style, ...focusedStyle }}
            // key={key}
        >
            {children}
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    focused: {
        transform: [{ scale: 1.5 }]
    }
})

export default FocusableOpacity