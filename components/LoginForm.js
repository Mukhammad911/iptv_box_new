/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
import React, { useState, useEffect } from 'react';
import {
  View,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
  Text,
  DeviceEventEmitter,
} from 'react-native';
import { colors } from '../templates/colors';
import Notify from './Notify';
import { BASE_API_URL, request_post } from '../api/request';
import Loader from './Loader';
import { useDispatch } from 'react-redux';
import * as actions from '../redux/actions';
import AsyncStorage from '@react-native-community/async-storage';
import { fSize, percentWidth, percentHeight } from '../templates/helper';
import Icon from 'react-native-vector-icons/Ionicons';
import { useRef } from 'react';
import { TouchableWithoutFeedback } from 'react-native-gesture-handler';
import Snackbar from 'react-native-snackbar';
import { Button } from 'react-native';
import InternationalInput from './InternationalInput';
import TouchableFocus from './TouchableFocus';

const LoginForm = ({ auth }) => {
  const dispatch = useDispatch();
  const [login, setLogin] = useState('');
  const [password, setPassword] = useState('');
  const [loader, setLoader] = useState(false);

  const [register, setRegister] = useState(false);
  const [gotOTP, setGotOTP] = useState(false);

  const [registerName, setRegisterName] = useState('');
  const [registerPhone, setRegisterPhone] = useState('');
  const [registerPassword, setRegisterPassword] = useState('');
  const [registerConfirmPassword, setRegisterConfirmPassword] = useState('');
  const [OTP, setOTP] = useState('');
  const [prefix, setPrefix] = useState('+992');

  const [resetPassword, setResetPassword] = useState(false);
  const [gotResetCode, setGotResetCode] = useState(false);
  const [confirmPassword, setConfirmPassword] = useState(false);

  const [resetPhone, setResetPhone] = useState('');
  const [resetCode, setResetCode] = useState('');
  const [newResetPassword, setNewResetPassword] = useState('');
  const [newResetConfirmPassword, setNewResetConfirmPassword] = useState('');
  const [resetConfirmKey, setResetConfirmKey] = useState('');

  // -------------------------------------------------------------------------------------------

  let loginRef = useRef(null);
  let passwordRef = useRef(null);

  // -------------------------------------------------------------------------------------------

  let phoneRef = useRef(null);
  let confirmCodeRef = useRef(null);
  let newPasswordRef = useRef(null)
  let confirmNewPasswordRef = useRef(null)

  // -------------------------------------------------------------------------------------------

  let registerCodeConfirmRef = useRef(null)
  let registerNameRef = useRef(null)
  let registerPhoneRef = useRef(null)
  let registerPasswordRef = useRef(null)
  let registerConfirmPasswordRef = useRef(null)

  // -------------------------------------------------------------------------------------------

  const login_btn = async () => {
    if (login.length === 0) {
      Notify('Ошибка!', 'Логин не может быть пустым');
      return;
    }
    if (password.length === 0) {
      Notify('Ошибка!', 'Пароль не может быть пустым');
      return;
    }
    setLoader(true);
    const data = new FormData();
    data.append('phone', prefix + '' + login);
    data.append('password', password);
    // data.append('device', 'Android');
    const response = await request_post('api/login', data);
    setLoader(false);
    if (!response.success) {
      Notify('Ошибка!', 'Неверный логин или пароль');
      dispatch(actions.setAuthState(false));
      return;
    }
    // await AsyncStorage.setItem('sid', response.sid);
    await AsyncStorage.setItem(
      'access_token',
      'Bearer ' + response?.data?.access_token,
    );
    // dispatch(actions.setSid(response.sid));
    dispatch(actions.setAuthState(true));
    DeviceEventEmitter.emit('goBack', {});
  };

  const clearRegistrationFields = () => {
    setRegisterName('');
    setRegisterPhone('');
    setRegisterPassword('');
    setRegisterConfirmPassword('');
  };

  const get_OTP = async () => {
    if (!registerName) {
      Notify('Ошибка!', 'Имя не может быть пустым');
      return;
    } else if (!registerPhone) {
      Notify('Ошибка!', 'Номер телефона не может быть пустым');
      return;
    } else if (!registerPassword) {
      Notify('Ошибка!', 'Пароль не может быть пустым');
      return;
    } else if (!registerConfirmPassword) {
      Notify('Ошибка!', 'Подтверждение пароля не может быть пустым');
      return;
    } else if (registerPassword.length < 8) {
      Notify('Ошибка!', 'Пароль должен состоять из не менее 8 символов');
      return;
    } else if (registerPassword !== registerConfirmPassword) {
      Notify('Ошибка!', 'Пароли не совпадают');
      return;
    }
    try {
      const bodyForm = new FormData();
      bodyForm.append('phone', prefix + '' + registerPhone);
      setLoader(true);
      const response = await request_post('api/register', bodyForm);
      // const response = await fetch('https://api.somon.tv/api/register', {body: bodyForm, method: 'POST'})
      setLoader(false);
      if (response == 'Error: Request failed with status code 422') {
        Notify('Ошибка!', 'Пользователь с таким номером уже существует');
        return;
      }
      setGotOTP(true);
    } catch (error) {
      Notify('Ошибка!', 'Что-то пошло не так, попробуйте снова');
    }
  };

  const verifyRegistration = async () => {
    if (OTP.length != 4) {
      Notify('Ошибка!', 'Код должен состоять из 4 символов');
      return;
    }
    clearRegistrationFields();
    try {
      const bodyForm = new FormData();
      bodyForm.append('code', OTP);
      bodyForm.append('name', registerName);
      bodyForm.append('phone', prefix + '' + registerPhone);
      bodyForm.append('password', registerPassword);
      bodyForm.append('password_confirmation', registerConfirmPassword);
      setLoader(true);
      const response = await request_post('api/auth-verify', bodyForm);
      // const response = await fetch('https://api.somon.tv/api/auth-verify', {body: bodyForm, method: 'POST'})
      setLoader(false);
      if (response == 'Error: Request failed with status code 500') {
        Notify('Ошибка!', 'Неверный код');
        return;
      }

      if (response.success) {
        await AsyncStorage.setItem(
          'access_token',
          'Bearer ' + response?.data?.access_token,
        );
        dispatch(actions.setAuthState(true));
        DeviceEventEmitter.emit('goBack', {});
        return;
      }

      Notify('Упс...', 'Что-то пошло не так');
    } catch (error) {
      Notify('Ошибка!', 'Что-то пошло не так, попробуйте снова');
    }
  };

  const getResetCode = async () => {
    try {
      if (resetPhone.length < 9) {
        Snackbar.show({
          text: 'Номер телефона должен состоять из 9 цифр',
          backgroundColor: '#F44336',
          duration: Snackbar.LENGTH_LONG,
        });
        return;
      }
      const bodyForm = new FormData();
      bodyForm.append('phone', prefix + '' + resetPhone);
      const response = await fetch(`${BASE_API_URL}api/reset_password`, {
        method: 'POST',
        body: bodyForm,
        headers: {
          'Content-Type': 'multipart/form-data',
          Accept: 'application/json',
        },
      });
      const responseJSON = await response.json();
      if (!responseJSON.success && response.status == 203) {
        Snackbar.show({
          text: responseJSON.message,
          backgroundColor: '#F44336',
          duration: Snackbar.LENGTH_LONG,
        });
        return;
      }
      if (responseJSON.success) {
        Snackbar.show({
          text: responseJSON.message,
          backgroundColor: '#4CAF50',
          duration: Snackbar.LENGTH_LONG,
        });
        setGotResetCode(true);
        return;
      }
      Snackbar.show({
        text: responseJSON['0']?.phone[0],
        backgroundColor: '#F44336',
        duration: Snackbar.LENGTH_LONG,
      });
    } catch (error) {
    }
  };

  const verifyResetCode = async () => {
    try {
      if (resetCode.length < 4) {
        Snackbar.show({
          text: 'Код должен состоять из 4 цифр',
          backgroundColor: '#F44336',
          duration: Snackbar.LENGTH_LONG,
        });
        return;
      }
      const bodyForm = new FormData();
      bodyForm.append('phone', prefix + '' + resetPhone);
      bodyForm.append('code', resetCode);
      const response = await fetch(
        `${BASE_API_URL}api/change_password/code_verification`,
        {
          method: 'POST',
          body: bodyForm,
          headers: {
            Accept: 'application/json',
            'Content-Type': 'multipart/form-data',
          },
        },
      );
      const responseJSON = await response.json();
      if (response.ok && responseJSON.success) {
        Snackbar.show({
          text: responseJSON.message,
          backgroundColor: '#4CAF50',
          duration: Snackbar.LENGTH_LONG,
        });
        setResetConfirmKey(responseJSON?.key);
        setConfirmPassword(true);
        return;
      }
      Snackbar.show({
        text: responseJSON[0]?.phone[0] || responseJSON?.message,
        backgroundColor: '#F44336',
        duration: Snackbar.LENGTH_LONG,
      });
    } catch (error) {
    }
  };

  const setNewPassword = async () => {
    try {
      if (newResetPassword.length < 8) {
        Snackbar.show({
          text: 'Пароль должен состоять минимум из 8 символов',
          backgroundColor: '#F44336',
          duration: Snackbar.LENGTH_LONG,
        });
        return;
      }
      if (newResetPassword != newResetConfirmPassword) {
        Snackbar.show({
          text: 'Введённые пароли не совпадают',
          backgroundColor: '#F44336',
          duration: Snackbar.LENGTH_LONG,
        });
        return;
      }
      const bodyForm = new FormData();
      bodyForm.append('phone', prefix + '' + resetPhone);
      bodyForm.append('key', resetConfirmKey);
      bodyForm.append('password', newResetPassword);
      bodyForm.append('password_confirmation', newResetConfirmPassword);
      const response = await fetch(`${BASE_API_URL}api/change_password`, {
        method: 'POST',
        body: bodyForm,
        headers: {
          Accept: 'application/json',
          'Content-Type': 'multipart/form-data',
        },
      });
      const responseJSON = await response.json();
      if (response.ok && responseJSON.success) {
        Snackbar.show({
          text: 'Вы успешно сменили пароль!',
          backgroundColor: '#4CAF50',
          duration: Snackbar.LENGTH_LONG,
        });
        await AsyncStorage.setItem(
          'access_token',
          'Bearer ' + responseJSON.data.access_token,
        );
        dispatch(actions.setAuthState(true));
        return;
      }
      Snackbar.show({
        text: 'Упс! Что-то пошло не так...',
        backgroundColor: '#F44336',
        duration: Snackbar.LENGTH_LONG,
      });
    } catch (error) {
    }
  };

  return (
    <View style={styles.container}>
      {loader ? <Loader /> : null}
      <View style={styles.LogoContainer}>
        <View style={{ height: percentHeight(20), alignItems: 'center' }}>
          <Image style={styles.image} source={require('../img/logo_new.png')} />
        </View>
      </View>
      <View>
        {!register ? (
          resetPassword ? (
            gotResetCode ? (
              confirmPassword ? (
                <View
                  style={{
                    height: percentHeight(50),
                    alignItems: 'center',
                    ...styles.LoginForm,
                  }}>
                  <Text
                    style={{
                      color: colors.secondary,
                      textAlign: 'center',
                      fontSize: fSize(13),
                      // backgroundColor: 'red',
                      position: 'relative',
                      top: -percentHeight(2),
                    }}>
                    Сброс пароля
                  </Text>
                  <TouchableFocus
                    focusedStyle={{
                      transform: [{ scale: 1.05 }],
                    }}
                    onPress={() => {
                      newPasswordRef.current.focus()
                    }}
                    focusedOnStart
                    nextFocusLeftSelf
                    nextFocusUpSelf
                    nextFocusRightSelf
                  >
                    <TextInput
                      ref={e => newPasswordRef.current = e}
                      value={newResetPassword}
                      onChangeText={(text) => {
                        setNewResetPassword(text);
                      }}
                      placeholder={'Новый пароль'}
                      style={styles.input}
                      placeholderTextColor={colors.secondary}
                      keyboardType="default"
                    />
                  </TouchableFocus>
                  <TouchableFocus
                    focusedStyle={{
                      transform: [{ scale: 1.05 }],
                    }}
                    onPress={() => {
                      confirmNewPasswordRef.current.focus()
                    }}
                    nextFocusLeftSelf
                    nextFocusRightSelf
                  >
                    <TextInput
                      ref={e => confirmNewPasswordRef.current = e}
                      value={newResetConfirmPassword}
                      onChangeText={(text) => {
                        setNewResetConfirmPassword(text);
                      }}
                      placeholder={'Повторите пароль'}
                      style={styles.input}
                      placeholderTextColor={colors.secondary}
                      keyboardType="default"
                    />
                  </TouchableFocus>
                  <TouchableFocus
                    focusedStyle={{
                      transform: [{ scale: 1.05 }],
                    }}
                    onPress={setNewPassword}
                    nextFocusLeftSelf
                    nextFocusRightSelf
                  >
                    <View style={styles.button_container}>
                      <TouchableOpacity
                        activeOpacity={0.95}
                        style={styles.button}
                      >
                        <Text style={styles.text}>Сменить пароль</Text>
                      </TouchableOpacity>
                    </View>
                  </TouchableFocus>
                  <TouchableFocus
                    style={{
                      // backgroundColor: colors.blue,
                      width: percentWidth(40),
                      height: percentHeight(4),
                      justifyContent: 'center',
                      marginTop: percentHeight(2),
                    }}
                    focusedStyle={{
                      transform: [{ scale: 1.1 }]
                    }}
                    onPress={() => {
                      setResetPassword(false);
                      setGotResetCode(false);
                      setConfirmPassword(false);
                      setResetPhone(false);
                      setResetCode(false);
                      setNewResetPassword(false);
                      setNewResetConfirmPassword(false);
                      setResetConfirmKey(false);
                    }}
                    nextFocusLeftSelf
                    nextFocusRightSelf
                    nextFocusDownSelf
                  >
                    <Text
                      style={{
                        color: colors.dark_text,
                        fontSize: fSize(7),
                        textAlign: 'center',
                        textDecorationColor: colors.dark_text,
                        textDecorationLine: 'underline',
                      }}>
                      Я вспомнил пароль!
                    </Text>
                  </TouchableFocus>
                </View>
              ) : (
                <View
                  style={{
                    height: percentHeight(50),
                    alignItems: 'center',
                    ...styles.LoginForm,
                  }}>
                  <Text
                    style={{
                      color: colors.secondary,
                      textAlign: 'center',
                      fontSize: fSize(13),
                      // backgroundColor: 'red',
                      position: 'relative',
                      top: -percentHeight(2),
                    }}>
                    Сброс пароля
                  </Text>
                  <TouchableFocus
                    focusedStyle={{
                      transform: [{ scale: 1.05 }],
                    }}
                    focusedOnStart
                    onPress={() => {
                      confirmCodeRef?.current?.focus();
                    }}
                    nextFocusLeftSelf
                    nextFocusUpSelf
                    nextFocusRightSelf
                  >
                    <TextInput
                      ref={(e) => (confirmCodeRef.current = e)}
                      value={resetCode}
                      onChangeText={(text) => {
                        if (text.length < 5) {
                          setResetCode(text);
                        }
                      }}
                      placeholder={'Введите код'}
                      style={styles.input}
                      placeholderTextColor={colors.secondary}
                      keyboardType="numeric"
                    />
                  </TouchableFocus>
                  <TouchableFocus
                    focusedStyle={{
                      transform: [{ scale: 1.05 }],
                    }}
                    onPress={verifyResetCode}
                    nextFocusLeftSelf
                    nextFocusRightSelf
                  >
                    <View style={styles.button_container}>
                      <TouchableOpacity
                        activeOpacity={0.95}
                        style={styles.button}
                      >
                        <Text style={styles.text}>Подтвердить код</Text>
                      </TouchableOpacity>
                    </View>
                  </TouchableFocus>
                  <TouchableFocus
                    style={{
                      // backgroundColor: colors.blue,
                      width: percentWidth(40),
                      height: percentHeight(4),
                      justifyContent: 'center',
                      marginTop: percentHeight(2),
                    }}
                    focusedStyle={{
                      transform: [{ scale: 1.1 }],
                    }}
                    onPress={() => {
                      setResetPassword(false);
                      setGotResetCode(false);
                      setConfirmPassword(false);
                      setResetPhone(false);
                      setResetCode(false);
                      setNewResetPassword(false);
                      setNewResetConfirmPassword(false);
                      setResetConfirmKey(false);
                    }}
                    nextFocusLeftSelf
                    nextFocusRightSelf
                    nextFocusDownSelf
                  >
                    {/* <TouchableOpacity> */}
                    <Text
                      style={{
                        color: colors.dark_text,
                        fontSize: fSize(7.5),
                        textAlign: 'center',
                        textDecorationColor: colors.dark_text,
                        textDecorationLine: 'underline',
                      }}>
                      Я вспомнил пароль!
                    </Text>
                    {/* </TouchableOpacity> */}
                  </TouchableFocus>
                </View>
              )
            ) : (
              <View
                style={{
                  height: percentHeight(50),
                  alignItems: 'center',
                  ...styles.LoginForm,
                }}>
                <Text
                  style={{
                    color: colors.secondary,
                    textAlign: 'center',
                    fontSize: fSize(13),
                    // backgroundColor: 'red',
                    position: 'relative',
                    top: -percentHeight(2),
                  }}>
                  Сброс пароля
                </Text>
                <TouchableFocus
                  focusedOnStart
                  focusedStyle={{
                    transform: [{ scale: 1.05 }],
                  }}
                  onPress={() => {
                    phoneRef.current.focus();
                  }}>
                  <InternationalInput
                    bindRef={phoneRef}
                    inputValue={resetPhone}
                    setInputValue={setResetPhone}
                    setPrefix={setPrefix}
                  />
                </TouchableFocus>
                <TouchableFocus
                  focusedStyle={{
                    transform: [{ scale: 1.05 }],
                  }}
                  onPress={getResetCode}>
                  <View style={styles.button_container}>
                    <TouchableOpacity
                      activeOpacity={0.95}
                      style={styles.button}>
                      <Text style={styles.text}>Отправить код</Text>
                    </TouchableOpacity>
                  </View>
                </TouchableFocus>
                <TouchableFocus
                  style={{
                    // backgroundColor: colors.blue,
                    width: percentWidth(40),
                    height: percentHeight(4),
                    justifyContent: 'center',
                    marginTop: percentHeight(2),
                  }}
                  focusedStyle={{
                    transform: [{ scale: 1.2 }],
                  }}
                  onPress={() => {
                    setResetPassword(false);
                    setGotResetCode(false);
                    setConfirmPassword(false);
                    setResetPhone('');
                    setResetCode('');
                    setNewResetPassword('');
                    setNewResetConfirmPassword('');
                    setResetConfirmKey('');
                  }}>
                  <Text
                    style={{
                      color: colors.dark_text,
                      fontSize: fSize(7),
                      textAlign: 'center',
                      textDecorationColor: colors.dark_text,
                      textDecorationLine: 'underline',
                    }}>
                    Я вспомнил пароль!
                  </Text>
                </TouchableFocus>
              </View>
            )
          ) : (
            <View style={styles.LoginForm}>
              <Text
                style={{
                  color: colors.secondary,
                  textAlign: 'center',
                  fontSize: fSize(13),
                  // backgroundColor: 'red',
                  position: 'relative',
                  top: -percentHeight(2),
                }}>
                Вход
              </Text>
              <TouchableFocus
                focusedStyle={{
                  transform: [{ scale: 1.05 }],
                }}
                focusedOnStart
                onPress={() => {
                  loginRef?.current?.focus();
              
                }}
                nextFocusLeftSelf
                nextFocusUpSelf
                nextFocusRightSelf
              >
                <InternationalInput
                  bindRef={loginRef}
                  inputValue={login}
                  setInputValue={setLogin}
                  setPrefix={setPrefix}
                />
              </TouchableFocus>
              <TouchableFocus
                focusedStyle={{
                  transform: [{ scale: 1.05 }],
                }}
                onPress={() => {
                  passwordRef?.current?.focus();
                }}
                nextFocusLeftSelf
                nextFocusRightSelf
              >
                <TextInput
                  // ref={passwordRef}
                  ref={(e) => (passwordRef.current = e)}
                  value={password}
         
                  onChangeText={(text) => {
                    setPassword(text);
                  }}
                  onSubmitEditing={() => {
                    passwordRef?.current?.blur();
                  }}
                  placeholder={'Пароль'}
                  secureTextEntry={true}
                  style={styles.input}
                  placeholderTextColor={colors.secondary}
                  keyboardType="default"
                  blurOnSubmit={false}
                />
              </TouchableFocus>
              <TouchableFocus
                focusedStyle={{
                  transform: [{ scale: 1.05 }],
                }}
                onPress={login_btn}
                nextFocusLeftSelf
                nextFocusRightSelf
              >
                <View style={styles.button_container}>
                  <TouchableOpacity
                    // ref={buttonRef}
                    // activeOpacity={0.95}
                    style={styles.button}
                    title="Вход"
                    focusedStyle={{}}>
                    <Text style={styles.text}>Вход</Text>
                    {/* <Icon name="log-in-outline" size={18} color="#fff" /> */}
                  </TouchableOpacity>
                </View>
              </TouchableFocus>
              <TouchableFocus
                style={{
                  // backgroundColor: colors.blue,
                  width: percentWidth(30),
                  height: percentHeight(4),
                  justifyContent: 'center',
                  marginTop: percentHeight(2),
                }}
                focusedStyle={{
                  transform: [{ scale: 1.2 }],
                }}
                onPress={() => {
                  setResetPassword(true);
                }}
                nextFocusLeftSelf
                nextFocusRightSelf
              >
                <Text
                  style={{
                    color: colors.dark_text,
                    fontSize: fSize(6),
                    textAlign: 'center',
                    textDecorationColor: colors.dark_text,
                    textDecorationLine: 'underline',
                  }}>
                  Забыли пароль?
                </Text>
              </TouchableFocus>
              <TouchableFocus
                style={{
                  marginTop: percentHeight(3),
                  height: percentHeight(5),
                  justifyContent: 'center',
                }}
                focusedStyle={{
                  transform: [{ scale: 1.2 }],
                }}
                onPress={() => setRegister(true)}
                nextFocusLeftSelf
                nextFocusRightSelf
                nextFocusDownSelf>
                <Text
                  style={{
                    color: colors.secondary,
                    fontSize: fSize(7.5),
                    textAlign: 'center',
                  }}>
                  Зарегистрироваться
                </Text>
              </TouchableFocus>
            </View>
          )
        ) : !gotOTP ? (
          <View
            style={{
              height: percentHeight(50),
              width: '100%',
              alignItems: 'center',
              ...styles.LoginForm,
            }}>
            <Text
              style={{
                color: colors.secondary,
                textAlign: 'center',
                fontSize: fSize(13),
                // backgroundColor: 'red',
                position: 'relative',
                top: -percentHeight(2),
              }}>
              Регистрация
            </Text>
            <TouchableFocus
              focusedStyle={{
                transform: [{ scale: 1.05 }],
              }}
              onPress={() => {
                registerNameRef.current.focus()
              }}
              focusedOnStart
              nextFocusLeftSelf
              nextFocusUpSelf
              nextFocusRightSelf
            >
              <TextInput
                ref={e => registerNameRef.current = e}
                value={registerName}
                onChangeText={(text) => setRegisterName(text)}
                placeholder={'Имя'}
                placeholderTextColor={colors.secondary}
                style={styles.input}
                keyboardType="default"
              />
            </TouchableFocus>
            <TouchableFocus
              focusedStyle={{
                transform: [{ scale: 1.05 }],
              }}
              onPress={() => {
                registerPhoneRef.current.focus()
              }}
              nextFocusLeftSelf
              nextFocusRightSelf
            >
              <InternationalInput
                bindRef={registerPhoneRef}
                inputValue={registerPhone}
                setInputValue={setRegisterPhone}
              />
            </TouchableFocus>
            <TouchableFocus
              focusedStyle={{
                transform: [{ scale: 1.05 }],
              }}
              onPress={() => {
                registerPasswordRef.current.focus()
              }}
              nextFocusLeftSelf
              nextFocusRightSelf
            >
              <TextInput
                ref={e => registerPasswordRef.current = e}
                value={registerPassword}
                onChangeText={(text) => setRegisterPassword(text)}
                placeholder={'Пароль'}
                secureTextEntry={true}
                style={styles.input}
                placeholderTextColor={colors.secondary}
                keyboardType="default"
              />
            </TouchableFocus>
            <TouchableFocus
              focusedStyle={{
                transform: [{ scale: 1.05 }],
              }}
              onPress={() => {
                registerConfirmPasswordRef.current.focus();
              }}
              nextFocusLeftSelf
              nextFocusRightSelf
            >
              <TextInput
                ref={e => registerConfirmPasswordRef.current = e}
                value={registerConfirmPassword}
                onChangeText={(text) => setRegisterConfirmPassword(text)}
                placeholder={'Подтвердите пароль'}
                secureTextEntry={true}
                style={styles.input}
                placeholderTextColor={colors.secondary}
                keyboardType="default"
              />
            </TouchableFocus>
            <TouchableFocus
              focusedStyle={{
                transform: [{ scale: 1.05 }],
              }}
              onPress={get_OTP}
              nextFocusLeftSelf
              nextFocusRightSelf
            >
              <View style={styles.button_container}>
                <TouchableOpacity
                  activeOpacity={0.95}
                  style={styles.button}
                >
                  <Text style={styles.text}>Зарегистрироваться</Text>
                </TouchableOpacity>
              </View>
            </TouchableFocus>
            <TouchableFocus
              style={{
                marginTop: percentHeight(3),
                height: percentHeight(5),
                justifyContent: 'center',
              }}
              focusedStyle={{
                transform: [{ scale: 1.1 }],
              }}
              onPress={() => {
                setRegister(true);
                clearRegistrationFields();
                setRegister(false);
                setGotOTP(false);
              }}
              nextFocusLeftSelf
              nextFocusRightSelf
              nextFocusDownSelf
            >
              <Text
                style={{
                  color: colors.secondary,
                  fontSize: fSize(7),
                  textAlign: 'center',
                }}>
                Уже есть аккаунт?
              </Text>
            </TouchableFocus>
          </View>
        ) : (
          <View style={{ height: percentHeight(20), ...styles.LoginForm }}>
            <Text
              style={{
                color: colors.secondary,
                textAlign: 'center',
                fontSize: fSize(13),
                // backgroundColor: 'red',
                position: 'relative',
                top: -percentHeight(2),
              }}>
              Регистрация
            </Text>
            <TouchableFocus
              focusedOnStart
              focusedStyle={{
                transform: [{ scale: 1.05 }],
              }}
              onPress={() => {
                registerCodeConfirmRef.current.focus()
              }}
              nextFocusLeftSelf
              nextFocusUpSelf
              nextFocusRightSelf
            >
              <TextInput
                ref={e => registerCodeConfirmRef.current = e}
                value={OTP}
                onChangeText={(text) => {
                  if (text.toString().length < 5) {
                    setOTP(text);
                  }
                }}
                placeholder={'Введите код'}
                placeholderTextColor={colors.secondary}
                style={styles.input}
                keyboardType="numeric"
              />
            </TouchableFocus>
            <TouchableFocus
              focusedStyle={{
                transform: [{ scale: 1.05 }],
              }}
              onPress={verifyRegistration}
              nextFocusLeftSelf
              nextFocusRightSelf
            >
              <View style={styles.button_container}>
                <TouchableOpacity
                  activeOpacity={0.95}
                  style={styles.button}
                >
                  <Text style={styles.text}>Подтвердить</Text>
                </TouchableOpacity>
              </View>
            </TouchableFocus>
            <TouchableFocus
              style={{
                marginTop: percentHeight(3),
                height: percentHeight(5),
                justifyContent: 'center',
              }}
              focusedStyle={{
                transform: [{ scale: 1.1 }]
              }}
              onPress={() => {
                setRegister(true);
                clearRegistrationFields();
                setRegister(false);
                setGotOTP(false);
              }}
              nextFocusLeftSelf
              nextFocusRightSelf
              nextFocusDownSelf
            >
              <Text
                style={{
                  color: colors.secondary,
                  fontSize: fSize(7),
                  textAlign: 'center',
                }}>
                Уже есть аккаунт?
              </Text>
            </TouchableFocus>
          </View>
        )}
      </View>
    </View>
  );
};
export default LoginForm;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    backgroundColor: colors.primary,
    flexDirection: 'row',
  },
  container2: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: colors.primary,
    paddingTop: percentHeight(10),
  },
  image: {
    width: percentWidth(20),
    height: percentHeight(10),
    resizeMode: 'contain',
  },
  LogoContainer: {
    flex: 1,
    width: percentWidth(55),
    justifyContent: 'center',
    alignItems: 'center',
  },
  LoginForm: {
    flex: 1,
    width: percentWidth(45),
    backgroundColor: '#191723',
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    width: percentWidth(30),
    height: percentHeight(8),
    padding: 10,
    color: colors.secondary,
    marginBottom: 10,
    borderWidth: 2,
    borderColor: '#36324A',
    borderRadius: 4,
  },
  button_container: {
    width: percentWidth(30),
    height: percentHeight(7),
    borderRadius: 4,
    borderColor: '#AA0008',
    borderWidth: 2,
    backgroundColor: '#AA0008',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    width: '100%',
    // height: '100%',
    // backgroundColor: 'blue'
  },
  text: {
    color: '#fff',
    fontSize: fSize(6),
    paddingBottom: 2,
  },
});

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     alignItems: 'center',
//     justifyContent: 'space-between',
//     backgroundColor: colors.primary,
//     flexDirection: 'row'
//   },
//   text: {
//     color: '#fff',
//     fontSize: fSize(6),
//     paddingBottom: 2,
//   },
//   image: {
//     width: percentWidth(20),
//     height: percentHeight(10),
//     resizeMode: 'contain',
//   },
//   LogoContainer: {
//     flex: 1,
//     width: percentWidth(55),
//     justifyContent: 'center',
//     alignItems: 'center'
//   },
//   LoginForm: {
//     flex: 1,
//     width: percentWidth(45),
//     backgroundColor: '#191723',
//     justifyContent: 'center',
//     alignItems: 'center'
//   },
//   input: {
//     width: percentWidth(30),
//     height: percentHeight(8),
//     padding: 10,
//     color: colors.secondary,
//     marginBottom: 10,
//     borderWidth: 2,
//     borderColor: '#36324A',
//     borderRadius: 4,
//   },
//   button_container: {
//     width: percentWidth(30),
//     height: percentHeight(7),
//     borderRadius: 4,
//     borderColor: '#AA0008',
//     borderWidth: 2,
//     backgroundColor: '#AA0008',
//     justifyContent: 'center',
//     alignItems: 'center'
//   },
//   button: {
//     flex: 1,
//     justifyContent: 'center',
//     alignItems: 'center',
//     flexDirection: 'row',
//     width: '100%',
//     // height: '100%',
//     // backgroundColor: 'blue'
//   },
// });
