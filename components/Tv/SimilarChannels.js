import React, { useEffect, useState } from 'react';
import { request_get } from '../../api/request';
import { View, StyleSheet, FlatList, Image, TouchableOpacity } from 'react-native';
import { fSize, percentHeight, percentWidth } from '../../templates/helper';
import Favorite from '../General/Favorite';
import { colors } from '../../templates/colors';
import { IPTV_ADMIN_URL } from '../../api/url';
import Loader from '../Loader';
import { Text } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/Ionicons';
import FocusableOpacity from '../FocusableOpacity';
import TouchableFocus from '../TouchableFocus';
import TouchableFocusArray from '../TouchableFocusArray';

const SimilarChannels = ({ group_channel_id, setId, id, setShowSimilarChannels, showControlsActivatorRef }) => {
    const [channels, setChannels] = useState([])
    const [loader, setLoader] = useState(true)

    const [closeSimilarChannelsButtonRef, setCloseSimilarChannelsButtonRef] = useState(null)

    const [channelsRefs, setChannelsRefs] = useState([])

    const findRefFromIndex = (arr, index) => {
        for (let i in arr) {
            if (arr[i].id == index) {
                return arr[i].ref;
            }
        }
        return null
    }

    useEffect(() => {
        getChannels()
    }, [])

    useEffect(() => {
        for (let i of channelsRefs) {
        }
    }, [channelsRefs])

    const updateRefsArray = (ref, index) => {
        setChannelsRefs({
            ref: ref,
            id: index
        })
    }

    const getChannels = async () => {
        const response = await request_get(`getChannels/${group_channel_id}`)
        if (!response.success) {
            return false
        }
        setChannels(response.data)
        // let arr = []
        // for (let i = 0; i < response.data; i++) {
        //     arr.push(null)
        // }
        // setChannelsRefs(arr)
        setLoader(false)
    }
    const channel = (item, index) => {
        return (
            <TouchableFocus
                // index={index}
                style={{
                    // backgroundColor: 'pink',
                    // borderWidth: 1,
                    // borderColor: 'white',
                    marginTop: percentWidth(2),
                    marginLeft: percentWidth(1),
                    marginRight: percentWidth(1),
                    borderWidth: 3,
                    borderColor: colors.primary
                }}
                focusedStyle={{
                    // transform: [{ scale: 1.2 }]
                    borderColor: colors.dark_red,
                    borderStyle: 'solid',
                    borderRadius: 10
                    // backgroundColor: colors.dark_red
                }}
                // setRefs={setChannelsRefs}
                setRef={(ref) => {
                    channelsRefs.push({ ref: ref, id: index })
                    // setChannelsRefs([...channelsRefs, { ref: ref, id: index }]);
                }}
                onPress={() => {
                    setId(item.id)
                    // findRefFromIndex(channelsRefs, index + 2).setNativeProps({
                    //     hasTVPreferredFocus: true
                    // })
                }}
                focusedOnStart={!index}
                nextFocusLeftSelf={!(index % 2)}
                nextFocusRightSelf={(index % 2)}
                nextFocusRight={(!(index % 2) ? findRefFromIndex(channelsRefs, index + 1) : null)}
                nextFocusLeft={((index % 2) ? findRefFromIndex(channelsRefs, index - 1) : null)}
                nextFocusUp={((index == 0 || index == 1) ? closeSimilarChannelsButtonRef : null)}
                nextFocusDownSelf={(((index == (channels.length - 1)) || (index == (channels.length - 2))))}
            >
                <View style={styles.card} >
                    <Image
                        style={{ height: percentHeight(13), width: percentWidth(13) }}
                        source={{ uri: `${IPTV_ADMIN_URL}images/channel/${item.image}` }}
                        resizeMode={'center'}
                    />
                </View>
                <View>
                    <Text
                        style={{
                            color: colors.secondary,
                            fontSize: fSize(5),
                        }}
                    >
                        {item.name + index}
                    </Text>
                    <Text
                        style={{
                            color: colors.dark_text,
                            fontSize: fSize(4)
                        }}
                    >
                        {item.program_name}
                    </Text>
                    <Text
                        style={{
                            color: colors.dark_text,
                            fontSize: fSize(4)
                        }}
                    >
                        {item.program_time}
                    </Text>
                </View>
            </TouchableFocus>
        )
    }

    return (

        <View style={loader ? styles.container : { ...styles.container, ...styles.container_background }}>
            {loader ?
                <Loader size={'small'} /> :
                <ScrollView style={{ flexDirection: "row", paddingHorizontal: percentWidth(1), flexWrap: 'wrap' }}>
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'space-between',
                            paddingTop: percentHeight(3)
                        }}
                    >
                        <View>
                            <Text
                                style={{
                                    color: colors.secondary,
                                    fontSize: fSize(7.5),
                                    marginLeft: percentWidth(1),
                                }}
                            >
                                Другие телеканалы
                            </Text>
                        </View>
                        <TouchableFocus
                            onPress={() => {
                                setShowSimilarChannels(false);
                                if (showControlsActivatorRef) {
                                    showControlsActivatorRef.setNativeProps({
                                        hasTVPreferredFocus: true
                                    })
                                }
                            }}
                            style={{
                                height: percentHeight(6),
                                width: percentHeight(6),
                                marginRight: percentWidth(1),
                                borderRadius: 40,
                                justifyContent: 'center',
                                alignItems: 'center'
                            }}
                            focusedStyle={{
                                // transform: [{ scale: 1.3 }]
                                backgroundColor: colors.dark_red,
                            }}
                            setRef={setCloseSimilarChannelsButtonRef}
                            nextFocusLeftSelf
                            nextFocusUpSelf
                            nextFocusRightSelf
                            focusedOnStart={!channels.length}
                        >
                            <Icon name="close-outline" size={fSize(9)} color={colors.dark_text} />
                        </TouchableFocus>
                    </View>
                    <FlatList
                        data={channels}
                        numColumns={2}
                        contentContainerStyle={{
                            width: '100%',
                            paddingBottom: percentHeight(4),
                        }}
                        renderItem={({ item, index }) => channel(item, index)}
                        keyExtractor={(item) => item.id.toString()}
                        // horizontal={true}
                        showsVerticalScrollIndicator={true}
                        showsHorizontalScrollIndicator={false}
                    />
                </ScrollView>
            }

        </View>
    )

}
export default SimilarChannels

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        right: 0,
        top: 0,
        height: '100%',
        width: percentWidth(32),
    },
    container_background: {
        backgroundColor: colors.primary
    },
    favorite: {
        justifyContent: "center",
        alignItems: "center",
        width: percentWidth(7),
        borderRightWidth: 1,
        borderRightColor: "#1B1827"
    },
    card: {
        width: percentWidth(13),
        height: percentHeight(13),
        backgroundColor: "#2C2A3C",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 5,
        overflow: 'hidden'
    }
})
