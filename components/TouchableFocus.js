import React, { useEffect, useRef, useState } from 'react'
import { findNodeHandle, TouchableOpacity } from 'react-native'

const TouchableFocus = ({
    children,
    style = {},
    focusedStyle = {},
    readyToGo = true,
    nextFocusLeft = null,
    nextFocusUp = null,
    nextFocusRight = null,
    nextFocusDown = null,
    nextFocusLeftSelf = false,
    nextFocusUpSelf = false,
    nextFocusRightSelf = false,
    nextFocusDownSelf = false,
    focusedOnStart = false,
    setRef = () => null,
    onPress = () => null,
}) => {

    const [focused, setFocused] = useState(false)

    const ref = useRef(null)

    useEffect(() => {
        setRef(ref.current)
    }, [])

    return (
        <TouchableOpacity
            hasTVPreferredFocus={focusedOnStart}
            ref={e => ref.current = e}
            onFocus={() => setFocused(true)}
            onBlur={() => setFocused(false)}
            style={focused ? {...style, ...focusedStyle} : {...style}}
            nextFocusLeft={nextFocusLeftSelf ? findNodeHandle(ref.current) : (!readyToGo ? null : (nextFocusLeft == null ? null : findNodeHandle(nextFocusLeft)))}
            nextFocusUp={nextFocusUpSelf ? findNodeHandle(ref.current) : (!readyToGo ? null : (nextFocusUp == null ? null : findNodeHandle(nextFocusUp)))}
            nextFocusRight={nextFocusRightSelf ? findNodeHandle(ref.current) : (!readyToGo ? null : (nextFocusRight == null ? null : findNodeHandle(nextFocusRight)))}
            nextFocusDown={nextFocusDownSelf ? findNodeHandle(ref.current) : (!readyToGo ? null : (nextFocusDown == null ? null : findNodeHandle(nextFocusDown)))}
            onPress={(e) => {
                if (e.eventKeyAction == 1) {
                    onPress()
                }
            }}
        >
            {children}
        </TouchableOpacity>
    )
}

export default TouchableFocus