import React from 'react'
import { Text, View } from 'react-native'
import { colors } from '../templates/colors'
import { fSize, percentHeight, percentWidth } from '../templates/helper'

const ScreenHeader = ({screenName}) => {
    return (
        <View
            style={{
                paddingTop: percentHeight(5),
                paddingLeft: percentWidth(2),
                paddingBottom: percentHeight(3)
            }}
        >
            <Text
                style={{
                    color: colors.secondary,
                    fontSize: fSize(12)
                }}
            >
                {screenName}
            </Text>
        </View>
    )
}

export default ScreenHeader