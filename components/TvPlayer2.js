import {
  BackHandler,
  StyleSheet,
  TouchableOpacity,
  View,
  StatusBar,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import React, { Fragment, useCallback, useEffect, useRef, useState } from 'react';
import { bindActionCreators } from 'redux';
import * as actions from '../redux/actions';
import { header } from './helper';
import { useDispatch, useSelector } from 'react-redux';
import { colors } from '../templates/colors';
import { fSize, percentHeight, percentWidth } from '../templates/helper';
import Loader from './Loader';
import { Button, TVEventHandler } from 'react-native';
import { Text, Image } from 'react-native';
import { VLCPlayer } from '@nghinv/react-native-vlc';
import { useFocusEffect } from '@react-navigation/native';
import Favorite from './General/Favorite';
import TouchableFocus from './TouchableFocus';
import * as Animatable from 'react-native-animatable';
import mainLogo from '../img/ic_launcher_round_new.png'
import Video from 'react-native-video';
import WebView from 'react-native-webview';

// let timeOutID;

const TvPlayer = ({
  navigation = null,
  autoShowHeader = false,
  back = null,
  uri = null,
  onFull = null,
  onPlay,
  autoPlay = true,
  isTv = false,
  vidid,
  vidName = '',
  setShowPlayer,
}) => {
  const [rUri, setRUri] = useState(null);
  const [showControlPanel, setShowControlPanel] = useState(false)
  const dispatch = useDispatch();
  const [visibleStatusBar, setVisibleStatusBar] = useState(false);
  const [play, setPlay] = useState(autoPlay);
  const [loader, setLoader] = useState(false);
  const [currentTime, setCurrentTime] = useState(-1);
  let playerRef = useRef(null);
  const sid = useSelector((state) => state.authReducer.sid);
  const [showLeftSkipButton, setShowLeftSkipButton] = useState(false);
  const [showRightSkipButton, setShowRightSkipButton] = useState(false);
  const [duration, setDuration] = useState(0);
  const [playerWidth, setPlayerWidth] = useState(0);
  const [showControls, setShowControls] = useState(false);
  // const [showPlayBtn, setShowPlayBtn] = useState(false);
  // let controlTimerId;

  const getCurrentTimePercentage = () => {
    if (currentTime > 0) {
      return parseFloat(currentTime) / parseFloat(duration);
    }
    return 0;
  };

  const [showControlsActivatorRef, setShowControlsActivatorRef] = useState(null);

  const [closePlayerButtonRef, setClosePlayerButtonRef] = useState(null);

  const [playerSkipBackButtonRef, setPlayerSkipBackButtonRef] = useState(null);
  const [playerFavoriteButtonRef, setPlayerFavoriteButtonRef] = useState(null);
  const [playerPlayBackButtonRef, setPlayerPlayBackButtonRef] = useState(null);
  const [playerPlayButtonRef, setPlayerPlayButtonRef] = useState(null);
  const [playerPlayForwardButtonRef, setPlayerPlayForwardRef] = useState(null);
  const [playerEPGMenuButtonRef, setPlayerEPGMenuButtonRef] = useState(null);
  // const [rewindCounter, setRewindCounter] = useState(0);

  let controlsTimer = useRef(null);
  const _tvEventHandler = new TVEventHandler();

  useEffect(() => {
    return () => {
      playerRef.current = null;
      clearTimeout(controlsTimer.current);
      clearTimeout(rewindTimeKeeper.current);
      controlsTimer.current = null;
      rewindTimeKeeper.current = null;
    };
  }, []);

  let [rewindCounter, setRewindCounter] = useState(0);
  let rewindCounter2 = useRef(0);
  let rewindTimeKeeper = useRef(null);
  const [showRewinder, setShowRewinder] = useState(null);

  const rewindPlayer = (delay) => {
    // console.log(rewindTimeKeeper);
    console.log(rewindCounter2.current);
    if (rewindTimeKeeper.current) {
      clearTimeout(rewindTimeKeeper.current);
    }
    // setRewindCounter(Number(+rewindCounter2.current) + Number(+delay));
    rewindCounter2.current += Number(+delay);
    rewindTimeKeeper.current = setTimeout(() => {
      console.log(rewindCounter2.current);
      playerRef.current.seek(Number((currentTime - currentTime % 1) + +rewindCounter2.current + +delay));
      // console.log('worked on', rewindCounter);
      // setRewindCounter(0);
      rewindCounter2.current = 0;
      rewindTimeKeeper.current = null;
    }, 3000)
  }

  useEffect(() => {
    if (duration) {

      let message = JSON.stringify({
        'type': 'playPause',
        'payload': {
          'shouldPlay': 'true',
        }
      });

      webViewRef.current?.postMessage(message);
    }
  }, [duration]);

  useFocusEffect(
    useCallback(() => {
      return () => {
        setRUri(null);
      };
    }, []),
  );
  useEffect(() => {
    if (uri) {
      setRUri(uri);
    }
  }, [uri]);

  let webViewRef = useRef(null);

  useEffect(() => {
    clearTimeout(controlsTimer.current);
    controlsTimer.current = setTimeout(() => {
      setShowControlPanel(false);
    }, 5000)
  }, [showControlPanel])

  useEffect(() => {
    console.log('controls timer changed to: ' + controlsTimer.current);
  }, [controlsTimer.current])

  // const _enableTVEventHandler = () => {
  //   _tvEventHandler.enable(this, function (cmp, evt) {
  //     console.log(controlsTimer.current);
  //     if (showControls && evt.eventKeyAction == 1) {
  //       clearTimeout(controlsTimer.current);
  //       controlsTimer.current = setTimeout(() => {
  //         setShowControls(false);
  //       }, 5000);
  //     }
  //     console.log('show constrols from TVEvent', showControls);
  //     if (!showControls) {
  //       if (evt.eventKeyAction == 1) {
  //         if (evt && evt.eventType === 'right') {
  //           if (duration - currentTime > 60) {
  //             console.log('got to right', showControls);
  //             // rewindPlayer(60);
  //           }
  //         } else if (evt && evt.eventType === 'up') {
  //         } else if (evt && evt.eventType === 'left') {
  //           if (currentTime > 60) {
  //             console.log('got to left', showControls);
  //             // rewindPlayer(-60);
  //           }
  //         } else if (evt && evt.eventType === 'down') {
  //         }
  //       }
  //     }
  //   });
  // };

  const _enableTVEventHandler = () => {
    _tvEventHandler.enable(this, (cmp, evt) => {
      if (evt.eventKeyAction == 1) {
        // console.log(controlsTimer.current, showControlPanel);
        if (showControlPanel) {
          clearTimeout(controlsTimer.current);
          controlsTimer.current = setTimeout(() => {
            setShowControlPanel(false);
          }, 5000);
        }
        if (!showControlPanel) {
          if (evt && evt.eventType === 'right') {
            console.log('(duration - currentTime)', duration - currentTime, duration, currentTime);
            if ((duration - currentTime) > 60) {
              console.log('got to right', showControls);
              rewindPlayer(60);
            }
            // setShowControlPanel(true);
          } else if (evt && evt.eventType === 'up') {
            // setShowSwitchChannel(true);
          } else if (evt && evt.eventType === 'left') {
            console.log('currentTime', currentTime);
            if (currentTime > 60) {
              console.log('got to left', showControls);
              rewindPlayer(-60);
            } else {
              rewindPlayer(0);
            }
            // setShowControlPanel(true);
          } else if (evt && evt.eventType === 'down') {
            // setShowSwitchChannel(true);
          }
        }
      }
    });
  };

  const foo = () => alert('foo')

  useEffect(() => {
    setRewindCounter(rewindCounter2.current);
  }, [rewindCounter2.current])

  const _disableTVEventHandler = () => {
    if (_tvEventHandler) {
      _tvEventHandler.disable();
    }
  }

  useEffect(() => {
    _disableTVEventHandler();
    _enableTVEventHandler();
    return () => _disableTVEventHandler();
  }, [showControlPanel, currentTime]);

  const onProgress = ({ currentTime, duration, seekableDuration }) => {
    // setDuration(duration);
    setDuration(seekableDuration);
    setCurrentTime(currentTime);
    // console.log(seekableDuration);
    // console.log(currentTime);
    // console.log(getVideoTimeFormat(currentTime));
  };

  const twodigit = (digit) => (+digit > 9 ? `${digit}` : `0${digit}`);

  const getVideoTimeFormat = (s) => {
    if (s <= 0) {
      return 0;
    }
    let ms = s % 1;
    // s = (s - ms) / 1000;
    s = (s - ms);
    // console.log('s ======== ' + s);
    let secs = s % 60;
    s = (s - secs) / 60;
    let mins = s % 60;
    let hrs = (s - mins) / 60;
    // console.log('secs', secs);
    // console.log('mins', mins);
    // console.log('hrs', hrs);
    if (hrs.toString() == '0') {
      return twodigit(mins) + ':' + twodigit(secs);
    }
    return twodigit(hrs) + ':' + twodigit(mins) + ':' + twodigit(secs);
  };

  if (!uri) {
    return <Loader />;
  }

  // console.log(uri);

  useEffect(() => {
    if (uri.length > 0) {

    }
  }, [uri])

  return (
    <View>
      {
        currentTime < 0 ?
          (
            <View
              style={{
                width: '100%',
                height: '100%',
                flex: 1,
                backgroundColor: '#12101C',
                position: 'absolute',
                zIndex: 1000,
                alignItems: 'center',
                justifyContent: 'center'
              }}
            >
              <Animatable.View
                animation="pulse"
                easing="ease-out"
                style={{
                  width: 100,
                  height: 100,
                  justifyContent: "center",
                  alignItems: 'center'
                }}
                iterationCount="infinite"
              >
                <Image source={mainLogo} resizeMode="center" width={50} height={50} style={{ width: percentHeight(25), height: percentHeight(25) }} />
              </Animatable.View>
              <Text
                style={{
                  position: 'absolute',
                  bottom: percentHeight(3),
                  fontSize: fSize(12),
                  color: colors.secondary,
                }}
              >
                {vidName}
              </Text>
            </View>
          )
          :
          null
      }
      <StatusBar hidden={visibleStatusBar} backgroundColor={colors.primary} />
      <View style={styles.videoFull}>
        <TouchableFocus
          // onPressIn={(event) => {
          //   showStatusBar();
          //   setShowPlayBtn(!showPlayBtn);
          // }}
          onPress={() => {
            setShowControlPanel(true);
          }}
          style={{
            zIndex: -999,
          }}
          focusedOnStart
          setRef={setShowControlsActivatorRef}
          nextFocusLeftSelf
          nextFocusUpSelf
          nextFocusRightSelf
          nextFocusDownSelf
        >
        {/* <VLCPlayer
            ref={playerRef}
            style={{
              height: '100%',
              width: '100%',
              zIndex: 500
            }}
            videoAspectRatio="16:9"
            source={{ uri: rUri }}
            resizeMode="center"
            fullscreen={true}
            paused={!play}
            onProgress={onProgress}
            // onBuffer={(buffer) => console.buffer('buffer:', buffer)}
            onReadyForDisplay={() => setLoader(false)}
            onError={(err) => {
              setLoader(false);
            }}
          /> */}
        <Video
            ref={e => playerRef.current = e}
            resizeMode='cover'
            style={{
              height: '100%',
              width: '100%',
              // height: percentHeight(100),
              // width: percentWidth(100),
              zIndex: 500,
            }}
            // controls={true}
            source={{ uri: (uri ?? "") }}
            // fullscreen={true}
            paused={!play}
            onProgress={onProgress}
            muted={false}
            onReadyForDisplay={() => setLoader(false)}
          />
        {/* {
            uri.length > 0 ? */}
        {/* <View
          style={{
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(255, 255, 255, 0.6)',
            zIndex: 600
          }}
        >
          <WebView
            // injectedJavaScript={
            //   `
            //   window.waitForBridge = function(fn) { return (window.postMessage.length === 1) ? fn() : setTimeout(function() { window.waitForBridge(fn) }, 5) }; window.waitForBridge(function() { window.postMessage(document.getElementById('videoPlayer').innerHTML) });
            //   `
            // let playAttempt = setInterval(() => {
            //   document.getElementById("player").play()
            //     .then(() => {
            //       clearInterval(playAttempt);
            //       alert('worked');
            //     })
            //     .catch(error => {
            //       alert('Unable to play the video, User has not interacted yet.');
            //     });
            // }, 500);
            // }
            ref={e => webViewRef.current = e}
            onLoadEnd={() => {
              webViewRef.current.injectJavaScript(
                `
                (function() {

                  // window.onclick = () => {
                  //   document.getElementById("player").play();
                  // }

                  document.addEventListener('message', (e) => {
                    window.onclick();
                    let message = null;
                    try {
                      message = JSON.parse(e.data);
                    } catch (error) {
                      alert('error');
                      return;
                    }
                    
                    switch (message['type']) {
                      case 'playPause':
                        setTimeout(() => {
                          // alert(typeof message.payload.shouldPlay);
                          // window.onclick();
                          // document.getElementById("player").click();
                          document.getElementById("player").autoplay = false;
                          document.getElementById("player").muted = !(message['payload']['shouldPlay'] == 'true');
                          // message['payload']['shouldPlay'] == 'true' ? document.getElementById("player").play() : document.getElementById("player").pause();
                        }, 5000)
                        break;
                    }

                  })


                  setInterval(() => {
                    let message = JSON.stringify({
                      "type": "playerTimers",
                      "payload": {
                        "currentTime": document.getElementById("player").currentTime,
                        "duration": document.getElementById("player").duration,
                      }
                    })
                    window.ReactNativeWebView.postMessage(message);
                  }, 1000);
                })()
              `
              )
            }}
            // injectedJavaScript={}
            onMessage={(e) => {
              // console.log('Received: ', e);
              webViewRef.current?.click();
              const message = JSON.parse(e.nativeEvent.data);
              switch (message['type']) {
                case 'playerTimers':
                  setCurrentTime(message['payload']['currentTime']);
                  if (duration != message['payload']['duration']) {
                    setDuration(message['payload']['duration']);
                  }
                  break;
                default:
                  break;
              }
            }}
            focusable
            // on
            source={{
              html: `
                <!DOCTYPE html>
                <html lang="en">
                
                <head>
                <meta charset="UTF-8">
                <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Document</title>
                <style>
                * {
                  padding: 0;
                  margin: 0;
                }
                </style>
                </head>
                
                <body>
                <div style="width: 100%; height: 100%;" id="videoPlayer" >
                      <video
                          id="player"
                          controls
                          src="http://flussonic1.somon.tv//media/nfs2/00/00/00/50/73/index.m3u8?token=vod_CGrFxIEdcK4x42o4TJxPHvOcqd1901&path=/media/nfs2/00/00/00/50/73/index.m3u8"
                          style="width: 100%; height: 100%;">
                      </video>
                      </div>
                  </body>
                  
                  </html>
              `
            }}
            allowsFullscreenVideo
            allowsInlineMediaPlayback
            originWhitelist={['*']}
            androidLayerType="hardware"
            useWebKit={true}
            style={{
              width: '100%',
              height: '100%',
              zIndex: 700,
            }}
          />
        </View> */}
        {/* :
              <Text style={{ color: colors.secondary }} >Not found</Text> */}
        {/* } */}
        </TouchableFocus>
        {/* {
          !showRewinder ?
            (
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: percentHeight(100),
                  width: percentHeight(100),
                  borderRadius: 1000,
                  position: 'absolute',
                  zIndex: 700,
                  backgroundColor: 'rgba(255, 255, 255, 0.1)',
                  left: rewindCounter < 0 ? -percentHeight(50) : 'auto',
                  right: rewindCounter > 0 ? -percentHeight(50) : 'auto'
                }}
              >
                <Text
                  style={{
                    color: colors.secondary,
                  }}
                >
                  {
                    rewindCounter2.current
                  }
                </Text>
              </View>
            )
            :
            null
        } */}
      </View>
      {showControlPanel && (duration > 0) ? (
        <TouchableFocus
          style={{
            width: percentHeight(10),
            height: percentHeight(10),
            position: 'absolute',
            top: percentHeight(5),
            left: percentHeight(5),
            borderRadius: 100,
            justifyContent: 'center',
            alignItems: 'center'
          }}
          focusedStyle={{
            backgroundColor: colors.dark_red
          }}
          onPress={setShowPlayer}
          setRef={setClosePlayerButtonRef}
          nextFocusLeftSelf
          nextFocusUpSelf
          nextFocusRightSelf
          nextFocusDown={playerSkipBackButtonRef}
        >
          <Icon name="arrow-back-outline" style={{ color: colors.secondary, fontSize: fSize(10) }} />
        </TouchableFocus>
      )
        :
        null
      }
      {showControlPanel && (duration > 0) ? (
        <View style={styles.mainProgressContainer} >
          <View style={styles.visibleProgressContainer} >
            <View style={styles.trackingControls}>
              <TouchableOpacity
                onLayout={({ nativeEvent }) => {
                  const { layout } = nativeEvent;
                  setPlayerWidth(layout.width);
                }}
                onPress={({ nativeEvent }) => {
                  const { pageX } = nativeEvent;
                  const seek = parseFloat(parseFloat((pageX - percentWidth(10)) / percentWidth(80)) * duration - percentHeight(0.75)) / 1000;
                  playerRef.current.seek(seek);
                  setPlay(true);
                }}>
                <View style={styles.progress}>
                  <View
                    style={[
                      styles.innerProgressCompleted,
                      {
                        flex: getCurrentTimePercentage() * 100,
                      },
                    ]}
                  />
                  <View
                    style={{
                      borderRadius: 15,
                      borderColor: 'white',
                      borderWidth: 1,
                      backgroundColor: colors.dark_red,
                      width: percentHeight(1.5),
                      height: percentHeight(1.5),
                      zIndex: 999,
                      position: 'relative',
                    }} />
                  <View
                    style={[
                      styles.innerProgressRemaining,
                      {
                        flex: (1 - getCurrentTimePercentage()) * 100,
                      },
                    ]}
                  />
                </View>
              </TouchableOpacity>
              <View
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Text
                  style={{
                    color: colors.dark_text,
                    fontSize: fSize(4),
                    paddingLeft: percentWidth(1)
                  }}>
                  {getVideoTimeFormat(currentTime)}
                </Text>

                <Text
                  style={{
                    color: colors.dark_text,
                    fontSize: fSize(4),
                    paddingRight: percentWidth(1),
                  }}>
                  {getVideoTimeFormat(duration)}
                </Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'center',
                paddingLeft: percentWidth(2),
                paddingRight: percentWidth(2),
                paddingTop: percentHeight(1),
                paddingBottom: percentHeight(1)
              }}
            >
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center'
                }}
              >
                <TouchableFocus
                  style={styles.settingButton}
                  focusedStyle={{
                    backgroundColor: colors.dark_red
                  }}
                  onPress={() => {
                    clearTimeout(controlsTimer.current);
                    // setShowControls(false);
                    setShowControlPanel(false);
                    showControlsActivatorRef.setNativeProps({
                      hasTVPreferredFocus: true
                    })
                    // setShowPlayBtn(true);
                  }}
                  setRef={setPlayerSkipBackButtonRef}
                  nextFocusLeftSelf
                  nextFocusDownSelf
                  nextFocusRight={playerFavoriteButtonRef}
                  nextFocusUp={closePlayerButtonRef}
                >
                  <Icon name={'close'} color={colors.secondary} size={fSize(7)} />
                </TouchableFocus>
                <Favorite
                  Check={`favorite/check/${vidid}`}
                  Add={`favorite/${vidid}`}
                  Remove={`favorite/${vidid}`}
                  iconSize={fSize(6)}
                  style={styles.settingButton}
                  focusedStyle={{
                    backgroundColor: colors.dark_red
                  }}
                  setRef={setPlayerFavoriteButtonRef}
                  nextFocusDownSelf
                  nextFocusLeft={playerSkipBackButtonRef}
                  nextFocusRight={playerPlayBackButtonRef}
                  nextFocusUp={closePlayerButtonRef}
                />
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center'
                }}
              >
                <Text style={styles.secondsText} >
                  -10 сек
                </Text>
                <TouchableFocus
                  style={styles.settingButton}
                  focusedStyle={{
                    backgroundColor: colors.dark_red
                  }}
                  onPress={() => {
                    if (currentTime > 10) {
                      // if (rewindTimeKeeper) {
                      //   clearTimeout(rewindTimeKeeper);
                      // }
                      // rewindCounter -= 10;
                      // rewindTimeKeeper = setTimeout(() => {
                      //   playerRef.current.seek((currentTime - currentTime % 1000) / 1000 + +rewindCounter);
                      //   rewindCounter = 0;
                      // }, 1000);
                      rewindPlayer(-10);
                    }
                  }}
                  setRef={setPlayerPlayBackButtonRef}
                  nextFocusDownSelf
                  nextFocusLeft={playerFavoriteButtonRef}
                  nextFocusRight={playerPlayButtonRef}
                  nextFocusUp={closePlayerButtonRef}
                >
                  <Icon name={'play-back-outline'} color={colors.secondary} size={fSize(5)} />
                </TouchableFocus>
                <TouchableFocus
                  style={{ ...styles.settingButton, width: percentHeight(6), height: percentHeight(6) }}
                  focusedStyle={{
                    backgroundColor: colors.dark_red
                  }}
                  onPress={() => {
                    setPlay(!play);
                    // if (isTv) {
                    //   playerRef.current.resume(!play);
                    // }
                    let message = JSON.stringify({
                      'type': 'playPause',
                      'payload': {
                        'shouldPlay': 'true',
                      }
                    });

                    webViewRef.current?.postMessage(message);
                  }}
                  focusedOnStart
                  setRef={setPlayerPlayButtonRef}
                  nextFocusDownSelf
                  nextFocusLeft={playerPlayBackButtonRef}
                  nextFocusRight={playerPlayForwardButtonRef}
                  nextFocusUp={closePlayerButtonRef}
                >
                  {
                    !play ? (
                      <Icon name={'play'} color={colors.secondary} size={fSize(7)} />
                    ) : (
                      <Icon
                        name={'pause'}
                        color={colors.secondary}
                        size={fSize(7)}
                      />
                    )
                  }
                </TouchableFocus>
                <TouchableFocus
                  style={styles.settingButton}
                  focusedStyle={{
                    backgroundColor: colors.dark_red
                  }}
                  onPress={() => rewindPlayer(10)}
                  setRef={setPlayerPlayForwardRef}
                  nextFocusUp={closePlayerButtonRef}
                  nextFocusDownSelf
                  nextFocusLeft={playerPlayButtonRef}
                  nextFocusRight={playerEPGMenuButtonRef}
                >
                  <Icon name={'play-forward-outline'} color={colors.secondary} size={fSize(5)} />
                </TouchableFocus>
                <Text style={styles.secondsText}>
                  +10 сек
                </Text>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center'
                }}
              >
                <TouchableFocus
                  style={styles.settingButton}
                  focusedStyle={{
                    backgroundColor: colors.dark_red
                  }}
                  setRef={setPlayerEPGMenuButtonRef}
                  nextFocusRightSelf
                  nextFocusDownSelf
                  nextFocusLeft={playerPlayForwardButtonRef}
                  nextFocusUp={closePlayerButtonRef}
                >
                  <Icon name={'list-outline'} color={colors.secondary} size={fSize(5)} />
                </TouchableFocus>
              </View>
            </View>
          </View>
        </View>
      ) : null}
    </View>
  );
};
export default TvPlayer;

const styles = StyleSheet.create({
  mainProgressContainer: {
    width: percentWidth(100),
    // backgroundColor: 'green',
    zIndex: 1100,
    position: 'absolute',
    bottom: percentHeight(20),
    alignItems: 'center'
  },
  visibleProgressContainer: {
    backgroundColor: colors.primary,
    width: percentWidth(80),
    paddingBottom: percentHeight(1),
  },
  videoFull: {
    width: '100%',
    height: '100%',
    backgroundColor: '#000',
    // justifyContent: 'center',
    // alignItems: 'center'
  },
  hidden: {
    height: 0,
    opacity: 0,
  },
  fullIcon: {
    position: 'absolute',
    top: StatusBar.currentHeight,
    right: percentHeight(8),
    elevation: 1,
  },
  progress: {
    //   flex: 1,
    flexDirection: 'row',
    borderRadius: 3,
    alignItems: 'center',
    overflow: 'hidden',
    marginTop: -4,
  },
  trackingControls: {
    backgroundColor: 'transparent',
    borderRadius: 5,
    //   position: 'absolute',
    //   bottom: 20,
    //   left: 20,
    //   right: 20,
  },
  innerProgressCompleted: {
    height: percentHeight(0.5),
    backgroundColor: colors.dark_red,
  },
  innerProgressRemaining: {
    height: percentHeight(0.5),
    backgroundColor: '#2C2C2C',
  },
  settingButton: {
    backgroundColor: '#2C293D',
    borderRadius: 500,
    width: percentHeight(5),
    height: percentHeight(5),
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: percentWidth(0.4),
    marginRight: percentWidth(0.4)
  },
  secondsText: {
    color: colors.dark_text,
    fontSize: fSize(4)
  }
});
