import { createStackNavigator } from '@react-navigation/stack';
import React from 'react';
import Replenish from '../components/Replenish';
import ProductsScreen from '../screens/ProductsScreen';
import RadioScreen from '../screens/RadioScreen';
import SubscribesScreen from '../screens/SubscribesScreen';
import HOC from './HOC';

const Stack = createStackNavigator();

const RadioStack = ({ setPath }) => {
  return (
    <Stack.Navigator initialRouteName="radio-list">
      <Stack.Screen name="radio-list" component={HOC(RadioScreen, setPath)} options={{ headerShown: false }} />
      <Stack.Screen name='subscribes' component={SubscribesScreen} />
      <Stack.Screen name='products' component={ProductsScreen} />
      <Stack.Screen name='replenish' component={Replenish} />
    </Stack.Navigator>
  );
};

export default RadioStack;
