import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { colors } from '../templates/colors';
import MusicScreen from '../screens/MusicScreen';
import MusicAlbums from '../screens/MusicAlbums';
import MusicPlayer from '../screens/MusicPlayer';
import HOC from './HOC';
import SubscribesScreen from '../screens/SubscribesScreen';
import ProductsScreen from '../screens/ProductsScreen';
import Replenish from '../components/Replenish';
const Stack = createStackNavigator();

const MusicStack = ({setPath}) => {
  return (
    <Stack.Navigator
      initialRouteName="Музыка"
      screenOptions={{
        headerStyle: {
          backgroundColor: colors.primary,
        },
        headerTintColor: colors.secondary,
      }}>
      <Stack.Screen name="Музыка" component={HOC(MusicScreen, setPath)} options={{ headerShown: false }} />
      <Stack.Screen name="albums" component={MusicAlbums} />
      <Stack.Screen name="mplayer" component={MusicPlayer} />
      <Stack.Screen name='subscribes' component={SubscribesScreen} />
      <Stack.Screen name='products' component={ProductsScreen} />
      <Stack.Screen name='replenish' component={Replenish} />
    </Stack.Navigator>
  );
};
export default MusicStack;
