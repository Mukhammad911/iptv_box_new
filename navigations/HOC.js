/* eslint-disable prettier/prettier */
/* eslint-disable no-unused-vars */
/* eslint-disable react-hooks/rules-of-hooks */
/* eslint-disable prettier/prettier */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */

import React, { useEffect, useRef, useState } from 'react';
import { View, Image, Button, TouchableOpacity, Text, TVEventHandler } from 'react-native';
import { percentHeight, percentWidth } from '../templates/helper';
import Icon from 'react-native-vector-icons/Ionicons';
import { FlatList, TouchableWithoutFeedback } from 'react-native-gesture-handler';
import { colors } from '../templates/colors';
import FocusableOpacity from '../components/FocusableOpacity';
import TouchableFocus from '../components/TouchableFocusMenu';
import { useDispatch, useSelector } from 'react-redux';
import { setFirstElementRef, setMenuRef } from '../redux/actions';
import { useIsFocused } from '@react-navigation/native';

const HOC = (Children, setPath) => (props) => {

    const isFocused = useIsFocused();
    let [menuFocused, setMenuFocused] = useState(false);
    const [menuHasFocus, setMenuHasFocus] = useState(false);
    const ref = useRef(null);
    const dispatch = useDispatch();
    const { menuRef, firstElementRef } = useSelector(state => state.refReducer);

    const _tvEventHandler = new TVEventHandler();

    let menuTimeout = null;

    const _enableTVEventHandler = () => {
        _tvEventHandler.enable(this, function (cmp, evt) {
            if (evt.eventKeyAction == 1) {
                // console.log(evt.eventType);
                if (evt && evt.eventType === 'right') {
                    setMenuFocused(false);
                } else if (evt && evt.eventType === 'up') {
                } else if (evt && evt.eventType === 'left') {
                } else if (evt && evt.eventType === 'down') {
                }
            }
        });
    };

    const _disableTVEventHandler = () => {
        if (_tvEventHandler) {
            _tvEventHandler.disable();
        }
    }

    useEffect(() => {
        if (!isFocused) {
            _disableTVEventHandler();
            return;
        }
        _enableTVEventHandler();
        return () => _disableTVEventHandler();
    }, [isFocused]);

    if (!isFocused) {
        return null;
    };

    // useEffect(() => {
    //     if (menuFocused) {
    //         clearTimeout(menuTimeout);
    //         setMenuHasFocus(true);
    //     } else {
    //         menuTimeout = setTimeout(() => {
    //             setMenuHasFocus(false);
    //         }, 500)
    //     }
    // }, [menuFocused])

    return (
        <View style={{ flexDirection: 'row', flex: 1, backgroundColor: '#191723' }} >
            <View
                style={{
                    width: menuFocused ? percentWidth(20) : percentWidth(5)
                    // width: percentWidth(5)
                }}
            >
                <View style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: 40,
                    marginBottom: 60,
                    backgroundColor: 'white',
                }} >
                    {
                        menuFocused ? (
                            <Image
                                style={{ height: percentWidth(3), width: percentWidth(14), resizeMode: 'stretch', position: 'absolute' }}
                                source={require('../img/logo_new.png')}
                            />
                        ) : (
                            <>
                                <Image
                                    style={{ height: percentWidth(3.5), width: percentWidth(3.5), resizeMode: 'stretch', position: 'absolute' }}
                                    source={require('../img/Ellipse.png')}
                                />
                                <Image
                                    style={{ height: percentWidth(1.5), width: percentWidth(1.5), resizeMode: 'stretch', position: 'absolute', marginLeft: 'auto', marginRight: 'auto' }}
                                    source={require('../img/Vector.png')}
                                />
                            </>
                        )
                    }
                </View>
                <View
                    style={{
                        overflow: 'hidden',
                    }}
                >
                    <TouchableOpacity />
                    {
                        menuFocused ? (

                            <>
                                <TouchableFocus
                                    setRef={reff => {
                                        dispatch(setMenuRef(reff));
                                        // console.log('got in setRef -----------------------' + reff._nativeTag);
                                    }}
                                    style={{
                                        backgroundColor: '#191723',
                                        flex: 1,
                                        paddingBottom: percentHeight(3),
                                        paddingTop: percentHeight(3),
                                        alignItems: 'center',
                                        justifyContent: 'flex-start',
                                        paddingLeft: percentWidth(2),
                                        flexDirection: 'row'
                                    }}
                                    onPress={() => {
                                        // menuRef.setNativeProps({
                                        //     hasTVPreferredFocus: true,
                                        // })
                                        dispatch(setFirstElementRef(null));
                                        props.navigation.jumpTo('Главная');
                                        setPath('Главная');
                                    }}
                                    focusedStyle={{
                                        backgroundColor: colors.dark_blue,
                                    }}
                                    onFocusChange={(isFocused) => {
                                        setMenuFocused(isFocused)
                                        menuFocused = isFocused;
                                        // console.log(isFocused ? 'focused' : 'blured');
                                    }}
                                    nextFocusLeftSelf={true}
                                    nextFocusUpSelf={true}
                                    nextFocusRight={firstElementRef}
                                >
                                    <Icon
                                        name="home-outline"
                                        size={percentHeight(3)}
                                        style={{
                                            color: colors.dark_text,
                                        }}
                                    />
                                    <Text
                                        style={{
                                            color: colors.dark_text,
                                        }}
                                    >
                                        &nbsp;&nbsp;Главная
                                    </Text>
                                </TouchableFocus>
                                <TouchableFocus
                                    style={{
                                        backgroundColor: '#191723',
                                        flex: 1,
                                        paddingBottom: percentHeight(3),
                                        paddingTop: percentHeight(3),
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        justifyContent: 'flex-start',
                                        paddingLeft: percentWidth(2),
                                        flexDirection: 'row'
                                    }}
                                    onPress={() => {
                                        // console.log('menuRef is:  ', menuRef);
                                        // menuRef.setNativeProps({
                                        //     hasTVPreferredFocus: true,
                                        // })
                                        dispatch(setFirstElementRef(null));
                                        props.navigation.jumpTo('Фильмы')
                                        setPath('Фильмы')
                                    }}
                                    focusedStyle={{
                                        backgroundColor: colors.dark_blue,
                                    }}
                                    // onFocusChange={(isFocused) => {
                                    //     setMenuFocused(isFocused);
                                    // }}
                                    onFocusChange={(isFocused) => {
                                        setMenuFocused(isFocused)
                                        menuFocused = isFocused;
                                        // console.log(isFocused ? 'focused' : 'blured');
                                    }}
                                    nextFocusLeftSelf={true}
                                    nextFocusRight={firstElementRef}
                                >
                                    <Icon
                                        name="videocam-outline"
                                        size={percentHeight(3)}
                                        style={{
                                            color: colors.dark_text,
                                        }}
                                    />
                                    <Text
                                        style={{
                                            color: colors.dark_text,
                                        }}
                                    >
                                        &nbsp;&nbsp;Фильмы
                                    </Text>
                                </TouchableFocus>
                                <TouchableFocus
                                    style={{
                                        backgroundColor: '#191723',
                                        flex: 1,
                                        paddingBottom: percentHeight(3),
                                        paddingTop: percentHeight(3),
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        justifyContent: 'flex-start',
                                        paddingLeft: percentWidth(2),
                                        flexDirection: 'row'
                                    }}
                                    onPress={() => {
                                        // menuRef.setNativeProps({
                                        //     hasTVPreferredFocus: true,
                                        // })
                                        dispatch(setFirstElementRef(null));
                                        props.navigation.jumpTo('ТВ');
                                        setPath('ТВ');
                                    }}
                                    focusedStyle={{
                                        backgroundColor: colors.dark_blue,
                                    }}
                                    // onFocusChange={(isFocused) => {
                                    //     setMenuFocused(isFocused);
                                    // }}
                                    onFocusChange={(isFocused) => {
                                        setMenuFocused(isFocused)
                                        menuFocused = isFocused;
                                        // console.log(isFocused ? 'focused' : 'blured');
                                    }}
                                    nextFocusLeftSelf={true}
                                    nextFocusRight={firstElementRef}
                                >
                                    <Icon
                                        name="tv"
                                        size={percentHeight(3)}
                                        style={{
                                            color: colors.dark_text,
                                        }}
                                    />
                                    <Text
                                        style={{
                                            color: colors.dark_text,
                                        }}
                                    >
                                        &nbsp;&nbsp;Телеканалы
                                    </Text>
                                </TouchableFocus>
                                <TouchableFocus
                                    style={{
                                        backgroundColor: '#191723',
                                        flex: 1,
                                        paddingBottom: percentHeight(3),
                                        paddingTop: percentHeight(3),
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        justifyContent: 'flex-start',
                                        paddingLeft: percentWidth(2),
                                        flexDirection: 'row'
                                    }}
                                    onPress={() => {
                                        // menuRef.setNativeProps({
                                        //     hasTVPreferredFocus: true,
                                        // })
                                        dispatch(setFirstElementRef(null));
                                        props.navigation.jumpTo('Музыка');
                                        setPath('Музыка');
                                    }}
                                    focusedStyle={{
                                        backgroundColor: colors.dark_blue,
                                    }}
                                    // onFocusChange={(isFocused) => {
                                    //     setMenuFocused(isFocused);
                                    // }}
                                    onFocusChange={(isFocused) => {
                                        setMenuFocused(isFocused)
                                        menuFocused = isFocused;
                                        // console.log(isFocused ? 'focused' : 'blured');
                                    }}
                                    nextFocusLeftSelf={true}
                                    nextFocusRight={firstElementRef}
                                >
                                    <Icon
                                        name="musical-notes-outline"
                                        size={percentHeight(3)}
                                        style={{
                                            color: colors.dark_text,
                                        }}
                                    />
                                    <Text
                                        style={{
                                            color: colors.dark_text,
                                        }}
                                    >
                                        &nbsp;&nbsp;Музыка
                                    </Text>
                                </TouchableFocus>
                                <TouchableFocus
                                    style={{
                                        backgroundColor: '#191723',
                                        flex: 1,
                                        paddingBottom: percentHeight(3),
                                        paddingTop: percentHeight(3),
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        justifyContent: 'flex-start',
                                        paddingLeft: percentWidth(2),
                                        flexDirection: 'row'
                                    }}
                                    onPress={() => {
                                        // menuRef.setNativeProps({
                                        //     hasTVPreferredFocus: true,
                                        // })
                                        dispatch(setFirstElementRef(null));
                                        props.navigation.jumpTo('Радио');
                                        setPath('Радио');
                                    }}
                                    focusedStyle={{
                                        backgroundColor: colors.dark_blue,
                                    }}
                                    onFocusChange={(isFocused) => {
                                        menuFocused = isFocused;
                                        setMenuFocused(isFocused);
                                    }}
                                    nextFocusLeftSelf={true}
                                    nextFocusRight={firstElementRef}
                                >
                                    <Icon
                                        name="radio"
                                        size={percentHeight(3)}
                                        style={{
                                            color: colors.dark_text,
                                        }}
                                    />
                                    <Text
                                        style={{
                                            color: colors.dark_text,
                                        }}
                                    >
                                        &nbsp;&nbsp;Радио
                                    </Text>
                                </TouchableFocus>
                                <TouchableFocus
                                    style={{
                                        backgroundColor: '#191723',
                                        flex: 1,
                                        paddingBottom: percentHeight(3),
                                        paddingTop: percentHeight(3),
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        justifyContent: 'flex-start',
                                        paddingLeft: percentWidth(2),
                                        flexDirection: 'row'
                                    }}
                                    onPress={() => {
                                        // menuRef.setNativeProps({
                                        //     hasTVPreferredFocus: true,
                                        // })
                                        dispatch(setFirstElementRef(null));
                                        props.navigation.jumpTo('Профиль');
                                        setPath('Профиль');
                                    }}
                                    focusedStyle={{
                                        backgroundColor: colors.dark_blue,
                                    }}
                                    onFocusChange={(isFocused) => {
                                        setMenuFocused(isFocused);
                                        menuFocused = isFocused;
                                    }}
                                    nextFocusLeftSelf={true}
                                    nextFocusDownSelf={true}
                                    nextFocusRight={firstElementRef}
                                >
                                    <Icon
                                        name="person-circle-outline"
                                        size={percentHeight(3)}
                                        style={{
                                            color: colors.dark_text,
                                        }}
                                    />
                                    <Text
                                        style={{
                                            color: colors.dark_text,
                                        }}
                                    >
                                        &nbsp;&nbsp;Профиль
                                    </Text>
                                </TouchableFocus>
                            </>
                        ) : (
                            <>
                                <TouchableFocus
                                    setRef={reff => {
                                        dispatch(setMenuRef(reff));
                                        // console.log('got in setRef -----------------------' + reff._nativeTag);
                                    }}
                                    style={{
                                        backgroundColor: '#191723',
                                        flex: 1,
                                        paddingBottom: percentHeight(3),
                                        paddingTop: percentHeight(3),
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}
                                    onPress={() => {
                                        // menuRef.setNativeProps({
                                        //     hasTVPreferredFocus: true,
                                        // })
                                        dispatch(setFirstElementRef(null));
                                        props.navigation.jumpTo('Главная');
                                        setPath('Главная');
                                    }}
                                    focusedStyle={{
                                        backgroundColor: colors.dark_blue,
                                    }}
                                    onFocusChange={(isFocused) => {
                                        setMenuFocused(isFocused)
                                        menuFocused = isFocused;
                                        // console.log(isFocused ? 'focused' : 'blured');
                                    }}
                                    nextFocusLeftSelf={true}
                                    nextFocusUpSelf={true}
                                    nextFocusRight={firstElementRef}
                                >
                                    <Icon
                                        name="home-outline"
                                        size={percentHeight(3)}
                                        style={{
                                            color: colors.dark_text,
                                        }}
                                    />
                                </TouchableFocus>
                                <TouchableFocus
                                    style={{
                                        backgroundColor: '#191723',
                                        flex: 1,
                                        paddingBottom: percentHeight(3),
                                        paddingTop: percentHeight(3),
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}
                                    onPress={() => {
                                        // console.log('menuRef is:  ', menuRef);
                                        // menuRef.setNativeProps({
                                        //     hasTVPreferredFocus: true,
                                        // })
                                        dispatch(setFirstElementRef(null));
                                        props.navigation.jumpTo('Фильмы')
                                        setPath('Фильмы')
                                    }}
                                    focusedStyle={{
                                        backgroundColor: colors.dark_blue,
                                    }}
                                    // onFocusChange={(isFocused) => {
                                    //     setMenuFocused(isFocused);
                                    // }}
                                    onFocusChange={(isFocused) => {
                                        setMenuFocused(isFocused)
                                        menuFocused = isFocused;
                                        // console.log(isFocused ? 'focused' : 'blured');
                                    }}
                                    nextFocusLeftSelf={true}
                                    nextFocusRight={firstElementRef}
                                >
                                    <Icon
                                        name="videocam-outline"
                                        size={percentHeight(3)}
                                        style={{
                                            color: colors.dark_text,
                                        }}
                                    />
                                </TouchableFocus>
                                <TouchableFocus
                                    style={{
                                        backgroundColor: '#191723',
                                        flex: 1,
                                        paddingBottom: percentHeight(3),
                                        paddingTop: percentHeight(3),
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}
                                    onPress={() => {
                                        // menuRef.setNativeProps({
                                        //     hasTVPreferredFocus: true,
                                        // })
                                        dispatch(setFirstElementRef(null));
                                        props.navigation.jumpTo('ТВ');
                                        setPath('ТВ');
                                    }}
                                    focusedStyle={{
                                        backgroundColor: colors.dark_blue,
                                    }}
                                    // onFocusChange={(isFocused) => {
                                    //     setMenuFocused(isFocused);
                                    // }}
                                    onFocusChange={(isFocused) => {
                                        setMenuFocused(isFocused)
                                        menuFocused = isFocused;
                                        // console.log(isFocused ? 'focused' : 'blured');
                                    }}
                                    nextFocusLeftSelf={true}
                                    nextFocusRight={firstElementRef}
                                >
                                    <Icon
                                        name="tv"
                                        size={percentHeight(3)}
                                        style={{
                                            color: colors.dark_text,
                                        }}
                                    />
                                </TouchableFocus>
                                <TouchableFocus
                                    style={{
                                        backgroundColor: '#191723',
                                        flex: 1,
                                        paddingBottom: percentHeight(3),
                                        paddingTop: percentHeight(3),
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}
                                    onPress={() => {
                                        // menuRef.setNativeProps({
                                        //     hasTVPreferredFocus: true,
                                        // })
                                        dispatch(setFirstElementRef(null));
                                        props.navigation.jumpTo('Музыка');
                                        setPath('Музыка');
                                    }}
                                    focusedStyle={{
                                        backgroundColor: colors.dark_blue,
                                    }}
                                    // onFocusChange={(isFocused) => {
                                    //     setMenuFocused(isFocused);
                                    // }}
                                    onFocusChange={(isFocused) => {
                                        setMenuFocused(isFocused)
                                        menuFocused = isFocused;
                                        // console.log(isFocused ? 'focused' : 'blured');
                                    }}
                                    nextFocusLeftSelf={true}
                                    nextFocusRight={firstElementRef}
                                >
                                    <Icon
                                        name="musical-notes-outline"
                                        size={percentHeight(3)}
                                        style={{
                                            color: colors.dark_text,
                                        }}
                                    />
                                </TouchableFocus>
                                <TouchableFocus
                                    style={{
                                        backgroundColor: '#191723',
                                        flex: 1,
                                        paddingBottom: percentHeight(3),
                                        paddingTop: percentHeight(3),
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}
                                    onPress={() => {
                                        // menuRef.setNativeProps({
                                        //     hasTVPreferredFocus: true,
                                        // })
                                        dispatch(setFirstElementRef(null));
                                        props.navigation.jumpTo('Радио');
                                        setPath('Радио');
                                    }}
                                    focusedStyle={{
                                        backgroundColor: colors.dark_blue,
                                    }}
                                    onFocusChange={(isFocused) => {
                                        menuFocused = isFocused;
                                        setMenuFocused(isFocused);
                                    }}
                                    nextFocusLeftSelf={true}
                                    nextFocusRight={firstElementRef}
                                >
                                    <Icon
                                        name="radio"
                                        size={percentHeight(3)}
                                        style={{
                                            color: colors.dark_text,
                                        }}
                                    />
                                </TouchableFocus>
                                <TouchableFocus
                                    style={{
                                        backgroundColor: '#191723',
                                        flex: 1,
                                        paddingBottom: percentHeight(3),
                                        paddingTop: percentHeight(3),
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                    }}
                                    onPress={() => {
                                        // menuRef.setNativeProps({
                                        //     hasTVPreferredFocus: true,
                                        // })
                                        dispatch(setFirstElementRef(null));
                                        props.navigation.jumpTo('Профиль');
                                        setPath('Профиль');
                                    }}
                                    focusedStyle={{
                                        backgroundColor: colors.dark_blue,
                                    }}
                                    onFocusChange={(isFocused) => {
                                        setMenuFocused(isFocused);
                                        menuFocused = isFocused;
                                    }}
                                    nextFocusLeftSelf={true}
                                    nextFocusDownSelf={true}
                                    nextFocusRight={firstElementRef}
                                >
                                    <Icon
                                        name="person-circle-outline"
                                        size={percentHeight(3)}
                                        style={{
                                            color: colors.dark_text,
                                        }}
                                    />
                                </TouchableFocus>
                            </>

                        )
                    }
                </View>
            </View>
            <View style={{ width: percentWidth(95) }} >
                <Children {...props} />
            </View>
        </View>
    );
};

export default HOC;
