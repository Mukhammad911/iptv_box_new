import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { colors } from '../templates/colors';
import MoviesScreen from '../screens/MoviesScreen';
import ProfileScreen from '../screens/ProfileScreen';
import VPlayerScreen from '../screens/VPlayerScreen';
import HOC from './HOC'
import MovieInfoScreen from '../screens/MovieInfoScreen';
import SubscribesScreen from '../screens/SubscribesScreen';
import ProductsScreen from '../screens/ProductsScreen';
import Replenish from '../components/Replenish';

const Stack = createStackNavigator();

const MoviesStack = ({ route, setPath }) => {
  return (
    <Stack.Navigator
      initialRouteName="movies"
      screenOptions={{
        headerStyle: {
          backgroundColor: colors.primary,
        },
        headerTintColor: colors.secondary,
      }}>
      <Stack.Screen name='movies' component={HOC(MoviesScreen, setPath)} options={{ headerShown: false }} />
      <Stack.Screen name='profile' component={ProfileScreen} />
      <Stack.Screen name='vplayer' component={VPlayerScreen} />
      <Stack.Screen name='movieInfo' component={MovieInfoScreen} />
      <Stack.Screen name='subscribes' component={SubscribesScreen} />
      <Stack.Screen name='products' component={ProductsScreen} />
      <Stack.Screen name='replenish' component={Replenish} />
    </Stack.Navigator>
  );
};
export default MoviesStack;
