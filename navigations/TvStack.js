import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { colors } from '../templates/colors';
import GroupChannelsScreen from '../screens/GroupChannelsScreen';
import ChannelsScreen from '../screens/ChannelsScreen';
import PlayerScreen from '../screens/PlayerScreen';
import ProfileScreen from '../screens/ProfileScreen';
import HOC from './HOC';
import SubscribesScreen from '../screens/SubscribesScreen';
import ProductsScreen from '../screens/ProductsScreen';
import Replenish from '../components/Replenish';

const Stack = createStackNavigator();

const TvStack = ({ setPath }) => {

    return (
        <Stack.Navigator
            initialRouteName="Группа каналов"
            screenOptions={{
                headerStyle: {
                    backgroundColor: colors.primary,

                },
                headerTintColor: colors.secondary
            }}
        >
            <Stack.Screen name="group_channels" component={HOC(GroupChannelsScreen, setPath)} options={{ headerShown: false }} />
            <Stack.Screen name="channels" component={ChannelsScreen} />
            <Stack.Screen name="player" component={PlayerScreen} />
            <Stack.Screen name="profile" component={ProfileScreen} />
            <Stack.Screen name='subscribes' component={SubscribesScreen} />
            <Stack.Screen name='products' component={ProductsScreen} />
            <Stack.Screen name='replenish' component={Replenish} />
        </Stack.Navigator>
    )
}
export default TvStack
