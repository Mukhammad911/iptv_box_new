import React, { useEffect, useState } from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import EpgScreen from '../screens/EpgScreen';
import { colors } from '../templates/colors';
import AsyncStorage from '@react-native-community/async-storage';
import { request_get } from '../api/request';
import { View, Text, ActivityIndicator } from 'react-native';
import { fSize, percentHeight, percentWidth } from '../templates/helper';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { color } from 'react-native-reanimated';
import Icon from 'react-native-vector-icons/Ionicons';
import TouchableFocus from '../components/TouchableFocus';
const Tab = createMaterialTopTabNavigator();
function EpgTabs({ channel_id, setUri, setShowEpgTabs, showControlsActivatorRef }) {
  const [data, setData] = useState([]);
  const [load, setLoad] = useState(true);
  const [epgFound, setEpgFound] = useState(false);
  const [activeEpg, setActiveEpg] = useState('')
  const [selectedProgram, setSelectedProgram] = useState('')

  const [firstEPGRef, setFirstEPGRef] = useState(null)

  useEffect(() => {
    const getData = async () => {
      setLoad(true);
      const sid = await AsyncStorage.getItem('sid');
      const response = await request_get(
        `get_epg/${channel_id}?sid=${sid}&reverse=true`,
      );
      if (!response.success) {
        setLoad(false);
        return;
      }
      let selected = ''
      for (let elem of response.data[response.data['active']]) {
        if (elem['start_date'] == (new Date()).getHours() + ":00") {
          selected = elem['url']
        }
      }
      setData(response.data);
      setActiveEpg(response.data['active'])
      setSelectedProgram(selected)
      setEpgFound(true);
      setLoad(false);
    };
    getData();
  }, [channel_id]);

  const epgView = () => {
    let epg = {};
    for (let key in data) {
      if (key == 'active') {
        continue;
      }
      epg[key] = data[key]
    }
    return epg;
  };

  const epgTags = () => {
    let epg = [];
    for (let key in data) {
      if (key == 'active') {
        continue;
      }
      epg.unshift(key)
    }
    return epg;
  };

  const loader = (show = false) => {
    return show ? (
      <ActivityIndicator
        color={colors.blue}
        size={'large'}
        style={{ position: 'absolute', top: 0, right: 0, left: 0, bottom: 0 }}
      />
    ) : null;
  };

  // useEffect(() => {
  //   setSelectedProgram(data[activeEpg][data[activeEpg].length - 1]['url'])
  // }, [activeEpg])

  return (
    <View
      style={{
        position: 'absolute',
        right: 0,
        top: 0,
        height: '100%',
        width: percentWidth(33),
        backgroundColor: colors.primary,
        paddingVertical: percentHeight(2),
        paddingHorizontal: percentWidth(2)
      }}
    >
      <View
        style={{
          marginVertical: percentHeight(2),
          flexDirection: 'row',
          justifyContent: 'space-between',
          alignItems: 'center'
        }}
      >
        <Text
          style={{
            color: colors.secondary,
            fontSize: fSize(7.5)
          }}
        >
          Телепрограмма
        </Text>
        <TouchableFocus
          onPress={() => {
            setShowEpgTabs(false)
            if (showControlsActivatorRef) {
              showControlsActivatorRef.setNativeProps({
                hasTVPreferredFocus: true
              })
            }
          }}
          style={{
            borderRadius: 40,
            height: percentHeight(6),
            width: percentHeight(6),
            justifyContent: 'center',
            alignItems: 'center'
          }}
          focusedStyle={{
            // transform: [{ scale: 1.3 }]
            backgroundColor: colors.dark_red,
          }}
          focusedOnStart={!epgFound}
          nextFocusLeftSelf
          nextFocusUpSelf
          nextFocusRightSelf
          nextFocusDownSelf={!epgFound}
        >
          <Icon name="close-outline" color={colors.secondary} size={fSize(10)} />
        </TouchableFocus>
      </View>
      {epgFound ? (
        <View>
          <ScrollView
            style={{
              height: percentHeight(8),
              // paddingTop: percentHeight(1),
              // backgroundColor: 'yellow'
            }}
            horizontal
          >
            {epgTags().map((elem, index, arr) => (
              <TouchableFocus
                style={{
                  backgroundColor: elem == activeEpg ? colors.dark_red : colors.primary,
                  paddingHorizontal: percentWidth(1),
                  paddingVertical: percentHeight(0.5),
                  width: percentWidth(7),
                  height: percentHeight(5),
                  borderWidth: 0,
                  borderRadius: 100,
                  // marginRight: percentWidth(0.5),
                  marginLeft: percentWidth(1),
                  marginTop: percentHeight(1.1)
                }}
                onPress={() => {
                  setActiveEpg(elem)
                }}
                focusedStyle={{
                  transform: [{ scale: 1.2 }]
                }}
                nextFocusLeftSelf={!index}
                nextFocusRightSelf={(index == (arr.length - 1))}
                nextFocusDown={firstEPGRef}
              >
                <Text
                  style={{
                    color: colors.secondary,
                    fontSize: fSize(6),
                    textAlign: 'center'
                  }}
                >
                  {elem}
                </Text>
              </TouchableFocus>
            ))}
          </ScrollView>
          <ScrollView
            style={{
              marginBottom: percentHeight(16),
              // paddingBottom: percentHeight(10)
            }}
          >
            {data[activeEpg].map((elem, index, arr) => (
              <TouchableFocus
                style={{
                  overflow: 'hidden',
                  marginVertical: percentHeight(1),
                  backgroundColor: elem['url'] == selectedProgram ? colors.dark_blue : colors.primary,
                  paddingVertical: percentHeight(1),
                  flexDirection: 'row',
                  justifyContent: 'space-between'
                }}
                onPress={() => {
                  setUri(elem['url'])
                }}
                focusedStyle={{
                  backgroundColor: colors.dark_blue
                }}
                focusedOnStart={!index}
                setRef={(!index ? setFirstEPGRef : (() => null))}
                nextFocusLeftSelf
                nextFocusRightSelf
                nextFocusDownSelf={(index == (arr.length - 1))}
              >
                <View
                  style={{
                    flex: 1,
                    paddingLeft: percentWidth(1)
                  }}
                >
                  <Text
                    style={{
                      color: colors.dark_text,
                      fontSize: fSize(5),
                    }}
                  >
                    {`${elem['start_date']} - ${elem['end_date']}`}
                  </Text>
                  <Text
                    style={{
                      color: colors.secondary,
                      fontSize: fSize(6),
                    }}
                  >
                    {elem['name']}
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    width: percentWidth(5)
                  }}
                >
                  <Icon name={elem['exists'] ? 'play' : 'alarm-outline'} color={colors.dark_red} size={fSize(10)} />
                </View>
              </TouchableFocus>
            ))}
          </ScrollView>
        </View>
      ) : (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
          {loader(load)}
          {data.length === 0 && !load ? (
            <Text
              style={{
                color: colors.gray,
                fontSize: fSize(7),
              }}>
              По данному каналу архивация отсутсвует
            </Text>
          ) : null}
        </View>
      )}
    </View>
  );
}
export default EpgTabs;
