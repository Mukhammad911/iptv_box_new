import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { colors } from '../templates/colors';
import Icon from 'react-native-vector-icons/Ionicons';
import MoviesStack from './MoviesStack';
import TvStack from './TvStack';
import ProfileStack from './ProfileStack';
import { useDispatch, useSelector } from 'react-redux';
import { setAuthState, setNavigationVisibility, setSid } from '../redux/actions';
import HomeStack from './HomeStack';
import sidValidation from '../services/sidChecker';
import AsyncStorage from '@react-native-community/async-storage';
import LoginForm from '../components/LoginForm';
import MusicScreen from '../screens/MusicScreen';
import RadioScreen from '../screens/RadioScreen';
import Loader from '../components/Loader';
import { View, StatusBar, Image, findNodeHandle } from 'react-native';
import RadioStack from './RadioStack';
import MusicStack from './MusicStack';
import KeyEvent from 'react-native-keyevent'
import { chechToken } from '../components/helper';
import SidNotFound from "../components/SidNotFound";
// import 'react-native-gesture-handler';

const Tab = createBottomTabNavigator()

const MainTabs = () => {
  /*const navigationState = useSelector(
    (state) => state.navigationReducer.navigationVisibility,
  );*/

  const isAuthorizated = useSelector((state) => state.authReducer.isAuth);

  // const firstElementRef = useSelector(state => state.refReducer.firstElementRef);
  const dispatch = useDispatch();
  const [loader, setLoader] = useState(false);

  const [path, setPath] = useState('Главная');

  useEffect(() => {
    switch (path) {
      case 'Главная':
        KeyEvent.removeKeyDownListener()
        KeyEvent.onKeyDownListener((key) => {
        })
        break;
      case 'Фильмы':
        KeyEvent.removeKeyDownListener()
        KeyEvent.onKeyDownListener((key) => {
        })
        break;
      case 'ТВ':
        KeyEvent.removeKeyDownListener()
        KeyEvent.onKeyDownListener((key) => {
        })
      default:
        break;
    }
  }, [path])

  useEffect(() => {
    dispatch(setNavigationVisibility(true));
  }, []);

  //Here we will check the access token
  useEffect(() => {
    console.log("[main.js][useEffect]['checking chechToken']");
    const ff = async () => {
      setLoader(true);
      let result = await chechToken()
      console.log("[main.js][useEffect]['checking result']:", result);
      if (result) {
      //  let sid = await AsyncStorage.getItem('sid')
        dispatch(setAuthState(true))
      //  dispatch(setSid(sid))
      } else {
        dispatch(setAuthState(false))
       // dispatch(setSid(''))
       // await AsyncStorage.removeItem('sid')

      }
      setLoader(false)
    }
    ff()
  }, []);

  //Here we will check the sid
  useEffect(() => {
     console.log("[main.js][useEffect]['checking sid']");

     setLoader(true);
     AsyncStorage.getItem('sid')
       .then((sid) => sidValidation(sid))
       .then(async ({ success, sid }) => {
         if (success) {
           dispatch(setSid(sid));
           //dispatch(setAuthState(true));
         } else {
          // dispatch(setAuthState(false));
           await AsyncStorage.removeItem('sid');
         }
       })
       .finally(() => setLoader(false));
   }, []);

  if (loader) {
    return (
      <View
        style={{
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: colors.primary
        }}>
        <Loader />
      </View>
    );
  }

  if (!isAuthorizated) {
    return <LoginForm />;
  }

  return (
    <NavigationContainer independent >
      <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;

            if (route.name === 'Главная') {
              iconName = 'home-outline';
            } else if (route.name === 'Фильмы') {
              iconName = 'videocam-outline';
            } else if (route.name === 'ТВ') {
              iconName = 'tv';
            } else if (route.name === 'Профиль') {
              iconName = 'person-circle-outline';
            } else if (route.name === 'Контакты') {
              iconName = 'call-outline';
            } else if (route.name === 'Радио') {
              iconName = 'radio';
            } else if (route.name === 'Музыка') {
              iconName = 'musical-notes-outline';
            }
            // You can return  component that you like here!
            return <Icon name={iconName} size={size} color={color} />;
          },
          tabBarVisible: false,

        })}
      >
        <Tab.Screen name="Главная">
          {props => <HomeStack {...props} setPath={setPath} />}
        </Tab.Screen>
        <Tab.Screen name="Фильмы">
          {props => <MoviesStack {...props} setPath={setPath} />}
        </Tab.Screen>
        <Tab.Screen name="ТВ">
          {props => <TvStack {...props} setPath={setPath} />}
        </Tab.Screen>
        <Tab.Screen name="Музыка">
          {props => <MusicStack {...props} setPath={setPath} />}
        </Tab.Screen>
        <Tab.Screen name="Радио">
          {props => <RadioStack {...props} setPath={setPath} />}
        </Tab.Screen>
        <Tab.Screen name="Профиль">
          {props => <ProfileStack {...props} setPath={setPath} />}
        </Tab.Screen>

      </Tab.Navigator>
    </NavigationContainer>
  );
};

const HOC = (Children, setPath) => (props) => {
  return <Children {...props} setPath={(path) => setPath(path)} />
}

export default MainTabs;
