/* eslint-disable prettier/prettier */
/* eslint-disable prettier/prettier */

import { SET_FIRST_FOCUSED_ELEMENT_REF, SET_MENU_REF } from './constants';

const initialState = {
    menuRef: null,
    firstElementRef: null,
};

const refReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_MENU_REF:
            return {
                ...state,
                menuRef: action.payload,
            };
        case SET_FIRST_FOCUSED_ELEMENT_REF:
            return {
                ...state,
                firstElementRef: action.payload,
            };
        default:
            return state;
    }
};

export default refReducer;
