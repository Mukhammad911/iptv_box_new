import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
export const BASE_API_URL = 'https://api.somon.tv/';

export const request_get = async (url) => {
  try {
    const access_token = await AsyncStorage.getItem('access_token');
    const response = await axios.get(BASE_API_URL + url, {headers: {'Authorization': access_token, 'Accept': 'application/json'}});
    return response.data;
  } catch (error) {
     // console.log(error);
  }
};

export const request_post = async (url, data = {}, config = {}) => {
  try {
    const response = await axios.post(BASE_API_URL + url, data, config);
    return response.data;
  } catch (error) {
    // console.error(error);
  }
};

export const request_delete = async (url, data = {}, config = {}) => {
  try {
    const response = await axios.delete(BASE_API_URL + url, data, config);
    return response.data;
  } catch (error) {
    // console.error(error);
  }
};
